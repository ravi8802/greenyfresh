package  com.dev.greeny.service.request

import com.dev.greeny.utility.Config
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit


class ApiClientConnection {


    fun createApiInterface(): Api? {
        apiInterface = null
        if (apiInterface == null) {

            val loggingInterceptor = HttpLoggingInterceptor()
            loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY

            val httpBuilder = OkHttpClient.Builder()
                .connectTimeout(300, TimeUnit.SECONDS)
                .readTimeout(300, TimeUnit.SECONDS)

            httpBuilder.addInterceptor(loggingInterceptor)

            val gson = GsonBuilder()
                    .setLenient()
                    .create()
            val builder = Retrofit.Builder()
                .baseUrl(Base_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(httpBuilder.build())

            val retrofit = builder.build()
            apiInterface = retrofit.create(Api::class.java)


        }
        return apiInterface
    }



    companion object {

        val Base_URL = Config().BASE_URL

        private var apiClientConnection: ApiClientConnection? = null
        private var apiInterface: Api? = null
        private val distanceMatrixInterface: Api? = null

        val instance: ApiClientConnection
            get() {
                apiClientConnection = null
                if (apiClientConnection == null) {
                    apiClientConnection = ApiClientConnection()
                }
                return apiClientConnection!!
            }
    }


}