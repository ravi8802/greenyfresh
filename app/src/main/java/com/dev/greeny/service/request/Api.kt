package  com.dev.greeny.service.request

import com.dev.greeny.baen.CartResponse
import com.dev.greeny.baen.CommonModel
import com.dev.greeny.baen.LoginInner
import  com.dev.greeny.service.response.RegistrationResponse
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.http.*
import java.util.*


interface Api {

    /* @POST
     @FormUrlEncoded
     fun getMastersJSON(@Url url: String, @Field("UserID") UserID: String): Call<List<MastersResponse>>
 */

    /*@POST("MastersData/GetMasterData")
    fun getAllMasters(@Body userID: UserId) : Call<List<MastersResponse>>
*/
    @FormUrlEncoded
    @POST("create")
    fun uploadreg1(
            @Field("name") name: String?, @Field("email") email: String?, @Field(
                    "phone"
            ) phone: String?, @Field("date_of_birth") date_of_birth: String?, @Field("address") address: String?, @Field(
                    "gender"
            ) gender: String?, @Field("password") password: String?, @Field("confirm_password") confirm_password: String?, @Field("user_type") user_type: String?, @Field("GST_number") GST_number: String?, @Field("state_id") state_id: String?, @Field("district_id") district_id: String?, @Field("category_id") Categoryid: String?, @Field("deviceType") deviceType: String?, @Field("deviceToken") deviceToken: String?
    ): Call<RegistrationResponse>

    @FormUrlEncoded
    @POST("register")
    fun register(@Field("refered_code ") refered_code : String?,@Field("full_name") full_name: String?, @Field("email") email: String?, @Field("phone"
    ) phone: String?, @Field("dob") dob: String?, @Field("address") address: String?, @Field("gender"
    ) gender: String?, @Field("password") password: String?, @Field("country_code") country_code: String?,
                 @Field("device_type") device_type: String?, @Field("device_token") device_token: String?): Call<RegistrationResponse>
//    refered_code

    @GET("category")
    fun categoriesfind(): Call<CommonModel>

    @GET("products")
    fun productsfindList(@Header("Accept") accept: String?,
                         @Header("apiconnection") apiconnection: String?,
                         @Header("apikey") apikey: String?): Call<RegistrationResponse>


    @GET("productsbycategory/{vegitables}/{userid}")
    fun productsbycategory(@Path("vegitables") id: String?,
                           @Path("userid") userid: String?): Call<RegistrationResponse>
// @GET("productsbycategory/{vegitables}")
//    fun productsbycategory(@Path("vegitables") id: String?): Call<RegistrationResponse>

    @GET("delete_address/{id}")
    fun deleteAddress(@Path("id") id: String?,
                      @Header("Accept") accept: String?,
                      @Header("Authorization") Authorization: String?): Call<RegistrationResponse>


    @GET("products/{products}")
    fun productsdetail(@Header("Accept") accept: String?,
                       @Header("apiconnection") apiconnection: String?,
                       @Header("apikey") apikey: String?,
                       @Path("products") id: String?): Call<RegistrationResponse>


    @GET("logout")
    fun logout(@Header("Accept") accept: String?,
               @Header("Authorization") Authorization: String?): Call<RegistrationResponse>

    @GET("cart_header")
    fun cartheader(@Header("Accept") accept: String?,
                   @Header("Authorization") Authorization: String?): Call<RegistrationResponse>

    @GET("wishlist/{id}")
    fun likeUnlike(@Path("id") id: String,
                   @Header("Accept") accept: String?,
                   @Header("Authorization") Authorization: String?): Call<RegistrationResponse>

    @GET("wishlist_products")
    fun wishlistProducts(@Header("Accept") accept: String?,
                         @Header("Authorization") Authorization: String?): Call<RegistrationResponse>


    @FormUrlEncoded
    @POST("addtocart")
    fun addtocart(@Header("Accept") accept: String?,
                  @Header("Authorization") Authorization: String?,
                  @Field("product_id") product_id: String?,
                  @Field("quantity") quantity: String?): Call<RegistrationResponse>

    @FormUrlEncoded
    @POST("updatecart")
    fun updatecart(@Header("Accept") accept: String?,
                   @Header("Authorization") Authorization: String?,
                   @Field("id") id: Int?,
                   @Field("qty") qty: Int?): Call<RegistrationResponse>


    @FormUrlEncoded
    @POST("productList")
    fun ProductList(@Field("category_id") category_id: Int?, @Field("shopkeeper_id") shopkeeper_id: Int?): Call<RegistrationResponse>

    @FormUrlEncoded
    @POST("shopList")
    fun ShopList(@Field("state_id") state_id: Int?, @Field("district_id") district_id: Int?, @Field("category_id") category_id: Int?, @Field("token") token: String?): Call<RegistrationResponse>

    @FormUrlEncoded
    @POST("categoryList")
    fun CategoryList(@Field("token") token: String?): Call<RegistrationResponse>

    @FormUrlEncoded
    @POST("addCategory")
    fun addCategory(@Field("token") token: String?, @Field("category_name") category_name: String?): Call<RegistrationResponse>

    /*@Multipart
    @POST("addProducts")
    fun addProducts(@Field("token") token: String?,@Field("product_name") product_name: String?,@Field("product_price") product_price: String?,@Field("product_description") product_description: String?,@Field("product_size") product_size: String?,@Field("product_color") product_color: String?,@Field("product_quantity") product_quantity: String?,@Field("category_id") category_id: Int?): Call<RegistrationResponse>
*/
    @Multipart
    @POST("addProducts")
    fun addProducts(@PartMap map: HashMap<String, RequestBody>, @Part image: MultipartBody.Part): Call<RegistrationResponse>


    @FormUrlEncoded
    @POST("login")
    fun login(@Field("phone_no") phone_no: String?, @Field("password") password: String?, @Field("device_type") device_type: String?, @Field("device_token") device_token: String?): Call<RegistrationResponse>

    @FormUrlEncoded
    @POST("createOrder")
    fun createOrder(@Field("token") token: String?,
                    @Field("product_id") product_id: Int?,
                    @Field("quantitiy") quantitiy: String): Call<RegistrationResponse>

    @FormUrlEncoded
    @POST("orderList")
    fun orderList(@Field("token") token: String?): Call<RegistrationResponse>

    @GET("cart")
    fun cardList(@Header("Authorization") Authorization: String?): Call<RegistrationResponse>

    @GET("address")
    fun addressList(@Header("Accept") Accept: String?, @Header("Authorization") Authorization: String?): Call<RegistrationResponse>

    @GET("checkout")
    fun checkout(@Header("Accept") Accept: String?, @Header("Authorization") Authorization: String?): Call<RegistrationResponse>

    @GET("removecart/{id}")
    fun removeItemList(@Path("id") id: String?,
                       @Header("Authorization") Authorization: String?): Call<RegistrationResponse>


    @FormUrlEncoded
    @POST("removeItem")
    fun removeItemList1(@Field("token") token: String?,
                        @Field("product_id") product_id: Int?): Call<RegistrationResponse>

    @FormUrlEncoded
    @POST("place_order")
    fun placeOrder(@Header("Accept") accept: String?,
                   @Header("Authorization") Authorization: String?,
                   @Field("full_name") full_name: String?,
                   @Field("phone") phone: String?,
                   @Field("address") address: String?,
                   @Field("city") city: String?,
                   @Field("state") state: String?,
                   @Field("zipcode") zipcode: String?,
                   @Field("other_notes") other_notes: String?,
                   @Field("payment_mode") payment_mode: String?): Call<RegistrationResponse>

    @FormUrlEncoded
    @POST("add_address")
    fun addAddress(@Header("Accept") accept: String?,
                   @Header("Authorization") Authorization: String?,
                   @Field("full_name") full_name: String?,
                   @Field("address") address: String?,
                   @Field("city") city: String?,
                   @Field("state") state: String?,
                   @Field("phone") phone: String?,
                   @Field("postcode") postcode: String?,
                   @Field("landmark") landmark: String?): Call<LoginInner>


    @Multipart
    @POST("editProfile")
    fun updateprofile(@PartMap map: HashMap<String, RequestBody>, @Part image: MultipartBody.Part): Call<RegistrationResponse>

    @FormUrlEncoded
    @POST("customerOrderHistory")
    fun OrderHistory(@Field("token") token: String?): Call<RegistrationResponse>

    @FormUrlEncoded
    @POST("forgot_password")
    fun forgot_password(@Field("email") email: String?): Call<RegistrationResponse>

    @GET("orders")
    fun ordersList(@Header("Accept") Accept: String?, @Header("Authorization") Authorization: String?): Call<CommonModel>

    @GET("order_number")
    fun ordernumber(@Header("Accept") Accept: String?, @Header("Authorization") Authorization: String?): Call<CommonModel>

    @GET("advertise")
    fun advertise(): Call<CommonModel>

    @Multipart
    @POST("generateChecksum")
    @JvmSuppressWildcards
    fun generateChecksum(@PartMap part: Map<String?, RequestBody?>?): Call<com.dev.greeny.baen.CommonModel?>?

    @POST("paytm_checksum")
    fun paytmchecksum(@Header("Accept") accept: String?,
                   @Header("Authorization") Authorization: String?): Call<CommonModel>
}