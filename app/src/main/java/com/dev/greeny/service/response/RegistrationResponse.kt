package  com.dev.greeny.service.response

import com.dev.greeny.baen.LoginInner
import com.dev.greeny.baen.OrderListResponse
import com.dev.greeny.baen.ProductDataResponce
import  com.dev.greeny.entity.*
import com.google.gson.annotations.SerializedName


class RegistrationResponse {

    @SerializedName("status")
    var status: String? = null

    @SerializedName("deliverycharges")
    var deliverycharges: String? = null

    @SerializedName("final_price")
    var final_price: Double? = null

    @SerializedName("subtotal")
    var subtotal: String? = null

    @SerializedName("sub_total")
    var sub_totalAmt: String? = null

    @SerializedName("message")
    var message: String? = null

    @SerializedName("final_tax")
    var final_tax: Double? = null

    @SerializedName("final_amount")
    var final_amount: Double? = null

    @SerializedName("totalcount")
    var totalcount: Int? = null

    @SerializedName("state")
    var mststate: List<mststate>? = null

    @SerializedName("registerResponse")
    var signupdata: LoginInner? = null

    @SerializedName("loginResponse")
    var logindata: LoginInner? = null

    @SerializedName("cartData")
    var cartdata: LoginInner? = null


    @SerializedName("district")
    var mstdistrict: List<mstdistrict>? = null

    @SerializedName("checkout")
    var checkoutdata: List<LoginInner>? = null

    @SerializedName("productList")
    var productList: List<ProductListEntity>? = null

    @SerializedName("productsbycategory")
    var dataList: List<ProductDataResponce>? = null

    @SerializedName("advertise")
    var advertisedata: List<ProductDataResponce>? = null

    @SerializedName("wishlist_products")
    var wishlistProductsList: List<ProductDataResponce>? = null

    @SerializedName("cart")
    var cartList: List<OrderListResponse>? = null

    @SerializedName("address")
    var addressList: List<OrderListResponse>? = null

    @SerializedName("categoryList")
    var categoryList: List<CategoryListEntity>? = null

    @SerializedName("shopList")
    var shopList: List<ShopListEntity>? = null

    @SerializedName("orderList")
    var orderList: List<OrderListResponse>? = null


    @SerializedName("orders")
    var cusOrderHistory: List<OrderListResponse>? = null


}