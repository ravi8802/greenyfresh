package com.dev.greeny.baen

import com.google.gson.annotations.SerializedName

class LoginResponse {
    @SerializedName("status")
    var status: String? = null

    @SerializedName("message")
    var message: String? = null

     @SerializedName("login")
    var logindata: LoginInner? = null




}