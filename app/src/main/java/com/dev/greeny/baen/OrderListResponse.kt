package com.dev.greeny.baen

import com.google.gson.annotations.SerializedName


class OrderListResponse {

    @SerializedName("address")
    var address: String? = null

    @SerializedName("city")
    var city: String? = null

    @SerializedName("state")
    var state: String? = null

    @SerializedName("full_name")
    var full_name: String? = null

//    @SerializedName("id")
//    var product_price: Int? = null

    @SerializedName("total_price")
    var total_price: Int? = null

    @SerializedName("phone")
    var phone: String? = null
@SerializedName("postcode")
    var postcode: String? = null
@SerializedName("landmark")
    var landmark: String? = null

    @SerializedName("name")
    var name: String? = null

    @SerializedName("image")
    var image: String? = null

    @SerializedName("price")
    var price: Int? = null

    @SerializedName("sale_price")
    var sale_price: Int? = null

    @SerializedName("quantity")
    var quantity: Int? = null

    @SerializedName("id")
    var id: Int? = null


}


//    @PrimaryKey @ColumnInfo(name = "product_id") val id: Int?,
//    @ColumnInfo(name = "product_name") val product_name: String?,
//    @ColumnInfo(name = "product_quantity") val product_quantity: String?,
//    @ColumnInfo(name = "product_image") val product_image: String?,
//    @ColumnInfo(name = "product_price") val product_price: Int?
