package com.dev.greeny.baen

import com.google.gson.annotations.SerializedName

class CartInnerResponse {
    @SerializedName("image")
    var image: String? = null

    @SerializedName("name")
    var name: String? = null

    @SerializedName("price")
    var price: Int? = null

    @SerializedName("quantity")
    var quantity: Int? = null

    @SerializedName("id")
    var id: Int? = null
}