package com.dev.greeny.baen;

import com.google.gson.annotations.SerializedName;

public class CommonResponse {

    @SerializedName("order_number")
    private String order_number;

    @SerializedName("name")
    private String name;

    @SerializedName("image")
    private String image;

    @SerializedName("subtotal")
    private String subtotal;

    @SerializedName("payment_method")
    private String payment_method;

    @SerializedName("address")
    private String address;
    @SerializedName("qty")
    private String qty;

    @SerializedName("price")
    private String price;
    @SerializedName("status")
    private String status;
    @SerializedName("title")
    private String title;

    @SerializedName("descriptions")
    private CommonResponse[] descriptionsData;

    @SerializedName("details")
    private CommonResponse[] detailsData;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public CommonResponse[] getDescriptionsData() {
        return descriptionsData;
    }

    public void setDescriptionsData(CommonResponse[] descriptionsData) {
        this.descriptionsData = descriptionsData;
    }

    public CommonResponse[] getDetailsData() {
        return detailsData;
    }

    public void setDetailsData(CommonResponse[] detailsData) {
        this.detailsData = detailsData;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getSubtotal() {
        return subtotal;
    }

    public String getQty() {
        return qty;
    }

    public void setQty(String qty) {
        this.qty = qty;
    }

    public void setSubtotal(String subtotal) {
        this.subtotal = subtotal;
    }

    public String getPayment_method() {
        return payment_method;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public void setPayment_method(String payment_method) {
        this.payment_method = payment_method;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getOrder_number() {
        return order_number;
    }

    public void setOrder_number(String order_number) {
        this.order_number = order_number;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public CommonResponse[] getSubCategoryData() {
        return detailsData;
    }

    public void setSubCategoryData(CommonResponse[] detailsData) {
        this.detailsData = detailsData;
    }
}
