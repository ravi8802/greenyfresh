package com.dev.greeny.baen

import com.google.gson.annotations.SerializedName

class CartResponse {
    @SerializedName("status")
    var status: String? = null

    @SerializedName("final_amount")
    var final_amount: Int? = null

    @SerializedName("cart")
    var cartList: List<CartInnerResponse>? = null
}