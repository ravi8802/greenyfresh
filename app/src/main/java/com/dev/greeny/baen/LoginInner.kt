package com.dev.greeny.baen

import com.google.gson.annotations.SerializedName

class LoginInner {
    @SerializedName("status")
    var status: String? = null

    @SerializedName("user")
    var userdata: LoginInner? = null

    @SerializedName("email")
    var email: String? = null

    @SerializedName("id")
    var user_id: String? = null

    @SerializedName("name")
    var name: String? = null

    @SerializedName("phone")
    var phone: String? = null

    @SerializedName("password")
    var password: String? = null

    @SerializedName("category_id")
    var category_id: Int? = null

    @SerializedName("auth")
    var token: String? = null

    @SerializedName("referal_code")
    var referal_code: String? = null

    @SerializedName("device_token")
    var device_token: String? = null

    @SerializedName("access_token")
    var access_token: String? = null

    @SerializedName("cartTotal")
    var cartTotal: Int? = null

    @SerializedName("total")
    var total: Int? = null


}