package com.dev.greeny.baen;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CommonModel {

    @SerializedName("status")
    private String status;

    @SerializedName("response_message")
    private String response_message;

    @SerializedName("advertise")
    List<CommonResponse> advertiseData;

    @SerializedName("category")
    List<CommonResponse> categoryData;


    @SerializedName("orders")
    private CommonResponse[] Data;


    public List<CommonResponse> getCategoryData() {
        return categoryData;
    }

    public void setCategoryData(List<CommonResponse> categoryData) {
        this.categoryData = categoryData;
    }

    public List<CommonResponse> getAdvertiseData() {
        return advertiseData;
    }

    public void setAdvertiseData(List<CommonResponse> advertiseData) {
        this.advertiseData = advertiseData;
    }

    @SerializedName("checksum")
    private String checksum;
    @SerializedName("order_number")
    private String order_number;

    public String getChecksum() {
        return checksum;
    }

    public void setChecksum(String checksum) {
        this.checksum = checksum;
    }

    //    public CommonResponse[] getAdvertiseData() {
//        return advertiseData;
//    }
//
//    public void setAdvertiseData(CommonResponse[] advertiseData) {
//        this.advertiseData = advertiseData;
//    }


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getResponse_message() {
        return response_message;
    }

    public void setResponse_message(String response_message) {
        this.response_message = response_message;
    }

    public CommonResponse[] getData() {
        return Data;
    }

    public void setData(CommonResponse[] data) {
        Data = data;
    }

    public String getOrder_number() {
        return order_number;
    }

    public void setOrder_number(String order_number) {
        this.order_number = order_number;
    }
}
