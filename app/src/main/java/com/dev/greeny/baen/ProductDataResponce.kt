package com.dev.greeny.baen

import com.dev.greeny.entity.ProductListEntity
import com.google.gson.annotations.SerializedName

class ProductDataResponce {
    @SerializedName("id")
    var id: Int? = null

    @SerializedName("product_id")
    var product_id: Int? = null

    @SerializedName("price")
    var price: Int? = null

    @SerializedName("image")
    var image: String? = null

    @SerializedName("name")
    var name: String? = null

    @SerializedName("wish")
    var wish: String? = null

    @SerializedName("cart")
    var cart: String? = null

    @SerializedName("alias")
    var alias: String? = null

    @SerializedName("descriptions")
    var descriptionsList: List<ProductDataResponce>? = null

    @SerializedName("product")
    var product: ProductDataResponce? = null

    @SerializedName("images")
    var imageList: List<ProductDataResponce>? = null
}