package com.dev.greeny.constant;

import java.io.Serializable;

public interface Constants extends Serializable {

    public static final int animDuration = 500;

    String kAppPreferences = "PeepsInPreferences";
    String kDefaultAppName = "HomeFuud";

    String kCurrentUser = "currentUser";
    String kData = "data";
    String kType = "Type";
    String kDebitCard = "Debit Card";
    String kCreaditCard = "Creadit Card";
    String kBuy = "buy";
    String kRent = "rent";
    String kRentEventCategory = "rentEvent";
    String kRrchInterior = "archInterior";
    String kRentEvent = "rentevent";
    String kStatus = "status";
    String kMessage = "message";
    String kMsg = "msg";
    String kSuccess = "SUCCESS";
    String kLikesDislikes = "likesDislikes";
    String kLikesMap = "kMap";
    String kConfirmReservation = "confirmReservation";

    String kSeperator = "__";
    String kEmptyString = "";
    String kWhitespace = " ";
    Integer kEmptyNumber = 0;

    String kMessageInternalInconsistency = "Some internal inconsistency occurred. Please try again.";
    String kMessageNetworkError = "Device does not connect to internet.";
    String kSocketTimeOut = kDefaultAppName + " Server not responding..";
    String kMessageServerNotRespondingError = kDefaultAppName + " server not responding!";
    String kMessageConnecting = "Connecting...";
    String kError = "Error";

    String kName = "name";
    String kToken = "token";
    String kRequestKey = "requestKey";
    String kIsFirstTime = "isFirstTime";
    String kRememberMe = "rememberMe";
    String kParameter = "parameter";
    String kFromActiveFragment = "activeFragment";
    String kTOTALCOUNT = "totalcount";

    String kDEVICETOKEN = "devicetoken";
    //////signUp///////////////

   /* <--------               ------->*/


}

