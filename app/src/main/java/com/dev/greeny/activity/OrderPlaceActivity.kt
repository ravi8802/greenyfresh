package com.dev.greeny.activity

import android.app.Activity
import android.app.Dialog
import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.Window
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.dev.greeny.R
import com.dev.greeny.baen.OrderListResponse
import com.dev.greeny.service.request.Api
import com.dev.greeny.service.request.ApiClientConnection
import com.dev.greeny.service.response.RegistrationResponse
import com.dev.greeny.utility.Utils
import com.dev.greeny.utility.Utils.Fullname
import com.dev.greeny.utility.Utils.Mobileno
import com.dev.greeny.utility.Validate
import com.dev.greeny.utility.snackBar
import com.razorpay.Checkout
import com.razorpay.PaymentResultListener
import kotlinx.android.synthetic.main.activity_order_place.*
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.Serializable


class OrderPlaceActivity : AppCompatActivity(), AdapterView.OnItemSelectedListener, View.OnClickListener, PaymentResultListener {
    internal lateinit var progressDialog: ProgressDialog
    var apiInterface: Api? = null
    var validate: Validate? = null
    var totalPrice: String? = null
    var paymentmethod: String? = null
    var paymentType: String? = null
    var full_name: String? = null
    var phone: String? = null
    var address: String? = null
    var city: String? = null
    var state: String? = null
    var zipcode: String? = null
    var from: String? = null

//    private val staticMainResponse: List<OrderListResponse>? = null
//
//
//   public fun getPlaceIntent(context: Context?, staticMainResponse: List<OrderListResponse?>?): Intent? {
//        val intent = Intent(context, OrderPlaceActivity::class.java)
//        intent.putExtra("kType", staticMainResponse as Serializable?)
//        return intent
//    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_order_place)

        validate = Validate(this)
        apiInterface = ApiClientConnection.Companion.instance.createApiInterface()
        Checkout.preload(applicationContext)

        if (intent != null) {
            from = intent.getStringExtra("from")
            if (from.equals("Address")) {
                full_name = intent.getStringExtra("full_name")
                phone = intent.getStringExtra("phone")
                address = intent.getStringExtra("address")
                city = intent.getStringExtra("city")
                state = intent.getStringExtra("state")
                zipcode = intent.getStringExtra("zipcode")


                text_deliveryaddress.setText(address)

            } else {
                totalPrice = intent.getStringExtra("totalPrice")
                text_total_price.setText(totalPrice)
            }


        }
        spinner_paymentmethod.setOnItemSelectedListener(this@OrderPlaceActivity)

        image_arrow.setOnClickListener {
            spinner_paymentmethod.performClick()
        }
        linear_select_address.setOnClickListener {
            val intent = Intent(this, AddressListActivity::class.java)
            //intent.putExtra("totalPrice", totalPrice)
            startActivity(intent)
        }
        image_back.setOnClickListener {
            finish()
        }


        //only online advance payment
        spinner_paymentmethod.isClickable = false
        spinner_paymentmethod.isEnabled = false
        //select card payment
        //select card payment
        spinner_paymentmethod.setSelection(1)
        setUpPaymentMethod()


        linear_payment.setOnClickListener {
            val dstr1 = totalPrice.toString()
            val double1: Double? = dstr1.toDouble()
            // var subtotal:Int = Integer.parseInt(totalPrice.toString())
            if (double1 != null) {
                if (double1 >= 200) {
                    if (!text_deliveryaddress.text.toString().equals("Select Delivery Address")) {
                        if (paymentmethod.equals("Card")) {
                            paymentType = "online"
                            startPayment()
                        } else if (paymentmethod.equals("COD")) {
                            paymentType = "cod"
                            callPlaceOrderListApi()
                        }else if (paymentmethod.equals("Paytm")) {
                            paymentType = "paytm"
                            val intent = Intent(this, PaymentGatewayActivity::class.java)
                            intent.putExtra("totalPrice", totalPrice)
                            intent.putExtra("paymentType", paymentType)
                            startActivity(intent)
                        }
                    } else {
                        Toast.makeText(applicationContext, "Please Provide the Delivery Address", Toast.LENGTH_SHORT).show()
                    }
                } else {
                    Toast.makeText(applicationContext, "Minimum order amount required Rs. 200", Toast.LENGTH_SHORT).show()
                }
            }
        }

        //callCheckoutApi()
    }

    override fun onResume() {
        super.onResume()
        callCheckoutApi()
    }


    private fun setUpPaymentMethod() {
        val paymentmethod = resources.getStringArray(R.array.payment_method)
        val spinner_adapter = ArrayAdapter(this@OrderPlaceActivity, R.layout.layout_spinner_text, paymentmethod)
        spinner_paymentmethod.setAdapter(spinner_adapter)
    }

    // Set an on item selected listener for spinner object
    override fun onClick(view: View?) {
        when (view) {
//            linear_payment -> {
//                //callPlaceOrderListApi()
//            }
        }
    }

    fun callPlaceOrderListApi() {
        progressDialog = ProgressDialog.show(
                this, resources.getString(R.string.app_name),
                getString(R.string.datadownloading)
        )
        //        progressDialog.setIcon(getDrawable(R.drawable.appicon))

        val call = apiInterface?.placeOrder("application/json",
                "Bearer " +
                        validate!!.RetriveSharepreferenceString(Utils.Token)!!, full_name, phone, address, city, state, zipcode, edit_requirement.text.toString(), paymentType)
        call?.enqueue(object : Callback<RegistrationResponse> {
            override fun onFailure(call: Call<RegistrationResponse>, t: Throwable) {
                t.printStackTrace()
                progressDialog.dismiss()
            }

            override fun onResponse(
                    call: Call<RegistrationResponse>, response: Response<RegistrationResponse>
            ) {
                when (response.code()) {

                    200 -> {
                        if (response!!.body().status.equals("true")) {
                            progressDialog.dismiss()
                            //  validate!!.SaveSharepreferenceInt(Utils.Totalcount,0)
                            showSuccessDialog()

                            snackBar(this@OrderPlaceActivity, response!!.body().message)

                        } else {
                            progressDialog.dismiss()
                            snackBar(this@OrderPlaceActivity, response!!.body().message)
                        }
                    }
                    500 -> {
                        progressDialog.dismiss()
                    }
                }

            }

        })


    }

    override fun onNothingSelected(parent: AdapterView<*>?) {
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        if (spinner_paymentmethod.getSelectedItemPosition() == 0) {
            text_payment_method.visibility = View.VISIBLE
            paymentmethod = "COD"
        } else if (spinner_paymentmethod.getSelectedItemPosition() == 1) {
            text_payment_method.visibility = View.VISIBLE
            paymentmethod = "Card"
//            val intent = Intent(this, PaymentGatewayActivity::class.java)
//            intent.putExtra("totalPrice", totalPrice)
//            startActivity(intent)
            //Toast.makeText(applicationContext,"Under development",Toast.LENGTH_SHORT).show()
        }else if (spinner_paymentmethod.getSelectedItemPosition() == 2) {
            text_payment_method.visibility = View.VISIBLE
            paymentmethod = "Paytm"
        }
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

//        when (requestCode) {
//            REQUEST_PICK_IMAGE -> {
//                if (resultCode == Activity.RESULT_OK) {
//                    if (data != null) {
//
//                    } else {
//                        Log.v("ProjectDetails", "data is null")
//                    }
//                }
//            }
//        }
    }


    fun callCheckoutApi() {
        progressDialog = ProgressDialog.show(
                this, resources.getString(R.string.app_name),
                getString(R.string.datadownloading)
        )
        progressDialog.setIcon(getDrawable(R.drawable.greenyfreshlogo))

        val call = apiInterface?.checkout("application/json", "Bearer " +
                validate!!.RetriveSharepreferenceString(Utils.Token)!!)
        call?.enqueue(object : Callback<RegistrationResponse> {
            override fun onFailure(call: Call<RegistrationResponse>, t: Throwable) {
                t.printStackTrace()
                progressDialog.dismiss()
            }

            override fun onResponse(
                    call: Call<RegistrationResponse>, response: Response<RegistrationResponse>
            ) {
                when (response.code()) {

                    200 -> {
                        if (response!!.body()?.status!!.equals("true", ignoreCase = true)) {
                            progressDialog.dismiss()

                            totalPrice = response!!.body()?.final_price.toString()
                            text_total_price.setText(response!!.body()?.final_price.toString())
                            tv_taxchrgs.setText(response!!.body()?.final_tax.toString())
                            tv_subtotal.setText(response!!.body()?.subtotal!!)
                            tv_delchrgs.setText(response!!.body()?.deliverycharges!!)
                            //snackBar(this@CartActivity, response!!.body().message)

                        } else {
                            progressDialog.dismiss()
                            //snackBar(this@CartActivity, response!!.body().message)
                        }
                    }
                    500 -> {
                        progressDialog.dismiss()
                    }
                }

            }

        })


    }

    fun startPayment() {
        //checkout.setKeyID("<YOUR_KEY_ID>");
        val totalpay: Double? = totalPrice?.toDoubleOrNull()
        val findResult = (totalpay?.times(100))
        val userEmail = validate!!.RetriveSharepreferenceString(Fullname)
        val userPhoneNumber = validate!!.RetriveSharepreferenceString(Mobileno)

        /**
         * Instantiate Checkout
         */
        val checkout = Checkout()
        /**
         * Set your logo here
         */
        //checkout.setImage(R.drawable.logo);
        /**
         * Reference to current activity
         */
        val activity: Activity = this
        /**
         * Pass your payment options to the Razorpay Checkout as a JSONObject
         */
        try {
            val options = JSONObject()
            options.put("name", "LinkMyParking")
            options.put("description", "Parking Payment")
            options.put("image", "https://s3.amazonaws.com/rzp-mobile/images/rzp.png")
            //            options.put("order_id", "order_DBJOWzybf0sJbb");//from response of step 3.
//            options.put("theme.color", "#3399cc");
            options.put("currency", "INR")
            options.put("amount", findResult) //pass amount in currency subunits
            //if (!userEmail.equals("")){
            options.put("prefill.email", "greenyfreshindia@gmail.com")
            // }
            if (userPhoneNumber != "") {
                options.put("prefill.contact", userPhoneNumber)
            }
            checkout.open(activity, options)
        } catch (e: Exception) {
            //Log.e(TAG, "Error in starting Razorpay Checkout$e", e)
        }
    }

//    fun onPaymentSuccess(s: String?) {
//        Toast.makeText(this, "Payment Successfull", Toast.LENGTH_SHORT).show()
//         serviceOnlinepaymentSuccess(s,payingAmount);
//    }
//
//    fun onPaymentError(i: Int, s: String?) {
//        Toast.makeText(this, "Payment Failure", Toast.LENGTH_SHORT).show()
//    }

    override fun onPaymentError(s: Int, p1: String?) {

    }

    override fun onPaymentSuccess(s: String?) {
        callPlaceOrderListApi()
    }


    private fun showSuccessDialog() {
        val dialog = Dialog(this)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(false)
        dialog.setContentView(R.layout.dialog_submit_successfully_layout)


        val yesBtn = dialog.findViewById(R.id.tv_ok) as TextView

        yesBtn.setOnClickListener {
            val intent = Intent(this@OrderPlaceActivity, CustomerDashboard::class.java)
            startActivity(intent)
            finish()
            dialog.dismiss()
        }

        dialog.show()

    }
}

