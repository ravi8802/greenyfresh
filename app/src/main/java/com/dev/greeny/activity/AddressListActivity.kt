package com.dev.greeny.activity

import android.app.Activity
import android.app.Dialog
import android.app.ProgressDialog
import android.content.Intent
import android.content.Intent.getIntent
import android.os.Bundle
import android.view.Window
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.dev.greeny.R
import com.dev.greeny.adapter.DeliveryAddressAdapter
import com.dev.greeny.baen.OrderListResponse
import com.dev.greeny.service.request.Api
import com.dev.greeny.service.request.ApiClientConnection
import com.dev.greeny.service.response.RegistrationResponse
import com.dev.greeny.utility.Utils
import com.dev.greeny.utility.Validate
import com.dev.greeny.utility.snackBar
import kotlinx.android.synthetic.main.activity_user_address_list.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class AddressListActivity : AppCompatActivity(), DeliveryAddressAdapter.CartItemsInterface {
    internal lateinit var progressDialog: ProgressDialog
    var apiInterface: Api? = null
    var validate: Validate? = null
    private var addressAdapter: DeliveryAddressAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user_address_list)
        //setToolbar(this, toolbar, tvTitle, getString(R.string.del_add))

        validate = Validate(this)
        apiInterface = ApiClientConnection.Companion.instance.createApiInterface()

        linear_add_address.setOnClickListener {
            val intent = Intent(this, AddNewAddressActivity::class.java)
            //intent.putExtra("totalPrice", totalPrice)
            startActivity(intent)
        }

        image_back.setOnClickListener {
            finish()
        }
        callAddressListApi()
    }

    private fun FillData(list: List<OrderListResponse>?) {
        rvaddressList.layoutManager = LinearLayoutManager(this)
        addressAdapter = DeliveryAddressAdapter(this, list)
        rvaddressList.adapter = addressAdapter
        addressAdapter?.onCartItem(this@AddressListActivity)
        addressAdapter?.onSelectCartItem(this@AddressListActivity)


    }

    fun callAddressListApi() {
        progressDialog = ProgressDialog.show(
                this, resources.getString(R.string.app_name),
                getString(R.string.datadownloading)
        )
        progressDialog.setIcon(getDrawable(R.drawable.greenyfreshlogo))

        val call = apiInterface?.addressList("application/json", "Bearer " +
                validate!!.RetriveSharepreferenceString(Utils.Token)!!)
        call?.enqueue(object : Callback<RegistrationResponse> {
            override fun onFailure(call: Call<RegistrationResponse>, t: Throwable) {
                t.printStackTrace()
                progressDialog.dismiss()
            }

            override fun onResponse(
                    call: Call<RegistrationResponse>, response: Response<RegistrationResponse>
            ) {
                when (response.code()) {

                    200 -> {
                        if (response!!.body()?.status!!.equals("true", ignoreCase = true)) {
                            progressDialog.dismiss()
                            val list = response!!.body()?.addressList
                            FillData(list)
                            //snackBar(this@CartActivity, response!!.body().message)

                        } else {
                            progressDialog.dismiss()
                            //snackBar(this@CartActivity, response!!.body().message)
                        }
                    }
                    500 -> {
                        progressDialog.dismiss()
                    }
                }

            }

        })


    }


    //Api call for Delete Address
    fun callDeleteAddressApi(id: String?) {
        progressDialog = ProgressDialog.show(
                this, resources.getString(R.string.app_name),
                getString(R.string.datadownloading)
        )
        progressDialog.setIcon(getDrawable(R.drawable.greenyfreshlogo))

        val call = apiInterface?.deleteAddress(id, "application/json", "Bearer " +
                validate!!.RetriveSharepreferenceString(Utils.Token)!!)
        call?.enqueue(object : Callback<RegistrationResponse> {
            override fun onFailure(call: Call<RegistrationResponse>, t: Throwable) {
                t.printStackTrace()
                progressDialog.dismiss()
            }

            override fun onResponse(
                    call: Call<RegistrationResponse>, response: Response<RegistrationResponse>
            ) {
                when (response.code()) {

                    200 -> {
                        if (response!!.body()?.status!!.equals("true", ignoreCase = true)) {
                            progressDialog.dismiss()
                            callAddressListApi()
                            snackBar(this@AddressListActivity, response!!.body().message)

                        } else {
                            progressDialog.dismiss()
                            snackBar(this@AddressListActivity, response!!.body().message)
                        }
                    }
                    500 -> {
                        progressDialog.dismiss()
                    }
                }

            }

        })


    }

    override fun onCallBackCartItem(cartData: OrderListResponse) {
        showDeleteDialog(cartData?.id)

    }

    override fun onSelectCallBackCartItem(cartSelectData: OrderListResponse) {
        val intent = Intent(this@AddressListActivity, OrderPlaceActivity::class.java)
        intent.putExtra("from","Address")
        intent.putExtra("full_name",cartSelectData.full_name)
        intent.putExtra("phone",cartSelectData.phone)
        intent.putExtra("address",cartSelectData.address)
        intent.putExtra("city",cartSelectData.city)
        intent.putExtra("state",cartSelectData.state)
        intent.putExtra("zipcode",cartSelectData.postcode)
        startActivity(intent)
        finish()
    }

    private fun showDeleteDialog(id: Int?) {
        val dialog = Dialog(this)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(false)
        dialog.setContentView(R.layout.dialog_delete_address_layout)
        val body = dialog.findViewById(R.id.header) as TextView
        body.text = "Do you really want to remove this address?"
        val yesBtn = dialog.findViewById(R.id.tv_ok) as TextView
        val noBtn = dialog.findViewById(R.id.tv_cancel) as TextView
        yesBtn.setOnClickListener {
            callDeleteAddressApi(id.toString());
            dialog.dismiss()
        }
        noBtn.setOnClickListener { dialog.dismiss() }
        dialog.show()

    }
}