package com.dev.greeny.activity

import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.AdapterView
import com.dev.greeny.R
import com.dev.greeny.constant.Constants
import com.dev.greeny.service.request.Api
import com.dev.greeny.service.request.ApiClientConnection
import com.dev.greeny.service.response.RegistrationResponse
import com.dev.greeny.utility.*
import com.dev.greeny.utility.ParamEnumcollection.ParamEnum
import com.tsongkha.spinnerdatepicker.DatePicker
import com.tsongkha.spinnerdatepicker.SpinnerDatePickerDialogBuilder
import kotlinx.android.synthetic.main.activity_sign_up.*
import kotlinx.android.synthetic.main.layout_toolbar.*
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

class SignUpActivity : AppCompatActivity(), View.OnClickListener, com.tsongkha.spinnerdatepicker.DatePickerDialog.OnDateSetListener {
    internal lateinit var progressDialog: ProgressDialog
    var apiInterface: Api? = null
    var validate: Validate? = null

    private var day = ""
    private var month = ""
    private var years = ""
    private var yyyy: Int = 0
    private var mm: Int = 0
    private var dd: Int = 0



    private val genderSelectListener = object : AdapterView.OnItemSelectedListener {
        override fun onNothingSelected(p0: AdapterView<*>?) {
        }

        override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
            if (p2 == 0)
                return
            tvgender.text = p0?.getItemAtPosition(p2).toString()

        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_up)
        setToolbar(this, toolbar, tvTitle, getString(R.string.signup))
        validate = Validate(this)
        apiInterface = ApiClientConnection.Companion.instance.createApiInterface()
        initClickListener()
    }

    private fun initClickListener() {
        btnSignUp.setOnClickListener(this)
        tvdob.setOnClickListener(this)
        tvgender.setOnClickListener(this)
        setSpinner(this,spingender,DataGenerator.getgender())
        spingender.onItemSelectedListener = genderSelectListener
    }

    override fun onClick(p0: View?) {
        when (p0) {
            btnSignUp -> {
                if (validation())
                    serviceSignUp()
                    //dispatchOTPVerificationAct()
            }
            tvgender -> spingender.performClick()
            tvdob ->  showDatePickerDialog()

        }
    }

    private fun validation(): Boolean {
        when {
            edtFullName.text.toString().isEmpty() -> {
                snackBar(this, "Please enter full name")
                return false
            }
            edtMailSignUp.text.toString().isEmpty() -> {
                snackBar(this, "Please enter email id")
                return false
            }
            edtPhoneNumber.text.toString().isEmpty() -> {
                snackBar(this, "Please enter phone number")
                return false
            }
            tvdob.text.toString().isEmpty() -> {
                snackBar(this, "Please enter DOB")
                return false
            }
            edtAddress.text.toString().isEmpty() -> {
                snackBar(this, "Please enter address")
                return false
            }
            edtPasswordSignUp.text.toString().isEmpty() -> {
                snackBar(this, "Please enter password")
                return false
            }
            edtConfirmPass.text.toString().isEmpty() -> {
                snackBar(this, "Please enter confirm password")
                return false
            }
            edtPasswordSignUp.text.toString() != edtConfirmPass.text.toString() -> {
                snackBar(this, "Password and confirm password should be match")
                return false
            }
            tvgender.text.toString().isEmpty() -> {
                snackBar(this, "Please select Gender")
                return false
            }
            !cbTermsCondition.isChecked -> {
                snackBar(this, "Please accept terms and conditions")
                return false
            }
            else -> return true
        }
    }

    private fun dispatchOTPVerificationAct() {
        val intent = Intent(this, OTPVerificationActivity::class.java)
        intent.putExtra(ParamEnum.COUNTRY_CODE.theValue(), countryCodePicker.formattedFullNumber)
        intent.putExtra(ParamEnum.NUMBER.theValue(), edtPhoneNumber.text.toString())
        intent.putExtra(ParamEnum.NAME.theValue(), edtFullName.text.toString())
        intent.putExtra(ParamEnum.EMAIL.theValue(), edtMailSignUp.text.toString())
        intent.putExtra(ParamEnum.DOB.theValue(),tvdob.text.toString())
        intent.putExtra(ParamEnum.GENDER.theValue(), tvgender.text.toString())
        intent.putExtra(ParamEnum.Stateid.theValue(), "0")
        intent.putExtra(ParamEnum.Districtid.theValue(), "0")
        intent.putExtra(ParamEnum.ADDRESS.theValue(), edtAddress.text.toString())
        intent.putExtra(ParamEnum.CUSTOMER.theValue(), "2")
        intent.putExtra(ParamEnum.PASSWORD.theValue(), edtPasswordSignUp.text.toString())
        intent.putExtra(ParamEnum.CONFIRM_PASSWORD.theValue(), edtConfirmPass.text.toString())
        intent.putExtra(ParamEnum.GST_number.theValue(), "")
        intent.putExtra(ParamEnum.Categoryid.theValue(), "0")
        startActivity(intent)
    }



    private fun showDatePickerDialog() {
        val calendar = Calendar.getInstance()
        val mdformat = SimpleDateFormat("yyyy/MM/dd")
        val currentDate = mdformat.format(calendar.time)
        val currentDateArray =
                currentDate.split("/".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
        yyyy = Integer.parseInt(currentDateArray[0])
        mm = Integer.parseInt(currentDateArray[1]) - 1
        dd = Integer.parseInt(currentDateArray[2])

        showDate(dd, mm, yyyy, R.style.NumberPickerStyle)
    }

    private fun showDate(dayOfMonth: Int, monthOfYear: Int, year: Int, spinnerTheme: Int) {
        SpinnerDatePickerDialogBuilder()
                .context(this)
                .callback(this)
                .spinnerTheme(spinnerTheme)
                .defaultDate(year, monthOfYear, dayOfMonth)
                .maxDate(yyyy, mm, dd)
                .build()
                .show()
    }

    override fun onDateSet(view: DatePicker?, year: Int, monthOfYear: Int, dayOfMonth: Int) {
        day = dayOfMonth.toString()
        month = monthOfYear.toString()
        years = year.toString()

        val calenderDate = dayOfMonth.toString() + "-" + (monthOfYear + 1) + "-" + year
        val inputFormate = SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH)
        val outputFormate = SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH)
        var date: Date? = null
        try {
            date = inputFormate.parse(calenderDate)
        } catch (e: ParseException) {
            e.printStackTrace()
        }

        val formattedDate = outputFormate.format(date)
        tvdob.text = formattedDate
    }



    //Signup Api Call..//
    fun serviceSignUp() {
        val sharedPreferences = getSharedPreferences("Greeny", Context.MODE_PRIVATE)
        var deviceToken = sharedPreferences.getString(Constants.kDEVICETOKEN, "")
        if(deviceToken.equals("")){
            deviceToken = "dbdvsnbfdsbjbsddvvhvfhdjhjbdjhmjhvhdchhvmhvmzhvmjdbvjhdb"
        }
        Log.e("SIGNIUP VEE"," -> Test > "+deviceToken)
        if (CommonUtilities.isNetworkAvailable(this)) {
            progressDialog = ProgressDialog.show(
                    this@SignUpActivity, resources.getString(R.string.app_name),
                    getString(R.string.datauploading)
            )

            val call = apiInterface?.register( validate!!.RetriveSharepreferenceString(Utils.ReferalCode)!!,edtFullName.text.toString(), edtMailSignUp.text.toString(), edtPhoneNumber.text.toString(),
                    tvdob.text.toString(), edtAddress.text.toString(), tvgender.text.toString(), edtPasswordSignUp.text.toString(), "91","android",deviceToken)
            call?.enqueue(object : Callback<RegistrationResponse> {
                override fun onFailure(call: Call<RegistrationResponse>, t: Throwable) {
                    t.printStackTrace()
                    progressDialog.dismiss()
                }

                override fun onResponse(
                        call: Call<RegistrationResponse>, response: Response<RegistrationResponse>
                ) {
                    when (response.code()) {
                        200 -> {
                            if (response!!.body()?.status!!.equals("true",ignoreCase = true)) {
                                progressDialog.dismiss()
                                snackBar(this@SignUpActivity, response!!.body().message)
                                validate!!.SaveSharepreferenceString(Utils.Token, response!!.body()?.signupdata!!.token!!)
                                validate!!.SaveSharepreferenceString(Utils.Fullname, response!!.body()?.signupdata!!.userdata!!.name!!)
                                validate!!.SaveSharepreferenceString(Utils.UserId, response!!.body()?.signupdata!!.userdata!!.user_id!!)
                                validate!!.SaveSharepreferenceString(Utils.Mobileno, response!!.body()?.signupdata!!.userdata!!.phone!!)
                                //validate!!.SaveSharepreferenceInt(Utils.userid, response!!.body()?.signupdata!!.user_id!!)
//                                validate!!.SaveSharepreferenceInt(Utils.Categoryid, response!!.body()?.signupdata!!.category_id!!)
//                                validate!!.SaveSharepreferenceString(Utils.Token, response!!.body()?.signupdata!!.token!!)
                                // validate!!.SaveSharepreferenceString(Utils.Password, response!!.body()?.signupdata!!.password!!)
                                // validate!!.SaveSharepreferenceString(Utils.Mobileno, response!!.body()?.signupdata!!.phone!!)
                                val intent = Intent(this@SignUpActivity, CustomerDashboard::class.java)
                                startActivity(intent)
                                finish()
                                //   intent.setFlags()
                                //snackBar(this@OTPVerificationActivity, response!!.body().message)
                                //} else {
                                //progressDialog.dismiss()


                            } else {
                                progressDialog.dismiss()
                                snackBar(this@SignUpActivity, response!!.body().message)

                            }
                        }
                        411 -> {
                            progressDialog.dismiss()
                            try {
                                val errorBody = response.errorBody().string()
                                var jsonObject = JSONObject(errorBody.trim())
                                val errorr = jsonObject.getString("message")
                                snackBar(this@SignUpActivity, errorr)

                            } catch (e: JSONException) {
                                e.printStackTrace();
                            }


                        }
                    }

                }

            })
        } else {
            snackBar(this, getString(R.string.no_internet))
        }

    }
}
