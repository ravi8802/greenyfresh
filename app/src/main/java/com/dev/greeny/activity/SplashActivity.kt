package com.dev.greeny.activity

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import com.dev.greeny.R
import com.dev.greeny.constant.Constants.kDEVICETOKEN
import com.dev.greeny.sharedpreferences.SPreferenceKey
import com.dev.greeny.sharedpreferences.SharedPreferenceWriter
import com.dev.greeny.utility.Utils
import com.dev.greeny.utility.Validate
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.iid.InstanceIdResult
import java.util.*


class SplashActivity : AppCompatActivity() {
  var validate: Validate?=null
  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_splash)
    // transparent status bar
    val w = window // in Activity's onCreate() for instance
    w.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS)
    validate= Validate(this)
    getDeviceToken()
    Handler().postDelayed(
            {
              //for deep linking
              if (intent != null) {
                val appLinkIntent = intent
                val appLinkData = appLinkIntent.data
                if (appLinkData != null) {
                  val url = appLinkData.toString()
                  try {
                    val id = url.substring(url.lastIndexOf("=") + 1)
                    val intent = Intent(this@SplashActivity, SignUpActivity::class.java)
                    intent.putExtra("", "")
                    startActivity(intent)
                    finish()
                  } catch (e: java.lang.Exception) {
                    e.printStackTrace()
                  }
                } else {
                  dispatchHome()
                }
              } else {
                dispatchHome()
              }
            }, 2000
    )
  }

  private fun dispatchHome() {
    if(validate!!.RetriveSharepreferenceString(Utils.Token).isNullOrEmpty()){
      val intent = Intent(this@SplashActivity, LoginActivity::class.java)
      startActivity(intent)
      finish()
    }else{
      val intent = Intent(this@SplashActivity, CustomerDashboard::class.java)
      startActivity(intent)
      finish()
    }
  }


  private fun getDeviceToken() {

    val thread = object : Thread() {
      override fun run() {
        Log.e(">>>>>>>>>>>>>>", "thred IS running")
        try {

          var sharedPreferences = getSharedPreferences("Greeny", Context.MODE_PRIVATE)

          var deviceToken: String? = sharedPreferences.getString(kDEVICETOKEN, "")
          Log.e("FindSplach ", "Token->>  " + deviceToken)

          if (deviceToken!!.isEmpty()) {
            val token = FirebaseInstanceId.getInstance().token
            // String token = Settings.Secure.getString(getApplicationContext().getContentResolver(),
            // Settings.Secure.ANDROID_ID);
            Log.e("Generated Device Token", "-->" + token!!)
            if (token == null) {
              getDeviceToken()
            } else {
              var editor = sharedPreferences.edit()
              editor.putString(kDEVICETOKEN, token)
              editor.commit()
            }
          }
        } catch (e1: Exception) {
          e1.printStackTrace()
        }

        super.run()
      }
    }
    thread.start()
  }

  private fun getDeviceTokenOld() {
    val thread = object : Thread() {
      override fun run() {
        Log.e(">>>>>>>>>>>>>>", "thred IS  running")
        try {
          if (validate!!.RetriveSharepreferenceString(Utils.TOKEN).isNullOrEmpty()) {
            FirebaseInstanceId.getInstance().instanceId.addOnCompleteListener { task ->
              if (!task.isSuccessful) {
                return@addOnCompleteListener
              }
              val token = Objects.requireNonNull<InstanceIdResult>(task.result).token
              Log.e("Generated Device Token", "-->$token")
              if (token == null) {
                getDeviceToken()
              } else {
                validate!!.SaveSharepreferenceString(Utils.TOKEN,token)
              }
            }
          }
        } catch (e1: Exception) {
          e1.printStackTrace()
        }
        super.run()
      }
    }
    thread.start()
  }

  private fun dispatchMainActivity() {
    if (SharedPreferenceWriter.getInstance(this).getString(SPreferenceKey.TOKEN) != null && !SharedPreferenceWriter.getInstance(this).getString(SPreferenceKey.TOKEN).isEmpty()) {
      //for deep linking
      if (intent != null) {
        val appLinkIntent = intent
        val appLinkData: Uri? = appLinkIntent.data
        if (appLinkData != null) {
          val url: String = appLinkData.toString()
          try {
            val userId = url.substring(url.lastIndexOf("=") + 1)
            val intent = Intent(this@SplashActivity, MainActivity::class.java)
            //intent.putExtra(ParamEnum.FROM.theValue(), ParamEnum.CALL.theValue())
            //intent.putExtra(ParamEnum.ID.theValue(), doctorId)
            startActivity(intent)
            finish()
          } catch (e: java.lang.Exception) {
            e.printStackTrace()
          }
        } else {
          //dispatchHome()
        }
      } else {
        //dispatchHome()
      }
    } else {
      //dispatchHome()
    }
  }

}
