package com.dev.greeny.activity

import android.app.ProgressDialog
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import androidx.recyclerview.widget.LinearLayoutManager
import com.dev.greeny.R
import com.dev.greeny.adapter.ShopListingAdapter
import com.dev.greeny.entity.ShopListEntity
import com.dev.greeny.entity.mstdistrict
import com.dev.greeny.entity.mststate
import com.dev.greeny.service.request.Api
import com.dev.greeny.service.request.ApiClientConnection
import com.dev.greeny.service.response.RegistrationResponse
import com.dev.greeny.utility.*
import kotlinx.android.synthetic.main.cloths.*
import kotlinx.android.synthetic.main.layout_toolbar.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class FoodActivity : AppCompatActivity(),View.OnClickListener {
  internal lateinit var progressDialog: ProgressDialog
  var apiInterface: Api? = null
  var validate: Validate? = null
  var statelist: List<mststate>? = null
  var statecode: Int = 0
  var distecode: Int = 0
  var categoryid: Int = 0
  var distlist: List<mstdistrict>? = null
  private val stateSelectListener = object : AdapterView.OnItemSelectedListener {
    override fun onNothingSelected(p0: AdapterView<*>?) {
    }

    override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
      if (p2 == 0) {
        statecode = 0
        return
      } else {
        statecode = statelist?.get(p2-1)!!.state_id
        filldistrict(statecode)
        tvstate.text = p0?.getItemAtPosition(p2).toString()
      }


    }
  }
  private val districtSelectListener = object : AdapterView.OnItemSelectedListener {
    override fun onNothingSelected(p0: AdapterView<*>?) {
    }

    override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
      if (p2 == 0) {
        distecode = 0
        return
      } else {
        distecode = distlist?.get(p2-1)!!.district_id
        shoplistitem()
        tvdistrict.text = p0?.getItemAtPosition(p2).toString()
      }
    }
  }

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.cloths)
    setToolbar(this, toolbar, tvTitle, getString(R.string.shoplist))
    validate = Validate(this)
    apiInterface = ApiClientConnection.Companion.instance.createApiInterface()
    initClickListener()
    // shoplistitem()
  }
  private fun initClickListener() {
    tvstate.setOnClickListener(this)
    tvdistrict.setOnClickListener(this)
    //fillstate()
    spinsate.onItemSelectedListener = stateSelectListener
    spindistrict.onItemSelectedListener = districtSelectListener
  }

  override fun onClick(p0: View?) {
    when (p0) {

      tvstate -> spinsate.performClick()
      tvdistrict -> spindistrict.performClick()

    }
  }
  private fun FillData(list: List<ShopListEntity>?) {
    rvProductList.layoutManager = LinearLayoutManager(this)
    rvProductList.adapter = ShopListingAdapter(this, list)
  }

  fun shoplistitem() {
    progressDialog = ProgressDialog.show(
            this, resources.getString(R.string.app_name),
            getString(R.string.datadownloading)
    )
    //        progressDialog.setIcon(getDrawable(R.drawable.appicon))

    val call = apiInterface?.ShopList(statecode,distecode,
            validate!!.RetriveSharepreferenceInt(Utils.Categoryid)!!,
            validate!!.RetriveSharepreferenceString(Utils.Token)!!)
    call?.enqueue(object : Callback<RegistrationResponse> {
      override fun onFailure(call: Call<RegistrationResponse>, t: Throwable) {
        t.printStackTrace()
        progressDialog.dismiss()
      }

      override fun onResponse(
              call: Call<RegistrationResponse>, response: Response<RegistrationResponse>
      ) {
        when (response.code()) {

          200 -> {
            if (!response!!.body()?.shopList.isNullOrEmpty()) {
              progressDialog.dismiss()
              val list = response!!.body()?.shopList
              FillData(list)
              snackBar(this@FoodActivity, response!!.body().message)

            } else {
              progressDialog.dismiss()
              snackBar(this@FoodActivity, response!!.body().message)
            }
          }
          500 -> {
            progressDialog.dismiss()
          }
        }

      }

    })


  }
//    fun fillstate() {
//        progressDialog = ProgressDialog.show(
//                this@FoodActivity, resources.getString(R.string.app_name),
//                getString(R.string.datadownloading)
//        )
////        progressDialog.setIcon(getDrawable(R.drawable.appicon))
//
//        val call = apiInterface?.statelist()
//        call?.enqueue(object : Callback<RegistrationResponse> {
//            override fun onFailure(call: Call<RegistrationResponse>, t: Throwable) {
//                t.printStackTrace()
//                progressDialog.dismiss()
//            }
//
//            override fun onResponse(
//                    call: Call<RegistrationResponse>, response: Response<RegistrationResponse>
//            ) {
//                when (response.code()) {
//                    200 -> {
//                        if (!response!!.body()?.mststate.isNullOrEmpty()) {
//                            progressDialog.dismiss()
//                            statelist = response!!.body()?.mststate
//                            //   FillData(list)
//                            setstateSpinner(this@FoodActivity, spinsate, statelist)
//
//                        } else {
//                            progressDialog.dismiss()
//
//
//                        }
//                    }
//                }
//
//            }
//
//        })
//
//
//    }

  fun filldistrict(sid: Int?) {
//        progressDialog = ProgressDialog.show(
//                this@FoodActivity, resources.getString(R.string.app_name),
//                getString(R.string.datadownloading)
//        )
////        progressDialog.setIcon(getDrawable(R.drawable.appicon))
//
//        val call = apiInterface?.districtlist(sid)
//        call?.enqueue(object : Callback<RegistrationResponse> {
//            override fun onFailure(call: Call<RegistrationResponse>, t: Throwable) {
//                t.printStackTrace()
//                progressDialog.dismiss()
//            }
//
//            override fun onResponse(
//                    call: Call<RegistrationResponse>, response: Response<RegistrationResponse>
//            ) {
//                when (response.code()) {
//                    200 -> {
//                        if (!response!!.body()?.mstdistrict.isNullOrEmpty()) {
//                            progressDialog.dismiss()
//                            distlist = response!!.body()?.mstdistrict
//                            //   FillData(list)
//                            setdistrictSpinner(this@FoodActivity, spindistrict, distlist)
//
//                        } else {
//                            progressDialog.dismiss()
//
//
//                        }
//                    }
//                }
//
//            }
//
//        })
//

  }
  override fun onBackPressed() {
    val intent = Intent(this, CustomerDashboard::class.java)
    intent.flags = (Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
    startActivity(intent)
  }


}
