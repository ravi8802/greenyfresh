package com.dev.greeny.activity

import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.view.WindowManager
import com.dev.greeny.R

class DashboardActivity : AppCompatActivity(), View.OnClickListener {

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.fragmenthome)
    // transparent status bar
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
      val w = window // in Activity's onCreate() for instance
      w.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS)
    }
    initClickListener()

  }

  private fun initClickListener() {
//        linear_supplier.setOnClickListener(this)
//        //linear_buyer.setOnClickListener(this)
//        linear_business.setOnClickListener(this)
//        linear_supplychain.setOnClickListener(this)
  }

  override fun onClick(p0: View?) {
    when(p0){
      // linear_supplier,linear_buyer,linear_business,linear_supplychain -> startActivity(Intent(this,MainActivity::class.java))
    }
  }

}
