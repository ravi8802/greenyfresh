package com.dev.greeny.activity

import android.app.ProgressDialog
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.dev.greeny.R
import com.dev.greeny.adapter.ClothShopListingAdapter
import com.dev.greeny.adapter.WishListAdapter
import com.dev.greeny.baen.ProductDataResponce
import com.dev.greeny.service.request.Api
import com.dev.greeny.service.request.ApiClientConnection
import com.dev.greeny.service.response.RegistrationResponse
import com.dev.greeny.utility.Utils
import com.dev.greeny.utility.Validate
import com.dev.greeny.utility.setToolbar
import com.dev.greeny.utility.snackBar
import kotlinx.android.synthetic.main.activity_wish_list.*

import kotlinx.android.synthetic.main.layout_toolbar.*
import kotlinx.android.synthetic.main.layout_toolbar.img_cart
import kotlinx.android.synthetic.main.layout_toolbar.toolbar
import kotlinx.android.synthetic.main.layout_toolbar.tvCartCount
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class WishListActivity : AppCompatActivity(),WishListAdapter.LikeInterface {
    internal lateinit var progressDialog: ProgressDialog
    var apiInterface: Api? = null
    var validate: Validate? = null
    private var wishListAdapter: WishListAdapter? = null
    var itemId: String = ""

    override fun onCallLikeItem(propertyData: ProductDataResponce) {
        itemId = propertyData.id.toString()
        callLikeUnlikeApi()

    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_wish_list)

        setToolbar(this, toolbar, tvTitle, getString(R.string.myfav))
        img_cart.visibility = View.GONE
        tvCartCount.visibility = View.GONE

        validate = Validate(this)
        apiInterface = ApiClientConnection.Companion.instance.createApiInterface()

        callwishlistProductsApi()
    }

    private fun FillData(list: List<ProductDataResponce>?) {
        if (list != null && list.size > 0) {
            rvWishList.visibility = View.VISIBLE
            linear_no_data_wish.visibility = View.GONE
            rvWishList.layoutManager = LinearLayoutManager(this)
            wishListAdapter = WishListAdapter(this, list)
            rvWishList.adapter = wishListAdapter
            wishListAdapter?.onLikeItem(this@WishListActivity)
        }else{
            linear_no_data_wish.visibility = View.VISIBLE
            rvWishList.visibility = View.GONE
        }

    }

    fun callwishlistProductsApi() {
        progressDialog = ProgressDialog.show(
                this, resources.getString(R.string.app_name),
                getString(R.string.datadownloading)
        )
        //        progressDialog.setIcon(getDrawable(R.drawable.appicon))

        val call = apiInterface?.wishlistProducts("application/json", "Bearer " + validate!!.RetriveSharepreferenceString(Utils.Token))
        call?.enqueue(object : Callback<RegistrationResponse> {
            override fun onFailure(call: Call<RegistrationResponse>, t: Throwable) {
                t.printStackTrace()
                progressDialog.dismiss()
            }

            override fun onResponse(
                    call: Call<RegistrationResponse>, response: Response<RegistrationResponse>
            ) {
                when (response.code()) {

                    200 -> {
                        if (response!!.body()?.status!!.equals("true", ignoreCase = true)) {
                            progressDialog.dismiss()
                            val list = response!!.body()?.wishlistProductsList
                            FillData(list)


                            snackBar(this@WishListActivity, response!!.body().message)

                        } else {
                            progressDialog.dismiss()
                            snackBar(this@WishListActivity, response!!.body().message)
                        }
                    }
                    500 -> {
                        progressDialog.dismiss()
                    }
                }

            }

        })


    }

    //Like&Unlike Api hit
    fun callLikeUnlikeApi() {
        progressDialog = ProgressDialog.show(
                this@WishListActivity, resources.getString(R.string.app_name),
                getString(R.string.datadownloading)
        )
        progressDialog.setIcon(getDrawable(R.drawable.greenyfreshlogo))

        val call = apiInterface?.likeUnlike(itemId,"application/json", "Bearer " + validate!!.RetriveSharepreferenceString(Utils.Token))
        call?.enqueue(object : Callback<RegistrationResponse> {
            override fun onFailure(call: Call<RegistrationResponse>, t: Throwable) {
                t.printStackTrace()
                progressDialog.dismiss()
            }

            override fun onResponse(
                    call: Call<RegistrationResponse>, response: Response<RegistrationResponse>
            ) {
                when (response.code()) {
                    200 -> {
                        if (!response!!.body()?.status.isNullOrEmpty()) {
                            progressDialog.dismiss()



                        } else {
                            progressDialog.dismiss()


                        }
                    }
                }

            }

        })


    }
}