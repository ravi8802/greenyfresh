package com.dev.greeny.activity;

import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.dev.greeny.R;

public class WebBrowserActivity extends AppCompatActivity implements View.OnClickListener {
    private WebView webview;
    ProgressDialog progressdialog;
    private float m_downX;
    ImageView image_back;
    TextView heading;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_browser);
        image_back = (ImageView) findViewById(R.id.image_back);
        heading = (TextView) findViewById(R.id.heading);
        image_back.setOnClickListener(this);
        webview = (WebView) findViewById(R.id.webView);
        webview.getSettings().setJavaScriptEnabled(true);
        webview.setWebViewClient(new WebViewClient());
        webview.getSettings().setDomStorageEnabled(true);
        webview.getSettings().setLoadsImagesAutomatically(true);
        webview.getSettings().setJavaScriptEnabled(true);
        webview.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        webview.setWebViewClient(new MyBrowser());

        String from = getIntent().getStringExtra("url");
        if(from.equalsIgnoreCase("Term Condition")){
            heading.setText("Term and Condition");
            webview.loadUrl("http://greenyfresh.co.in/api_terms");
        }else if(from.equalsIgnoreCase("About Us")){
            heading.setText("About Us");
            webview.loadUrl("http://greenyfresh.co.in/api_about");
        }else if(from.equalsIgnoreCase("Contact Us")){
            heading.setText("Contact Us");
            webview.loadUrl("http://3.131.217.202/shopping/public/api_terms");
        }else if(from.equalsIgnoreCase("Privacy Policy")){
            heading.setText("Privacy Policy");
            webview.loadUrl("http://greenyfresh.co.in/api_privacy_policy");
        }




        progressdialog = new ProgressDialog(WebBrowserActivity.this);
        progressdialog.setMessage("Loading.....");

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.image_back:
                WebBrowserActivity.this.finish();
                break;
        }
    }

    private class MyBrowser extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
            if (!progressdialog.isShowing()) {
                progressdialog.show();
            }
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            if (progressdialog.isShowing()) {
                progressdialog.dismiss();
            }
        }

    }

    @Override
    protected void onPause() {
        super.onPause();
        webview.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        webview.onResume();
    }
}