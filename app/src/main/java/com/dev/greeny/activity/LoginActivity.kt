package com.dev.greeny.activity

import android.Manifest
import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import com.dev.greeny.R
import com.dev.greeny.constant.Constants
import com.dev.greeny.constant.Constants.kDEVICETOKEN
import com.dev.greeny.service.request.Api
import com.dev.greeny.service.request.ApiClientConnection
import com.dev.greeny.service.response.RegistrationResponse
import com.dev.greeny.sharedpreferences.SPreferenceKey
import com.dev.greeny.sharedpreferences.SharedPreferenceWriter
import com.dev.greeny.utility.*
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.layout_toolbar.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class LoginActivity : AppCompatActivity(), View.OnClickListener {

    internal lateinit var progressDialog: ProgressDialog
    var apiInterface: Api? = null
    var validate: Validate? = null
    internal var PERMISSION_ALL = 1
    internal var PERMISSIONS = arrayOf(
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.CAMERA

    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        getDeviceToken(this)
        validate = Validate(this)
        apiInterface = ApiClientConnection.Companion.instance.createApiInterface()
        setToolbar()
        initClickListener()
        //tvForgetPassword.setText(Html.fromHtml("&#x1F1EE;&#x1F1F3"))
    }

    private fun setToolbar() {
        setSupportActionBar(toolbar)
        tvTitle.text = getString(R.string.login)
        supportActionBar!!.setDisplayShowTitleEnabled(false)
    }

    private fun initClickListener() {
        btnLogin.setOnClickListener(this)
        tvSignUp.setOnClickListener(this)
        tvForgetPassword.setOnClickListener(this)
    }

    override fun onClick(p0: View?) {
        when(p0){
           // btnLogin -> serviceLogin()
            tvForgetPassword -> startActivity(Intent(this,ForgotPasswordActivity::class.java))
            tvSignUp -> startActivity(Intent(this,SignUpActivity::class.java))
            btnLogin -> {
                if (edtphone.text.toString().isEmpty() || edtPasswordLogin.text.toString().isEmpty()) {
                    Toast.makeText(applicationContext,"Enter the phone number and pwd",Toast.LENGTH_SHORT).show()

                } else {
                    serviceLogin()
                }
            }
//            tvSignUp -> startActivity(Intent(this,SignUpActivity::class.java))
//            btnLogin ->   {val intent = Intent(this@LoginActivity, CustomerDashboard::class.java)
//                startActivity(intent)}
        }
    }
    override fun onResume() {
        super.onResume()
        if (!hasPermissions(this, *PERMISSIONS)) {
            ActivityCompat
                    .requestPermissions(this, PERMISSIONS, PERMISSION_ALL)
        }

    }
    fun hasPermissions(context: Context?, vararg permissions: String): Boolean {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null) {
            for (permission in permissions) {
                if (ActivityCompat.checkSelfPermission(
                                context,
                                permission
                        ) != PackageManager.PERMISSION_GRANTED
                ) {
                    return false
                }
            }
        }
        return true
    }

    fun serviceLogin() {
        val sharedPreferences = getSharedPreferences("Greeny", Context.MODE_PRIVATE)
        var deviceToken = sharedPreferences.getString(kDEVICETOKEN, "")
        if(deviceToken.equals("")){
            deviceToken = "dbdvsnbfdsbjbsddvvhvfhdjhjbdjhmjhvhdchhvmhvmzhvmjdbvjhdb"
        }
        if (CommonUtilities.isNetworkAvailable(this)) {
            progressDialog = ProgressDialog.show(
                    this@LoginActivity, resources.getString(R.string.app_name),
                  getString(R.string.datakoading)
            )

            val call = apiInterface?.login(edtphone.text.toString(),edtPasswordLogin.text.toString(),"android",deviceToken)
            call?.enqueue(object : Callback<RegistrationResponse> {
                override fun onFailure(call: Call<RegistrationResponse>, t: Throwable) {
                    t.printStackTrace()
                    progressDialog.dismiss()
                }

                override fun onResponse(
                        call: Call<RegistrationResponse>, response: Response<RegistrationResponse>
                ) {
                    when (response.code()) {
                        200 -> {
                            if (response!!.body()?.status!!.equals("true",ignoreCase = true)) {
                                progressDialog.dismiss()
                                //validate!!.SaveSharepreferenceInt(Utils.userid, response!!.body()?.logindata!!.userdata?.user_id!!)
//                               // validate!!.SaveSharepreferenceInt(Utils.Categoryid, response!!.body()?.logindata!!.category_id!!)
                                validate!!.SaveSharepreferenceString(Utils.Token, response!!.body()?.logindata!!.token!!)
                                validate!!.SaveSharepreferenceString(Utils.Fullname, response!!.body()?.logindata!!.userdata!!.name!!)
                                validate!!.SaveSharepreferenceString(Utils.UserId, response!!.body()?.logindata!!.userdata!!.user_id!!)
                                validate!!.SaveSharepreferenceString(Utils.email, response!!.body()?.logindata!!.userdata!!.email!!)
                                validate!!.SaveSharepreferenceString(Utils.Mobileno, response!!.body()?.logindata!!.userdata!!.phone!!)
                                validate!!.SaveSharepreferenceString(Utils.ReferalCode, response!!.body()?.logindata!!.userdata!!.referal_code!!)
                                SharedPreferenceWriter.getInstance(this@LoginActivity).writeStringValue("token",response.body().logindata!!.token);
                                SharedPreferenceWriter.getInstance(this@LoginActivity).writeStringValue("user_mail",response.body().logindata!!.email);
//                                val intent = Intent(this@LoginActivity, CustomerDashboard::class.java)
//                                startActivity(intent)checksum
//                                //   intent.setFlags()
                                snackBar(this@LoginActivity, response!!.body().message)
                                val intent = Intent(this@LoginActivity, CustomerDashboard::class.java)
                                startActivity(intent)
                            finish()
                            } else {
                                progressDialog.dismiss()
                                snackBar(this@LoginActivity, response!!.body().message)

                            }
                        }
                    }

                }

            })
        } else {
            snackBar(this, getString(R.string.no_internet))
        }

    }

}
