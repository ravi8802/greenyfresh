package com.dev.greeny.activity

import android.app.ProgressDialog
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import androidx.recyclerview.widget.LinearLayoutManager
import com.dev.greeny.R
import com.dev.greeny.adapter.ClothShopListingAdapter
import com.dev.greeny.baen.ProductDataResponce
import com.dev.greeny.entity.mstdistrict
import com.dev.greeny.entity.mststate
import com.dev.greeny.service.request.Api
import com.dev.greeny.service.request.ApiClientConnection
import com.dev.greeny.service.response.RegistrationResponse
import com.dev.greeny.utility.*
import kotlinx.android.synthetic.main.cart_activity.*
import kotlinx.android.synthetic.main.cloths.*
import kotlinx.android.synthetic.main.layout_toolbar.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ClothsActivity : AppCompatActivity(), View.OnClickListener, ClothShopListingAdapter.AddCardInterface,ClothShopListingAdapter.LikeInterface {
    internal lateinit var progressDialog: ProgressDialog
    private var clothShopAdapter: ClothShopListingAdapter? = null
    var apiInterface: Api? = null
    var validate: Validate? = null
    var statelist: List<mststate>? = null
    var proId: String = ""
    var itemId: String = ""
    var statecode: Int = 0
    var distecode: Int = 0
    var categoryid: Int = 0
    var selectitem: String = ""
    var distlist: List<mstdistrict>? = null
    private val stateSelectListener = object : AdapterView.OnItemSelectedListener {
        override fun onNothingSelected(p0: AdapterView<*>?) {
        }

        override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
            if (p2 == 0) {
                statecode = 0
                return
            } else {
                statecode = statelist?.get(p2 - 1)!!.state_id
                //callAddtocartApi()
                tvstate.text = p0?.getItemAtPosition(p2).toString()
            }


        }
    }
    private val districtSelectListener = object : AdapterView.OnItemSelectedListener {
        override fun onNothingSelected(p0: AdapterView<*>?) {
        }

        override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
            if (p2 == 0) {
                distecode = 0
                return
            } else {
                distecode = distlist?.get(p2 - 1)!!.district_id
                // shoplistitem()
                tvdistrict.text = p0?.getItemAtPosition(p2).toString()
            }
        }
    }

    override fun onCallBackAddCardItem(propertyData: List<ProductDataResponce>,position:Int) {
        proId = propertyData.get(position).id.toString()
        callAddtocartApi(propertyData,position)

    }

    override fun onCallLikeItem(propertyData: ProductDataResponce) {
        itemId = propertyData.id.toString()
        callLikeUnlikeApi()

    }



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.cloths)
        setToolbar(this, toolbar, tvTitle, getString(R.string.shoplist))
        validate = Validate(this)
        apiInterface = ApiClientConnection.Companion.instance.createApiInterface()
        initClickListener()

        if (validate!!.RetriveSharepreferenceInt(Utils.Categoryid) == 1) {
            selectitem = "1"
            //FillDataduommy()
        } else if (validate!!.RetriveSharepreferenceInt(Utils.Categoryid) == 2) {
            selectitem = "3"

            //  FillDataduommymasale()
        } else if (validate!!.RetriveSharepreferenceInt(Utils.Categoryid) == 3) {
            selectitem = "2"
            //FillDataduommyfruit()
        }
        callAllProductslistApi()
    }

    private fun initClickListener() {
        tvstate.setOnClickListener(this)
        tvdistrict.setOnClickListener(this)
        //fillstate()
        spinsate.onItemSelectedListener = stateSelectListener
        spindistrict.onItemSelectedListener = districtSelectListener
    }

    override fun onClick(p0: View?) {
        when (p0) {

            tvstate -> spinsate.performClick()
            tvdistrict -> spindistrict.performClick()

        }
    }

    private fun FillData(list: List<ProductDataResponce>?) {
        if (list != null && list.size > 0) {
            linear_no_data_cloth.visibility = View.GONE
            rvProductList.layoutManager = LinearLayoutManager(this)
            clothShopAdapter = ClothShopListingAdapter(this, list)
            rvProductList.adapter = clothShopAdapter
            clothShopAdapter?.onAddCard(this@ClothsActivity)
            clothShopAdapter?.onLikeItem(this@ClothsActivity)
        }else{
            rvProductList.visibility= View.GONE
            linear_no_data_cloth.visibility = View.VISIBLE
        }

    }

    //product list showing Api
    fun callAllProductslistApi() {
        progressDialog = ProgressDialog.show(
                this, resources.getString(R.string.app_name),
                getString(R.string.datadownloading))
        progressDialog.setIcon(getDrawable(R.drawable.greenyfreshlogo))

        val call = apiInterface?.productsbycategory(selectitem,validate!!.RetriveSharepreferenceString(Utils.UserId))
        call?.enqueue(object : Callback<RegistrationResponse> {
            override fun onFailure(call: Call<RegistrationResponse>, t: Throwable) {
                t.printStackTrace()
                progressDialog.dismiss()
            }

            override fun onResponse(
                    call: Call<RegistrationResponse>, response: Response<RegistrationResponse>
            ) {
                when (response.code()) {

                    200 -> {
                        if (response!!.body()?.status!!.equals("true", ignoreCase = true)) {
                            progressDialog.dismiss()
                            val list = response!!.body()?.dataList
                            FillData(list)
                            //snackBar(this@ClothsActivity, response!!.body().message)

                        } else {
                            progressDialog.dismiss()
                            //snackBar(this@ClothsActivity, response!!.body().message)
                        }
                    }
                    500 -> {
                        progressDialog.dismiss()
                    }
                }

            }

        })


    }

    //AddtoCard Api hit
    fun callAddtocartApi(propertyData: List<ProductDataResponce>,position:Int) {
        progressDialog = ProgressDialog.show(
                this@ClothsActivity, resources.getString(R.string.app_name),
                getString(R.string.datadownloading)
        )
        progressDialog.setIcon(getDrawable(R.drawable.greenyfreshlogo))

        val call = apiInterface?.addtocart("application/json", "Bearer " + validate!!.RetriveSharepreferenceString(Utils.Token), proId, "1")
        call?.enqueue(object : Callback<RegistrationResponse> {
            override fun onFailure(call: Call<RegistrationResponse>, t: Throwable) {
                t.printStackTrace()
                progressDialog.dismiss()
            }

            override fun onResponse(
                    call: Call<RegistrationResponse>, response: Response<RegistrationResponse>
            ) {
                when (response.code()) {
                    200 -> {
                        if (!response!!.body()?.status.isNullOrEmpty()) {
                            progressDialog.dismiss()
                            //callAllProductslistApi()
                            //   FillData(list)
                            propertyData.get(position).cart="yes"
                            clothShopAdapter?.updatedata(propertyData)

                        } else {
                            progressDialog.dismiss()


                        }
                    }
                }

            }

        })


    }

    //Like&Unlike Api hit
    fun callLikeUnlikeApi() {
        progressDialog = ProgressDialog.show(
                this@ClothsActivity, resources.getString(R.string.app_name),
                getString(R.string.datadownloading)
        )
        progressDialog.setIcon(getDrawable(R.drawable.greenyfreshlogo))

        val call = apiInterface?.likeUnlike(itemId,"application/json", "Bearer " + validate!!.RetriveSharepreferenceString(Utils.Token))
        call?.enqueue(object : Callback<RegistrationResponse> {
            override fun onFailure(call: Call<RegistrationResponse>, t: Throwable) {
                t.printStackTrace()
                progressDialog.dismiss()
            }

            override fun onResponse(
                    call: Call<RegistrationResponse>, response: Response<RegistrationResponse>
            ) {
                when (response.code()) {
                    200 -> {
                        if (!response!!.body()?.status.isNullOrEmpty()) {
                            progressDialog.dismiss()
                            callAllProductslistApi()
                            //   FillData(list)


                        } else {
                            progressDialog.dismiss()


                        }
                    }
                }

            }

        })


    }

//    fun fillstate() {
//        progressDialog = ProgressDialog.show(
//                this@ClothsActivity, resources.getString(R.string.app_name),
//                getString(R.string.datadownloading)
//        )
////        progressDialog.setIcon(getDrawable(R.drawable.appicon))
//
//        val call = apiInterface?.statelist()
//        call?.enqueue(object : Callback<RegistrationResponse> {
//            override fun onFailure(call: Call<RegistrationResponse>, t: Throwable) {
//                t.printStackTrace()
//                progressDialog.dismiss()
//            }
//
//            override fun onResponse(
//                    call: Call<RegistrationResponse>, response: Response<RegistrationResponse>
//            ) {
//                when (response.code()) {
//                    200 -> {
//                        if (!response!!.body()?.mststate.isNullOrEmpty()) {
//                            progressDialog.dismiss()
//                            statelist = response!!.body()?.mststate
//                            //   FillData(list)
//                            setstateSpinner(this@ClothsActivity, spinsate, statelist)
//
//                        } else {
//                            progressDialog.dismiss()
//
//
//                        }
//                    }
//                }
//
//            }
//
//        })
//
//
//    }


    override fun onBackPressed() {
        val intent = Intent(this, CustomerDashboard::class.java)
        intent.flags = (Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
        startActivity(intent)
    }

//    private fun FillDataduommy() {
//        var list: List<ShopListEntity>? = null
//        rvProductList.layoutManager = LinearLayoutManager(this)
//        //rvProductList.adapter = ClothShopListingAdapter(this, list)
//    }

}
