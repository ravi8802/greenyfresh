package com.dev.greeny.activity

import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ShareCompat
import com.dev.greeny.R
import com.dev.greeny.sharedpreferences.SPreferenceKey
import com.dev.greeny.sharedpreferences.SharedPreferenceWriter
import com.dev.greeny.utility.Utils
import com.dev.greeny.utility.Validate
import kotlinx.android.synthetic.main.activity_invite_friend.*


class InviteFriendActivity : AppCompatActivity() {

    var validate: Validate? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_invite_friend)
        validate = Validate(this)
        backinvite.setOnClickListener {
            finish()
        }
        linear_share.setOnClickListener {
            shareIntent()
           // share_with_other(this@InviteFriendActivity, text_link!!.text.toString().trim { it <= ' ' })
        }
        text_copy.setOnClickListener {
            CopyToClipboard(text_link!!.text.toString().trim { it <= ' ' })
        }

    }

    private fun shareIntent() {
        ShareCompat.IntentBuilder
                .from(this)
                .setText("Download Greeny and visit my profile using below app links\n\n" +
                        "Android app link:- https://greenyfresh.page.link/?" +
                        "link=http://greenyfresh.co.in/?userId%3D${validate?.RetriveSharepreferenceString(Utils.ReferalCode)}" +
                        "&apn=com.dev.greeny" +
                        "&efr=1")
                .setType("text/plain")
                .setChooserTitle("Share with the users")
                .startChooser()
    }


    private fun CopyToClipboard(link: String) {
        val clipboard = getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
        val clip = ClipData.newPlainText("label", link)
        clipboard.setPrimaryClip(clip)
        Toast.makeText(this@InviteFriendActivity, resources.getString(R.string.copyed), Toast.LENGTH_SHORT).show()
    }


    fun share_with_other(context: Context, msg: String?) {
        val sendIntent = Intent()
        sendIntent.action = Intent.ACTION_SEND
        sendIntent.putExtra(Intent.EXTRA_TEXT, msg)
        sendIntent.type = "text/plain"
        context.startActivity(sendIntent)
    }
}