package com.dev.greeny.activity

import android.app.ProgressDialog
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.dev.greeny.R
import com.dev.greeny.service.request.Api
import com.dev.greeny.service.request.ApiClientConnection
import com.dev.greeny.service.response.RegistrationResponse
import com.dev.greeny.utility.Utils
import com.dev.greeny.utility.Validate
import com.dev.greeny.utility.snackBar
import kotlinx.android.synthetic.main.activity_forgot_password.*
import kotlinx.android.synthetic.main.app_bar_main2.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ForgotPasswordActivity : AppCompatActivity() {
    internal lateinit var progressDialog: ProgressDialog
    var apiInterface: Api? = null
    var validate: Validate? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_forgot_password)
        setSupportActionBar(toolbar)
        validate = Validate(this)

        validate!!.SaveSharepreferenceInt(Utils.flag, 1)
        apiInterface = ApiClientConnection.Companion.instance.createApiInterface()
        heading.text = "Forgot Password"
        image_back.setOnClickListener {
            finish()
        }
        linear_submit.setOnClickListener {
            if (!edit_email.text.toString().equals("")) {
                callForgotPwdApi()
            } else {
                Toast.makeText(this@ForgotPasswordActivity, "Please enter email", Toast.LENGTH_SHORT).show()
            }
        }

    }


    fun callForgotPwdApi() {
        progressDialog = ProgressDialog.show(
                this, resources.getString(R.string.app_name),
                getString(R.string.datadownloading)
        )
        //        progressDialog.setIcon(getDrawable(R.drawable.appicon))

        val call = apiInterface?.forgot_password(edit_email.text.toString())
        call?.enqueue(object : Callback<RegistrationResponse> {
            override fun onFailure(call: Call<RegistrationResponse>, t: Throwable) {
                t.printStackTrace()
                progressDialog.dismiss()
            }

            override fun onResponse(
                    call: Call<RegistrationResponse>, response: Response<RegistrationResponse>
            ) {
                when (response.code()) {

                    200 -> {
                        if (response!!.body().status.equals("true")) {
                            progressDialog.dismiss()
                            //  validate!!.SaveSharepreferenceInt(Utils.Totalcount,0)

                            Toast.makeText(this@ForgotPasswordActivity, response!!.body().message, Toast.LENGTH_LONG).show()
                            //snackBar(this@ForgotPasswordActivity, response!!.body().message)
                            finish()

                        } else {
                            progressDialog.dismiss()
                            snackBar(this@ForgotPasswordActivity, response!!.body().message)
                        }
                    }
                    500 -> {
                        progressDialog.dismiss()
                    }
                }

            }

        })


    }
}