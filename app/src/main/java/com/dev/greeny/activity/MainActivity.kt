package com.dev.greeny.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.core.view.GravityCompat
import com.dev.greeny.R
import com.dev.greeny.fragment.ProductListFrag
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.layout_toolbar.*

class MainActivity : AppCompatActivity(), View.OnClickListener {

  private lateinit var productListFrag: ProductListFrag

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_main)
    initListener()
    initFrag()
    setNavigationDrawer()
  }

  private fun initListener() {
    //   ivAddProduct.setOnClickListener(this)
  }

  override fun onClick(p0: View?) {
    when(p0){
      //  ivAddProduct -> startActivity(Intent(this,AddProductActivity::class.java))
    }
  }

  private fun initFrag() {
    productListFrag = ProductListFrag()
    supportFragmentManager.beginTransaction().replace(R.id.mainFrame,productListFrag).commit()
  }

  private fun setNavigationDrawer() {
    setSupportActionBar(toolbar)
    supportActionBar?.setDisplayShowTitleEnabled(false)
    supportActionBar?.setDisplayHomeAsUpEnabled(true)
    supportActionBar?.setHomeAsUpIndicator(R.drawable.side_menu)
  }

  override fun onOptionsItemSelected(item: MenuItem): Boolean {
    if (item.itemId == android.R.id.home) {
      drawerLayout.openDrawer(GravityCompat.START)
      return true
    }
    return super.onOptionsItemSelected(item)
  }

  fun setMainToolbar(title: String, addVisibility: Int = View.GONE){
    tvTitle.text = title
    // ivAddProduct.visibility = addVisibility
  }
}
