package com.dev.greeny.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.LinearLayout;
import android.widget.Toast;
import android.widget.Toolbar;

import com.dev.greeny.R;

import com.dev.greeny.adapter.ThreeLevelListAdapter;
import com.dev.greeny.baen.CommonModel;
import com.dev.greeny.baen.CommonResponse;
import com.dev.greeny.service.request.Api;
import com.dev.greeny.service.request.ApiClientConnection;
import com.dev.greeny.utility.MyInterface;
import com.dev.greeny.utility.Utils;
import com.dev.greeny.utility.Validate;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.dev.greeny.utility.CommonUtilitiesKt.setToolbar;

public class OrderHistoryListActivity extends AppCompatActivity implements MyInterface {
    ProgressDialog progressDialog;
    private CommonResponse[] commonResponses;
    private ExpandableListView expandableListView;
    private Validate validate = null;
    LinearLayout linear_no_data_order;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_history_list);

        validate =new Validate(OrderHistoryListActivity.this);
        expandableListView = (ExpandableListView) findViewById(R.id.expandible_listview);
        linear_no_data_order = (LinearLayout) findViewById(R.id.linear_no_data_order);

        orderCategoryList();

    }

    private void fillData(CommonResponse[] commonResponses) {
        if (commonResponses != null && commonResponses.length > 0) {
            linear_no_data_order.setVisibility(View.GONE);
            expandableListView.setVisibility(View.VISIBLE);
            // expandable listview
            // parent adapter
            ThreeLevelListAdapter threeLevelListAdapterAdapter = new ThreeLevelListAdapter(this, this.commonResponses, this);
            // set adapter
            expandableListView.setAdapter(threeLevelListAdapterAdapter);

            // OPTIONAL : Show one list at a time
            expandableListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
                int previousGroup = -1;

                @Override
                public void onGroupExpand(int groupPosition) {
                    if (groupPosition != previousGroup)
                        expandableListView.collapseGroup(previousGroup);
                    previousGroup = groupPosition;
                    //SharedPreferenceWriter.getInstance(MapProfessinalWorkerActivity.this).writeStringValue(kCategoryId, commonResponses[groupPosition].get_id());
                    //getSubCategory(categoryResponse.getData().get(groupPosition).getId());
                /*Toast.makeText(getApplicationContext(),
                        categoryResponse.getData().get(groupPosition).getId() + " Expanded",
                        Toast.LENGTH_SHORT).show();*/
                }
            });
        }else {
            linear_no_data_order.setVisibility(View.VISIBLE);
            expandableListView.setVisibility(View.GONE);
        }

    }


    //api calling
    private void orderCategoryList() {
        //progressDialog.show();
        progressDialog = ProgressDialog.show(this, getResources().getString(R.string.app_name), getString(R.string.datadownloading)
        );
        progressDialog.setIcon(getDrawable(R.drawable.greenyfreshlogo));
        Api apiInterface = ApiClientConnection.Companion.getInstance().createApiInterface();
        Call<CommonModel> call = apiInterface.ordersList("application/json", "Bearer "+validate.RetriveSharepreferenceString(Utils.INSTANCE.getToken()));
        call.enqueue(new Callback<CommonModel>() {
            @Override
            public void onResponse(Call<CommonModel> call, Response<CommonModel> response) {
                progressDialog.dismiss();
                if (response.isSuccessful()) {
                    CommonModel body = response.body();
                    if (body.getStatus().equalsIgnoreCase("true")) {
                        if (response.body() != null) {
                            commonResponses = body.getData();
                            fillData(commonResponses);
                            progressDialog.dismiss();
                            // getSubCategory(categoryResponse.getData().get(0).getId());
                        }
                    } else {
                        progressDialog.dismiss();
                        Toast.makeText(OrderHistoryListActivity.this, body.getResponse_message(), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    progressDialog.dismiss();
                    Toast.makeText(OrderHistoryListActivity.this, "Error!", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<CommonModel> call, Throwable t) {
                progressDialog.dismiss();
                t.printStackTrace();
            }
        });
    }

    @Override
    public void onCategory(String text) {
        //Toast.makeText(this, "done", Toast.LENGTH_SHORT).show();
    }
}