package com.dev.greeny.activity;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.dev.greeny.R;
import com.dev.greeny.service.request.Api;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.DialogInterface.BUTTON_NEGATIVE;
import static android.content.DialogInterface.BUTTON_POSITIVE;

public class AddAddressActivity extends AppCompatActivity implements View.OnClickListener {
    ImageView image_back;
    EditText edit_title, edit_buildingno, edit_floorno, edit_flat, edit_landmark;
    TextView text_address;
    Spinner spinner_city;
    LinearLayout linear_add, linear_delete;
    int PLACE_PICKER_REQUEST = 119;
    Api apiInterface;

    ImageView spinner_arrow;
    boolean fromLogin = true;

    String[] spinner_city_array;
    String latitute = "0", longitute = "0", selected_city_id = "", address_id = "", is_default = "";
    boolean isUpdate = false;
    CheckBox checkbox;

    double pLat, pLong;
    private String lat1;
    private String lat2;
    private String deslat1;
    private String deslat2;
    private String address1 = "";

    protected final int REQ_CODE_GPS_SETTINGS = 150;
    private final int REQ_CODE_LOCATION = 107;
    private Location mLastLocation;
    private boolean isLocServiceStarted = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_address);


        image_back = (ImageView) findViewById(R.id.image_back);
        image_back.setOnClickListener(this);

        edit_title = (EditText) findViewById(R.id.edit_title);
        edit_buildingno = (EditText) findViewById(R.id.edt_passcode);
        edit_floorno = (EditText) findViewById(R.id.edt_phn);
        edit_flat = (EditText) findViewById(R.id.edt_state);
        edit_flat = (EditText) findViewById(R.id.edit_cityno);
        edit_landmark = (EditText) findViewById(R.id.edit_landmark);

        spinner_city = (Spinner) findViewById(R.id.spinner_city);

        spinner_arrow = (ImageView) findViewById(R.id.spinner_arrow);
        spinner_arrow.setOnClickListener(this);

        text_address = (TextView) findViewById(R.id.text_address);
        text_address.setOnClickListener(this);

        linear_add = (LinearLayout) findViewById(R.id.linear_add);
        linear_add.setOnClickListener(this);

        linear_delete = (LinearLayout) findViewById(R.id.linear_delete);
        linear_delete.setOnClickListener(this);

        checkbox = (CheckBox) findViewById(R.id.checkbox);
        //get data from previous activity and change ui according to that
        if (getIntent().hasExtra("id")) {
            isUpdate = true;
            getDataFromIntent();
            linear_delete.setVisibility(View.VISIBLE);
        } else {
            isUpdate = false;
            linear_delete.setVisibility(View.GONE);
        }


    }

    private void getDataFromIntent() {
        is_default = getIntent().getStringExtra("default");
        if (is_default.equalsIgnoreCase("yes")) {
            checkbox.setChecked(true);
        } else {
            checkbox.setChecked(false);
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.image_back:
                AddAddressActivity.this.finish();
                break;

            case R.id.text_address:
                //              startActivityForResult(AddressPickerAct.getIntent(this, "DROPOFF", ""), PLACE_PICKER_REQUEST);
//                PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
//                try {
//                    startActivityForResult(builder.build(this), PLACE_PICKER_REQUEST);
//                } catch (GooglePlayServicesRepairableException e) {
//                    e.printStackTrace();
//                } catch (GooglePlayServicesNotAvailableException e) {
//                    e.printStackTrace();
//                }
                break;

            case R.id.spinner_arrow:
                spinner_city.performClick();
                break;

            case R.id.linear_add:
                if (checkbox.isChecked()) {
                    is_default = "yes";
                } else {
                    is_default = "no";
                    // }
//                if (edit_title.getText().toString().trim().equalsIgnoreCase("")) {
//                    CommonUtilFunctions.Error_Alert_Dialog(AddAddressActivity.this, getResources().getString(R.string.enter_title));
//                } else if (text_address.getText().toString().trim().equalsIgnoreCase("")) {
//                    CommonUtilFunctions.Error_Alert_Dialog(AddAddressActivity.this, getResources().getString(R.string.enter_loc));
//                } else if (edit_buildingno.getText().toString().trim().equalsIgnoreCase("")) {
//                    CommonUtilFunctions.Error_Alert_Dialog(AddAddressActivity.this, getResources().getString(R.string.enter_buildingno));
//                } else if (edit_floorno.getText().toString().trim().equalsIgnoreCase("")) {
//                    CommonUtilFunctions.Error_Alert_Dialog(AddAddressActivity.this, getResources().getString(R.string.enter_floorgno));
//                } else if (edit_flat.getText().toString().trim().equalsIgnoreCase("")) {
//                    CommonUtilFunctions.Error_Alert_Dialog(AddAddressActivity.this, getResources().getString(R.string.enter_flatno));
//                } else if (edit_landmark.getText().toString().trim().equalsIgnoreCase("")) {
//                    CommonUtilFunctions.Error_Alert_Dialog(AddAddressActivity.this, getResources().getString(R.string.enter_landmark));
//                } else {
//                    if (CommonMethods.checkConnection()) {
//                        PostAddress(isUpdate);
//                    } else {
//                        CommonUtilFunctions.Error_Alert_Dialog(AddAddressActivity.this, getResources().getString(R.string.internetconnection));
//                    }
                }

                break;

            case R.id.linear_delete:
                //delete address
//                if (CommonMethods.checkConnection()) {
//                    DeleteAddress();
//                } else {
//                    CommonUtilFunctions.Error_Alert_Dialog(AddAddressActivity.this, getResources().getString(R.string.internetconnection));
//                }
                break;
        }
    }


//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//        if (requestCode == PLACE_PICKER_REQUEST) {
//            if (resultCode == Activity.RESULT_OK) {
//
//                address1 = data.getStringExtra("ADDRESS");
//                lat1 = data.getStringExtra("LAT");
//                lat2 = data.getStringExtra("LONG");
//                pLat = Double.parseDouble(lat1);
//                pLong = Double.parseDouble(lat2);
//                text_address.setText(address1);
//                text_address.setTextColor(getResources().getColor(R.color.black));
//            }
//
//        } else if (requestCode == REQ_CODE_GPS_SETTINGS) {
//            switch (resultCode) {
//                case Activity.RESULT_OK:
//
//                    break;
//                case Activity.RESULT_CANCELED:
//                    startActivityForResult(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS), 0);
//                    break;
//            }
//        }
//
////        if (requestCode == PLACE_PICKER_REQUEST) {
////            if (resultCode == RESULT_OK) {
////                Place place = PlacePicker.getPlace(data, this);
////                latitute = String.valueOf(place.getLatLng().latitude);
////                longitute = String.valueOf(place.getLatLng().longitude);
////                text_address.setText(place.getName().toString());
////                text_address.setTextColor(getResources().getColor(R.color.black));
//////                String toastMsg = String.format("Place: %s", place.getAddress());
//////                Toast.makeText(this, toastMsg, Toast.LENGTH_LONG).show();
////            }
////        }
//    }

    //getcity


}
