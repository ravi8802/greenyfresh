//package com.dev.greeny.activity;
//
//import android.app.ProgressDialog;
//import android.content.Intent;
//import android.graphics.Color;
//import android.os.Bundle;
//import android.util.Log;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.AdapterView;
//import android.widget.ArrayAdapter;
//import android.widget.Button;
//import android.widget.EditText;
//import android.widget.Spinner;
//import android.widget.TextView;
//import android.widget.Toast;
//
//import androidx.annotation.NonNull;
//import androidx.appcompat.app.AppCompatActivity;
//import androidx.appcompat.widget.Toolbar;
//
//import com.dev.greeny.R;
//import com.dev.greeny.baen.CommonModel;
//import com.dev.greeny.retrofit.ServicesConnection;
//import com.dev.greeny.retrofit.ServicesInterface;
//import com.dev.greeny.service.request.Api;
//import com.dev.greeny.service.request.ApiClientConnection;
//import com.dev.greeny.sharedpreferences.SPreferenceKey;
//import com.dev.greeny.sharedpreferences.SharedPreferenceWriter;
//import com.dev.greeny.utility.CommonUtilities;
//import com.dev.greeny.utility.ParamEnum;
//import com.dev.greeny.utility.Validate;
//import com.paytm.pgsdk.PaytmOrder;
//import com.paytm.pgsdk.PaytmPGService;
//import com.paytm.pgsdk.PaytmPaymentTransactionCallback;
//
//
//import java.util.ArrayList;
//import java.util.Arrays;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//import java.util.Random;
//
//import okhttp3.MediaType;
//import okhttp3.RequestBody;
//import retrofit2.Call;
//import retrofit2.Callback;
//import retrofit2.Response;
//
//public class PaymentGatewayActivity extends AppCompatActivity implements PaytmPaymentTransactionCallback {
//
//  private final String TAG = PaymentGatewayActivity.class.getSimpleName();
//  ProgressDialog progressDialog;
//  Api apiInterface;
//  Validate validate;
//
//  Spinner year_edt, month_edt;
//
//  String my_token;
//  double priceSum = 0;
//  private Toolbar myToolbar;
//  private TextView gst_amaount, total_amaount, ticket_amaount, gsttext;
//  private EditText card_no, cvc_edt, card_holder_nme;
//  private Button btn_pay;
//  //private ArrayList<TicketTypeInner> paymentlist = new ArrayList<>();
//  private String userPublishableKey = "pk_live_Pn7o04Gg9s9YCcVl31NIozif";
//  private String userSecretKey = "sk_live_9YKwJvhYlI6jOFtl0WdW1td0";
//  private String ids, tickests;
//  private String monthSelected;
//  private int monthPos = 0;
//  private int yearPos;
//  private String yearSelected;
//  private double gst_txt;
//  //private String merchant_id = "fwshhV03227180789586"; // for testing
//  private String merchant_id = "zZxkLU85293121032720";   // for live
//  private String orderId = "";
//  private String customerId = "";
//  private String bookingType = "";
//  private String notification_id="";
//  private double decimalPointAmount;
//
//  String totalPrice;
//  int total;
//
//  @Override
//  protected void onCreate(Bundle savedInstanceState) {
//    super.onCreate(savedInstanceState);
//    setContentView(R.layout.activity_payment);
//
//    validate = new Validate(this);
//    apiInterface = ApiClientConnection.Companion.getInstance().createApiInterface();
//
//    total_amaount = (TextView)findViewById(R.id.total_amaount);
//
//    if (getIntent() != null) {
//      totalPrice = getIntent().getStringExtra("totalPrice");
//      total_amaount.setText(totalPrice);
//    }
//
//    gst_amaount = (TextView)findViewById(R.id.gst_amaount);
//    ticket_amaount = (TextView)findViewById(R.id.ticket_amaount);
//
//    gsttext = (TextView)findViewById(R.id.gsttext);
//
//    card_no = (EditText) findViewById(R.id.card_no);
////        month_edt = (EditText) findViewById(R.id.month_edt);
////        year_edt = (Spinner) findViewById(R.id.year_edt);
//    cvc_edt = (EditText) findViewById(R.id.cvc_edt);
//    card_holder_nme = (EditText) findViewById(R.id.card_holder_nme);
//    btn_pay = (Button) findViewById(R.id.btn_pay);
//    ticket_amaount.setText(String.valueOf("" + "$" + priceSum));
//
//
//
//    //  stripe = new Stripe(PaymentGatewayActivity.this);
//
//
//    year_edt = (Spinner) findViewById(R.id.year_edt);
//    setUpYearSpinner();
//    year_edt.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//      @Override
//      public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//        yearSelected = parent.getItemAtPosition(position).toString();
//        yearPos = position;
//      }
//
//
//      @Override
//      public void onNothingSelected(AdapterView<?> adapterView) {
//
//      }
//    });
//
//
//    //MONTHS
//    month_edt = (Spinner) findViewById(R.id.month_edt);
//    setUpMonthSpinner();
//    month_edt.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//      @Override
//      public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//        monthSelected = parent.getItemAtPosition(position).toString();
//        monthPos = position;
//      }
//
//      @Override
//      public void onNothingSelected(AdapterView<?> adapterView) {
//
//      }
//    });
//
////       Card card = new Card("5105 1051 0510 5100", 12, 2018, "123");
//
//
//    btn_pay.setOnClickListener(new View.OnClickListener() {
//      @Override
//      public void onClick(View view) {
//        if (validation()) {
//          serviceGenerateChecksum();
////
////                    Card card = new Card(card_no.getText().toString().trim(), Integer.parseInt(month_edt.getSelectedItem().toString().trim()), Integer.parseInt(year_edt.getSelectedItem().toString()), cvc_edt.getText().toString().trim());
////
////                    if (!card.validateCard()) {
////                        Toast.makeText(PaymentGatewayActivity.this, "Invalid card", Toast.LENGTH_SHORT).show();
////                        return;
////                    }
////                    final Stripe stripe = new Stripe(PaymentGatewayActivity.this, "pk_test_dC2PSVM608UUafdiQEfTZNhC00ZBPP3RmE");
////                    progressDialog = ProgressDialog.show(
////                            PaymentGatewayActivity.this, getResources().getString(R.string.app_name),
////                            getString(R.string.datadownloading));
////                   // MyDialog.getInstance(PaymentGatewayActivity.this).showDialog(PaymentGatewayActivity.this);
////                    stripe.createToken(card, new TokenCallback() {
////                                public void onSuccess(Token token) {
////                                    my_token = token.getId();
////                                    // Send token to your server
////                                    //ticketBookingService();
////                                    Log.e("PayToken "," Ravi "+my_token);
////                                    progressDialog.dismiss();
////                                }
////
////                                public void onError(Exception error) {
////                                    // Show localized error message
////                                    Toast.makeText(PaymentGatewayActivity.this, error.getLocalizedMessage(), Toast.LENGTH_LONG).show();
////                                    progressDialog.dismiss();
////                                }
////                            }
////                    );
//        }
//
//      }
//    });
//
//
//
//  }
//
//
//
//  private void setUpYearSpinner() {
//    String[] spinnerItems = new String[]{
//            "YY",
//            "2020",
//            "2021",
//            "2022",
//            "2023",
//            "2024",
//            "2025",
//            "2026",
//            "2028",
//            "2029",
//            "2030",
//            "2031",
//            "2032",
//            "2033",
//            "2034",
//            "2035",
//            "2036",
//            "2037",
//            "2038",
//            "2039",
//            "2040",
//            "2041",
//            "2042",
//            "2043",
//            "2044",
//            "2045",
//            "2046"};
//
//
//    List<String> spinnerList = new ArrayList<>(Arrays.asList(spinnerItems));
//
//    ArrayAdapter<String> arrayAdapter =
//            new ArrayAdapter<String>(PaymentGatewayActivity.this, R.layout.spinner_drop_singlerow, spinnerList) {
//              @Override
//              public boolean isEnabled(int position) {
//                if (position == 0) {
//                  //Disable the first item of spinner.
//                  Log.i(TAG, "Position[0]: Spinner first item is disabled");
//                  return false;
//                } else {
//                  String itemSelected = year_edt.getItemAtPosition(position).toString();
//                  Log.i(TAG, "Selected Item[" + position + "]: " + itemSelected);
//                  return true;
//                }
//              }
//
//              @Override
//              public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
//                View spinnerView = super.getDropDownView(position, convertView, parent);
//
//                TextView spinnerTextV = (TextView) spinnerView;
//                if (position == 0) {
//                  //Set the disable spinner item color fade .
//                  spinnerTextV.setTextColor(Color.parseColor("#a7a7a6"));
//                } else {
//                  //#404040
//                  spinnerTextV.setTextColor(Color.parseColor("#b2000000"));
//                }
//                return spinnerView;
//              }
//            };
//
//    arrayAdapter.setDropDownViewResource(R.layout.spinner_drop_singlerow);
//
//    year_edt.setAdapter(arrayAdapter);
//
//  }
//
//  private void setUpMonthSpinner() {
//    String[] spinnerItems = new String[]{
//            "MM",
//            "01",
//            "02",
//            "03",
//            "04",
//            "05",
//            "06",
//            "07",
//            "08",
//            "09",
//            "10",
//            "11",
//            "12"};
//
//
//    List<String> spinnerList = new ArrayList<>(Arrays.asList(spinnerItems));
//
//    ArrayAdapter<String> arrayAdapter =
//            new ArrayAdapter<String>(PaymentGatewayActivity.this, R.layout.spinner_drop_singlerow, spinnerList) {
//              @Override
//              public boolean isEnabled(int position) {
//                if (position == 0) {
//                  //Disable the first item of spinner.
//                  Log.i(TAG, "Position[0]: Spinner first item is disabled");
//                  return false;
//                } else {
//                  String itemSelected = month_edt.getItemAtPosition(position).toString();
//                  Log.i(TAG, "Selected Item[" + position + "]: " + itemSelected);
//                  return true;
//                }
//              }
//
//              @Override
//              public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
//                View spinnerView = super.getDropDownView(position, convertView, parent);
//
//                TextView spinnerTextV = (TextView) spinnerView;
//                if (position == 0) {
//                  //Set the disable spinner item color fade .
//                  spinnerTextV.setTextColor(Color.parseColor("#a7a7a6"));
//                } else {
//                  //#404040
//                  spinnerTextV.setTextColor(Color.parseColor("#b2000000"));
//                }
//                return spinnerView;
//              }
//            };
//
//    arrayAdapter.setDropDownViewResource(R.layout.spinner_drop_singlerow);
//
//    month_edt.setAdapter(arrayAdapter);
//
//
//  }
//
//  private boolean validation() {
//    boolean flag = true;
//
////        if(card_no.length() == 0 || month_edt.length() == 0 || year_edt.length() == 0 || cvc_edt.length() == 0 || card_holder_nme.length() == 0){
////
////            Toast.makeText(getApplicationContext(), "pls fill the empty fields", Toast.LENGTH_SHORT).show();
////        }
//
//    if (card_no.getText().toString().trim().equals("")) {
//      Toast.makeText(PaymentGatewayActivity.this, "Please enter card number", Toast.LENGTH_LONG).show();
//      flag = false;
//    } else if (month_edt.getSelectedItem().toString().trim().equals("")) {
//      Toast.makeText(PaymentGatewayActivity.this, "Please enter Month", Toast.LENGTH_LONG).show();
//      flag = false;
//    } else if (year_edt.getSelectedItem().toString().trim().equals("")) {
//      Toast.makeText(PaymentGatewayActivity.this, "Please enter year", Toast.LENGTH_LONG).show();
//      flag = false;
//    } else if (cvc_edt.getText().toString().trim().equals("")) {
//      Toast.makeText(PaymentGatewayActivity.this, "Please enter cvv", Toast.LENGTH_LONG).show();
//      flag = false;
//    } else if (card_holder_nme.getText().toString().trim().equals("")) {
//      Toast.makeText(PaymentGatewayActivity.this, "Please enter Card holder name", Toast.LENGTH_LONG).show();
//      flag = false;
//    }
//    return flag;
//  }
//
//  //  METHOD TO REQUEST GENERATE_CHECKSUM_PAY_TM SERVICE ..
//  private void serviceGenerateChecksum( ) {
//    getRandomIdFor("ORDER");
//    getRandomIdFor("CUSTOM");
//    if (CommonUtilities.isNetworkAvailable(this)) {
//      try {
//        ServicesInterface servicesInterface = ServicesConnection.getInstance().createService();
//        Call<CommonModel> commonModelCall = servicesInterface.generateChecksum(getParams());
//
//        ServicesConnection.getInstance().enqueueWithoutRetry(
//                commonModelCall,
//                this,
//                true,
//                new Callback<CommonModel>() {
//                  @Override
//                  public void onResponse(Call<CommonModel> call, Response<CommonModel> response) {
//                    if (response.isSuccessful()) {
//                      CommonModel body = ((CommonModel) response.body());
//                      if (body.getStatus().equalsIgnoreCase(ParamEnum.SUCCESS.theValue())) {
//                        doPayment(body.getChecksum());
//                      } else {
//                        //CommonUtilities.snackBar(PaymentActivity.this,body.getMessage());
//                      }
//                    }
//                  }
//                  @Override
//                  public void onFailure(Call<CommonModel> call, Throwable t) {
//                  }
//                });
//      } catch (Exception e) {
//        e.printStackTrace();
//      }
//    } else {
//      //CommonUtilities.snackBar(this, getString(R.string.no_internet));
//    }
//  }
//
//  // generate random order_id and customer_id
//  private void getRandomIdFor(String for_what) {
//    String randomGenId = "";
//    Random rand = null;
//    String randomString = "";
//    switch (for_what) {
//      case "ORDER":
//        rand = new Random();
//        randomString = Integer.toString(rand.nextInt()) + (System.currentTimeMillis() / 1000L);
//
//        orderId = "ORDER" + randomString.substring(1, 11);
//
//        randomGenId = orderId;
//        Log.e(TAG, "\n--ORDER id --: " + orderId);
//
//        break;
//
//      case "CUSTOM":
//        rand = new Random();
//        randomString = Integer.toString(rand.nextInt()) + (System.currentTimeMillis() / 1000L);
//
//        customerId = "CUSTOM" + randomString.substring(1, 11);
//
//        randomGenId = customerId;
//        Log.e(TAG, "\n--CUSTOM id --: " + customerId);
//
//        break;
//
//    }
//  }
//
//  private Map<String, RequestBody> getParams(){
//    String email_id = SharedPreferenceWriter.getInstance(this).getString(SPreferenceKey.USER_EMAIL);
//    String mob_no = "9873529969";
//
//    Map<String, RequestBody> fields = new HashMap<>();
//    fields.put("MID", RequestBody.create(MediaType.parse("text/plain"), merchant_id != null ? merchant_id : ""));
//    fields.put("ORDER_ID", RequestBody.create(MediaType.parse("text/plain"), orderId != null ? orderId : ""));
//    fields.put("CUST_ID", RequestBody.create(MediaType.parse("text/plain"), customerId != null ? customerId : ""));
//    fields.put("INDUSTRY_TYPE_ID", RequestBody.create(MediaType.parse("text/plain"), "Retail"));
//    fields.put("CHANNEL_ID", RequestBody.create(MediaType.parse("text/plain"), "WAP"));
//    fields.put("TXN_AMOUNT", RequestBody.create(MediaType.parse("text/plain"), String.valueOf(decimalPointAmount)));
//    //fields.put("WEBSITE", RequestBody.create(MediaType.parse("text/plain"), "WEBSTAGING"));
//    fields.put("WEBSITE", RequestBody.create(MediaType.parse("text/plain"), "DEFAULT"));
//    //fields.put("CALLBACK_URL", RequestBody.create(MediaType.parse("text/plain"), "https://securegw-stage.paytm.in/theia/paytmCallback?ORDER_ID="+orderId));
//    fields.put("CALLBACK_URL", RequestBody.create(MediaType.parse("text/plain"), "https://securegw.paytm.in/theia/paytmCallback?ORDER_ID="+orderId));
//    fields.put("EMAIL", RequestBody.create(MediaType.parse("text/plain"), email_id!=null?email_id:""));
//    fields.put("MOBILE_NO", RequestBody.create(MediaType.parse("text/plain"), mob_no!=null?mob_no:""));
//    //fields.put("token", RequestBody.create(MediaType.parse("text/plain"), SharedPreferenceWriter.getInstance(this).getString(SPreferenceKey.TOKEN)));
//    //fields.put("language_type", RequestBody.create(MediaType.parse("text/plain"), SharedPreferenceWriter.getInstance(this).getString(SPreferenceKey.LANGUAGE)));
//
//    return fields;
//  }
//
//  private void doPayment(String checkSum){
//    String email_id = SharedPreferenceWriter.getInstance(this).getString(SPreferenceKey.USER_EMAIL);
//    String mob_no = "9873529969";
//    //PaytmPGService service = PaytmPGService.getStagingService();  //For Staging environment:
//    PaytmPGService service = PaytmPGService.getProductionService(); //For Production environment:
//
//    HashMap<String, String> paramMap = new HashMap<String,String>();
//    paramMap.put( "MID" , merchant_id);  //Key in your staging and production MID available in your dashboard:
//    paramMap.put( "ORDER_ID" , orderId);
//    paramMap.put( "CUST_ID" , customerId);
//    paramMap.put( "MOBILE_NO" , mob_no);
//    paramMap.put( "EMAIL" , email_id);
//    paramMap.put( "CHANNEL_ID" , "WAP");
//    paramMap.put( "TXN_AMOUNT" , String.valueOf(decimalPointAmount));
//    //paramMap.put( "WEBSITE" , "WEBSTAGING");
//    paramMap.put( "WEBSITE" , "DEFAULT");
//    paramMap.put( "INDUSTRY_TYPE_ID" , "Retail");
//    //paramMap.put( "CALLBACK_URL", "https://securegw-stage.paytm.in/theia/paytmCallback?ORDER_ID="+orderId);
//    paramMap.put( "CALLBACK_URL", "https://securegw.paytm.in/theia/paytmCallback?ORDER_ID="+orderId);
//    paramMap.put( "CHECKSUMHASH" , checkSum);
//    PaytmOrder order = new PaytmOrder(paramMap);
//
//    service.initialize(order, null); // Initialize Service
//
//    // Call start transaction method using service object:
//    service.startPaymentTransaction(this, true, true
//            ,this );
//  }
//
//  @Override
//  public void onTransactionResponse(Bundle bundle) {
//    // After successful transaction this method gets called.
//    // Response bundle contains the merchant response parameters.
//    Log.w(TAG, "--Transaction Response--: " + bundle.toString());
//    String responseCode = bundle.getString("RESPCODE");
//    String respmsg = bundle.getString("RESPMSG");
//    String status = bundle.getString("STATUS");
//    String orderId = bundle.getString("ORDERID");
//    String amount = bundle.getString("TXNAMOUNT");
//    String transactionID = bundle.getString("TXNID");
//    if (responseCode != null) {
//      switch (responseCode){
//        //01 : Transaction Successful
//        case "01":
//        case "1":
//          //Toast.makeText(this, "Transaction successful", Toast.LENGTH_LONG).show();
//          serviceAddPayment(status,orderId,amount,transactionID);
//          break;
//
//        //227 :Payment Failed due to a Bank Failure. Please try after some time.
//        case "227":
//          //Toast.makeText(this, "Payment Failed due to a Bank Failure. Try again after some time.", Toast.LENGTH_LONG).show();
//          serviceAddPayment(status,orderId,amount,transactionID);
//          break;
//
//        //repeated request
//        case "269":
//          //Invalid checksum
//        case "330":
//          //REQUEST CANCELLED BY YOU
//        case "14112":
//          //Toast.makeText(this, "" + respmsg, Toast.LENGTH_LONG).show();
//          serviceAddPayment(status,orderId,amount,transactionID);
//          break;
//
//        default:
//          //Toast.makeText(this, "Payment Failed. Something went wrong!", Toast.LENGTH_LONG).show();
//          serviceAddPayment(status,orderId,amount,transactionID);
//          break;
//
//      }
//    }
//  }
//
//  @Override
//  public void networkNotAvailable() {
//    // If network is not available, then this method gets called.
//    Toast.makeText(this, "Network connection error: Check your internet connectivity", Toast.LENGTH_LONG).show();
//  }
//
//  @Override
//  public void clientAuthenticationFailed(String inResponse) {
//        /*Client authentication failure:
//          This can happen due to multiple reason -
//          * Paytm services are not available due to a downtime
//          * Server unable to generate checksum or checksum response is not in proper format
//          * Server failed to authenticate the client. That is value of payt_STATUS is 2. // payt_STATUS hasn't been defined anywhere*/
//    Toast.makeText(this, "Authentication failed: Server error" + inResponse, Toast.LENGTH_LONG).show();
//  }
//
//  @Override
//  public void someUIErrorOccurred(String inErrorMessage) {
//    // Some UI Error Occurred in Payment Gateway Activity.
//    // This may be due to initialization of views in Payment Gateway Activity or may be due to initialization of webview.
//    // Error Message details the error occurred.
//    Toast.makeText(this, "UI Error " + inErrorMessage , Toast.LENGTH_LONG).show();
//  }
//
//  @Override
//  public void onErrorLoadingWebPage(int i, String inResponse, String s1) {
//    // This page gets called if some error occurred while loading some URL in Webview.
//    // Error Code and Error Message describes the error.
//    // Failing URL is the URL that failed to load.
//    Toast.makeText(this, "Unable to load webpage " + inResponse, Toast.LENGTH_LONG).show();
//  }
//
//  @Override
//  public void onBackPressedCancelTransaction() {
//    Toast.makeText(this, "Transaction cancelled" , Toast.LENGTH_LONG).show();
//  }
//
//  @Override
//  public void onTransactionCancel(String s, Bundle bundle) {
//
//  }
//
//  //    METHOD TO REQUEST ADD_PAYMENT SERVICE ..
//  private void serviceAddPayment(String status,String orderId,String amount,String transactionID) {
//    if(bookingType.equalsIgnoreCase("Home Visit"))
//      bookingType = "home";
//    else
//      bookingType = "call";
//    getRandomIdFor("ORDER");
//    getRandomIdFor("CUSTOM");
//    if (CommonUtilities.isNetworkAvailable(this)) {
//      try {
//        ServicesInterface servicesInterface = ServicesConnection.getInstance().createService();
//        Call<CommonModel> commonModelCall = servicesInterface.addPayment(SharedPreferenceWriter.getInstance(this).getString(SPreferenceKey.TOKEN),
//                "","",amount,status,transactionID,orderId,bookingType,
//                "");
//
//        ServicesConnection.getInstance().enqueueWithoutRetry(
//                commonModelCall,
//                this,
//                true,
//                new Callback<CommonModel>() {
//                  @Override
//                  public void onResponse(Call<CommonModel> call, Response<CommonModel> response) {
//                    if (response.isSuccessful()) {
//                      CommonModel body = ((CommonModel) response.body());
//                      if (body.getStatus().equalsIgnoreCase(ParamEnum.SUCCESS.theValue())) {
//                        Toast.makeText(PaymentGatewayActivity.this, "Transaction successfully", Toast.LENGTH_LONG).show();
//                        Intent intent =  new Intent(PaymentGatewayActivity.this,MainActivity.class);
//                        //intent.putExtra(ParamEnum.TYPE.theValue(),"");
//                        startActivity(intent);
//                        //serviceNotificationRead();
//                      } else {
//                        //CommonUtilities.snackBar(PaymentActivity.this,body.getMessage());
//                      }
//                    }
//                  }
//                  @Override
//                  public void onFailure(Call<CommonModel> call, Throwable t) {
//                  }
//                });
//      } catch (Exception e) {
//        e.printStackTrace();
//      }
//    } else {
//      //CommonUtilities.snackBar(this, getString(R.string.no_internet));
//    }
//  }
//
//
//  @Override
//  public void onPointerCaptureChanged(boolean hasCapture) {
//
//  }
//}
package com.dev.greeny.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.dev.greeny.R;
import com.dev.greeny.baen.CommonModel;
import com.dev.greeny.retrofit.ServicesConnection;
import com.dev.greeny.retrofit.ServicesInterface;
import com.dev.greeny.service.request.Api;
import com.dev.greeny.service.request.ApiClientConnection;
import com.dev.greeny.sharedpreferences.SPreferenceKey;
import com.dev.greeny.sharedpreferences.SharedPreferenceWriter;
import com.dev.greeny.utility.CommonUtilities;
import com.dev.greeny.utility.ParamEnum;
import com.dev.greeny.utility.Utils;
import com.dev.greeny.utility.Validate;
import com.paytm.pgsdk.PaytmOrder;
import com.paytm.pgsdk.PaytmPGService;
import com.paytm.pgsdk.PaytmPaymentTransactionCallback;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PaymentGatewayActivity extends AppCompatActivity implements PaytmPaymentTransactionCallback {

    private final String TAG = PaymentGatewayActivity.class.getSimpleName();
    ProgressDialog progressDialog;
    Api apiInterface;
    Validate validate;

    Spinner year_edt, month_edt;

    String my_token;
    double priceSum = 0;
    private Toolbar myToolbar;
    private TextView gst_amaount, total_amaount, ticket_amaount, gsttext;
    private EditText card_no, cvc_edt, card_holder_nme;
    private Button btn_pay;
    //private ArrayList<TicketTypeInner> paymentlist = new ArrayList<>();
    private String userPublishableKey = "pk_live_Pn7o04Gg9s9YCcVl31NIozif";
    private String userSecretKey = "sk_live_9YKwJvhYlI6jOFtl0WdW1td0";
    private String ids, tickests;
    private String monthSelected;
    private int monthPos = 0;
    private int yearPos;
    private String yearSelected;
    private double gst_txt;
    //private String merchant_id = "fwshhV03227180789586"; // for testing
    private String merchant_id = "zZxkLU85293121032720";   // for live
    private String orderId = "";
    private String customerId = "";
    private String bookingType = "";
    private String notification_id = "";
    private double decimalPointAmount;

    String totalPrice;
    String paymentType;
    int total;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);

        validate = new Validate(this);
        apiInterface = ApiClientConnection.Companion.getInstance().createApiInterface();

        total_amaount = (TextView) findViewById(R.id.total_amaount);

        if (getIntent() != null) {
            totalPrice = getIntent().getStringExtra("totalPrice");
            paymentType = getIntent().getStringExtra("paymentType");
            total_amaount.setText(totalPrice);
        }

        gst_amaount = (TextView) findViewById(R.id.gst_amaount);
        ticket_amaount = (TextView) findViewById(R.id.ticket_amaount);

        gsttext = (TextView) findViewById(R.id.gsttext);

        card_no = (EditText) findViewById(R.id.card_no);
//        month_edt = (EditText) findViewById(R.id.month_edt);
//        year_edt = (Spinner) findViewById(R.id.year_edt);
        cvc_edt = (EditText) findViewById(R.id.cvc_edt);
        card_holder_nme = (EditText) findViewById(R.id.card_holder_nme);
        btn_pay = (Button) findViewById(R.id.btn_pay);
        ticket_amaount.setText(String.valueOf("" + "$" + priceSum));


        //  stripe = new Stripe(PaymentGatewayActivity.this);


        year_edt = (Spinner) findViewById(R.id.year_edt);
        setUpYearSpinner();
        year_edt.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                yearSelected = parent.getItemAtPosition(position).toString();
                yearPos = position;
            }


            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        //MONTHS
        month_edt = (Spinner) findViewById(R.id.month_edt);
        setUpMonthSpinner();
        month_edt.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                monthSelected = parent.getItemAtPosition(position).toString();
                monthPos = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

//       Card card = new Card("5105 1051 0510 5100", 12, 2018, "123");


        btn_pay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (validation()) {
                    serviceGenerateChecksumPaytm();
//
//                    Card card = new Card(card_no.getText().toString().trim(), Integer.parseInt(month_edt.getSelectedItem().toString().trim()), Integer.parseInt(year_edt.getSelectedItem().toString()), cvc_edt.getText().toString().trim());
//
//                    if (!card.validateCard()) {
//                        Toast.makeText(PaymentGatewayActivity.this, "Invalid card", Toast.LENGTH_SHORT).show();
//                        return;
//                    }
//                    final Stripe stripe = new Stripe(PaymentGatewayActivity.this, "pk_test_dC2PSVM608UUafdiQEfTZNhC00ZBPP3RmE");
//                    progressDialog = ProgressDialog.show(
//                            PaymentGatewayActivity.this, getResources().getString(R.string.app_name),
//                            getString(R.string.datadownloading));
//                   // MyDialog.getInstance(PaymentGatewayActivity.this).showDialog(PaymentGatewayActivity.this);
//                    stripe.createToken(card, new TokenCallback() {
//                                public void onSuccess(Token token) {
//                                    my_token = token.getId();
//                                    // Send token to your server
//                                    //ticketBookingService();
//                                    Log.e("PayToken "," Ravi "+my_token);
//                                    progressDialog.dismiss();
//                                }
//
//                                public void onError(Exception error) {
//                                    // Show localized error message
//                                    Toast.makeText(PaymentGatewayActivity.this, error.getLocalizedMessage(), Toast.LENGTH_LONG).show();
//                                    progressDialog.dismiss();
//                                }
//                            }
//                    );
                }

            }
        });

        serviceGenerateOrderNumber();

    }


    private void setUpYearSpinner() {
        String[] spinnerItems = new String[]{
                "YY",
                "2020",
                "2021",
                "2022",
                "2023",
                "2024",
                "2025",
                "2026",
                "2028",
                "2029",
                "2030",
                "2031",
                "2032",
                "2033",
                "2034",
                "2035",
                "2036",
                "2037",
                "2038",
                "2039",
                "2040",
                "2041",
                "2042",
                "2043",
                "2044",
                "2045",
                "2046"};


        List<String> spinnerList = new ArrayList<>(Arrays.asList(spinnerItems));

        ArrayAdapter<String> arrayAdapter =
                new ArrayAdapter<String>(PaymentGatewayActivity.this, R.layout.spinner_drop_singlerow, spinnerList) {
                    @Override
                    public boolean isEnabled(int position) {
                        if (position == 0) {
                            //Disable the first item of spinner.
                            Log.i(TAG, "Position[0]: Spinner first item is disabled");
                            return false;
                        } else {
                            String itemSelected = year_edt.getItemAtPosition(position).toString();
                            Log.i(TAG, "Selected Item[" + position + "]: " + itemSelected);
                            return true;
                        }
                    }

                    @Override
                    public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
                        View spinnerView = super.getDropDownView(position, convertView, parent);

                        TextView spinnerTextV = (TextView) spinnerView;
                        if (position == 0) {
                            //Set the disable spinner item color fade .
                            spinnerTextV.setTextColor(Color.parseColor("#a7a7a6"));
                        } else {
                            //#404040
                            spinnerTextV.setTextColor(Color.parseColor("#b2000000"));
                        }
                        return spinnerView;
                    }
                };

        arrayAdapter.setDropDownViewResource(R.layout.spinner_drop_singlerow);

        year_edt.setAdapter(arrayAdapter);

    }

    private void setUpMonthSpinner() {
        String[] spinnerItems = new String[]{
                "MM",
                "01",
                "02",
                "03",
                "04",
                "05",
                "06",
                "07",
                "08",
                "09",
                "10",
                "11",
                "12"};


        List<String> spinnerList = new ArrayList<>(Arrays.asList(spinnerItems));

        ArrayAdapter<String> arrayAdapter =
                new ArrayAdapter<String>(PaymentGatewayActivity.this, R.layout.spinner_drop_singlerow, spinnerList) {
                    @Override
                    public boolean isEnabled(int position) {
                        if (position == 0) {
                            //Disable the first item of spinner.
                            Log.i(TAG, "Position[0]: Spinner first item is disabled");
                            return false;
                        } else {
                            String itemSelected = month_edt.getItemAtPosition(position).toString();
                            Log.i(TAG, "Selected Item[" + position + "]: " + itemSelected);
                            return true;
                        }
                    }

                    @Override
                    public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
                        View spinnerView = super.getDropDownView(position, convertView, parent);

                        TextView spinnerTextV = (TextView) spinnerView;
                        if (position == 0) {
                            //Set the disable spinner item color fade .
                            spinnerTextV.setTextColor(Color.parseColor("#a7a7a6"));
                        } else {
                            //#404040
                            spinnerTextV.setTextColor(Color.parseColor("#b2000000"));
                        }
                        return spinnerView;
                    }
                };

        arrayAdapter.setDropDownViewResource(R.layout.spinner_drop_singlerow);

        month_edt.setAdapter(arrayAdapter);


    }

    private boolean validation() {
        boolean flag = true;

//        if(card_no.length() == 0 || month_edt.length() == 0 || year_edt.length() == 0 || cvc_edt.length() == 0 || card_holder_nme.length() == 0){
//
//            Toast.makeText(getApplicationContext(), "pls fill the empty fields", Toast.LENGTH_SHORT).show();
//        }

        if (card_no.getText().toString().trim().equals("")) {
            Toast.makeText(PaymentGatewayActivity.this, "Please enter card number", Toast.LENGTH_LONG).show();
            flag = false;
        } else if (month_edt.getSelectedItem().toString().trim().equals("")) {
            Toast.makeText(PaymentGatewayActivity.this, "Please enter Month", Toast.LENGTH_LONG).show();
            flag = false;
        } else if (year_edt.getSelectedItem().toString().trim().equals("")) {
            Toast.makeText(PaymentGatewayActivity.this, "Please enter year", Toast.LENGTH_LONG).show();
            flag = false;
        } else if (cvc_edt.getText().toString().trim().equals("")) {
            Toast.makeText(PaymentGatewayActivity.this, "Please enter cvv", Toast.LENGTH_LONG).show();
            flag = false;
        } else if (card_holder_nme.getText().toString().trim().equals("")) {
            Toast.makeText(PaymentGatewayActivity.this, "Please enter Card holder name", Toast.LENGTH_LONG).show();
            flag = false;
        }
        return flag;
    }

    //  METHOD TO REQUEST GENERATE_CHECKSUM_PAY_TM SERVICE ..
    private void serviceGenerateOrderNumber() {
        if (CommonUtilities.isNetworkAvailable(this)) {
            try {
                Api servicesInterface = ApiClientConnection.Companion.getInstance().createApiInterface();
                Call<CommonModel> commonModelCall = servicesInterface.ordernumber("application/json", "Bearer " + SharedPreferenceWriter.getInstance(this).getString(SPreferenceKey.TOKEN));

                ServicesConnection.getInstance().enqueueWithoutRetry(
                        commonModelCall,
                        this,
                        true,
                        new Callback<CommonModel>() {
                            @Override
                            public void onResponse(Call<CommonModel> call, Response<CommonModel> response) {
                                if (response.isSuccessful()) {
                                    CommonModel body = ((CommonModel) response.body());
                                    if (body.getStatus().equalsIgnoreCase(ParamEnum.TRUE.theValue())) {
                                        orderId = body.getOrder_number();
                                    } else {
                                        //CommonUtilities.snackBar(PaymentActivity.this,body.getMessage());
                                    }
                                }
                            }

                            @Override
                            public void onFailure(Call<CommonModel> call, Throwable t) {
                            }
                        });
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            //CommonUtilities.snackBar(this, getString(R.string.no_internet));
        }
    }


    //  METHOD TO REQUEST GENERATE_CHECKSUM_PAY_TM SERVICE ..
    private void serviceGenerateChecksum() {
        getRandomIdFor("ORDER");
        getRandomIdFor("CUSTOM");
        if (CommonUtilities.isNetworkAvailable(this)) {
            try {
                Api servicesInterface = ApiClientConnection.Companion.getInstance().createApiInterface();
                Call<CommonModel> commonModelCall = servicesInterface.generateChecksum(getParams());

                ServicesConnection.getInstance().enqueueWithoutRetry(
                        commonModelCall,
                        this,
                        true,
                        new Callback<CommonModel>() {
                            @Override
                            public void onResponse(Call<CommonModel> call, Response<CommonModel> response) {
                                if (response.isSuccessful()) {
                                    CommonModel body = ((CommonModel) response.body());
                                    if (body.getStatus().equalsIgnoreCase(ParamEnum.SUCCESS.theValue())) {
                                        doPayment(body.getChecksum());
                                    } else {
                                        //CommonUtilities.snackBar(PaymentActivity.this,body.getMessage());
                                    }
                                }
                            }

                            @Override
                            public void onFailure(Call<CommonModel> call, Throwable t) {
                            }
                        });
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            //CommonUtilities.snackBar(this, getString(R.string.no_internet));
        }
    }

    //  METHOD TO REQUEST GENERATE_CHECKSUM_PAY_TM SERVICE ..
    private void serviceGenerateChecksumPaytm() {
        if (CommonUtilities.isNetworkAvailable(this)) {
            try {
                Api servicesInterface = ApiClientConnection.Companion.getInstance().createApiInterface();
                Call<CommonModel> commonModelCall = servicesInterface.paytmchecksum("application/json", "Bearer " + SharedPreferenceWriter.getInstance(this).getString(SPreferenceKey.TOKEN));

                ServicesConnection.getInstance().enqueueWithoutRetry(
                        commonModelCall,
                        this,
                        true,
                        new Callback<CommonModel>() {
                            @Override
                            public void onResponse(Call<CommonModel> call, Response<CommonModel> response) {
                                if (response.isSuccessful()) {
                                    CommonModel body = ((CommonModel) response.body());
                                    if (body.getStatus().equalsIgnoreCase(ParamEnum.TRUE.theValue())) {
                                        doPayment(body.getChecksum());
                                    } else {
                                        //CommonUtilities.snackBar(PaymentActivity.this,body.getMessage());
                                    }
                                }
                            }

                            @Override
                            public void onFailure(Call<CommonModel> call, Throwable t) {
                            }
                        });
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            //CommonUtilities.snackBar(this, getString(R.string.no_internet));
        }
    }

    // generate random order_id and customer_id
    private void getRandomIdFor(String for_what) {
        String randomGenId = "";
        Random rand = null;
        String randomString = "";
        switch (for_what) {
            case "ORDER":
                rand = new Random();
                randomString = Integer.toString(rand.nextInt()) + (System.currentTimeMillis() / 1000L);

                orderId = "ORDER" + randomString.substring(1, 11);

                randomGenId = orderId;
                Log.e(TAG, "\n--ORDER id --: " + orderId);

                break;

            case "CUSTOM":
                rand = new Random();
                randomString = Integer.toString(rand.nextInt()) + (System.currentTimeMillis() / 1000L);

                customerId = "CUSTOM" + randomString.substring(1, 11);

                randomGenId = customerId;
                Log.e(TAG, "\n--CUSTOM id --: " + customerId);

                break;

        }
    }

    private Map<String, RequestBody> getParams() {
        String email_id = SharedPreferenceWriter.getInstance(this).getString(SPreferenceKey.USER_EMAIL);
        String mob_no = "9873529969";

        Map<String, RequestBody> fields = new HashMap<>();
        fields.put("MID", RequestBody.create(MediaType.parse("text/plain"), merchant_id != null ? merchant_id : ""));
        fields.put("ORDER_ID", RequestBody.create(MediaType.parse("text/plain"), orderId != null ? orderId : ""));
        fields.put("CUST_ID", RequestBody.create(MediaType.parse("text/plain"), customerId != null ? customerId : ""));
        fields.put("INDUSTRY_TYPE_ID", RequestBody.create(MediaType.parse("text/plain"), "Retail"));
        fields.put("CHANNEL_ID", RequestBody.create(MediaType.parse("text/plain"), "WAP"));
        fields.put("TXN_AMOUNT", RequestBody.create(MediaType.parse("text/plain"), String.valueOf(decimalPointAmount)));
        //fields.put("WEBSITE", RequestBody.create(MediaType.parse("text/plain"), "WEBSTAGING"));
        fields.put("WEBSITE", RequestBody.create(MediaType.parse("text/plain"), "DEFAULT"));
        //fields.put("CALLBACK_URL", RequestBody.create(MediaType.parse("text/plain"), "https://securegw-stage.paytm.in/theia/paytmCallback?ORDER_ID="+orderId));
        fields.put("CALLBACK_URL", RequestBody.create(MediaType.parse("text/plain"), "https://securegw.paytm.in/theia/paytmCallback?ORDER_ID=" + orderId));
        fields.put("EMAIL", RequestBody.create(MediaType.parse("text/plain"), email_id != null ? email_id : ""));
        fields.put("MOBILE_NO", RequestBody.create(MediaType.parse("text/plain"), mob_no != null ? mob_no : ""));
        //fields.put("token", RequestBody.create(MediaType.parse("text/plain"), SharedPreferenceWriter.getInstance(this).getString(SPreferenceKey.TOKEN)));
        //fields.put("language_type", RequestBody.create(MediaType.parse("text/plain"), SharedPreferenceWriter.getInstance(this).getString(SPreferenceKey.LANGUAGE)));

        return fields;
    }

    private void doPayment(String checkSum) {
        String email_id = SharedPreferenceWriter.getInstance(this).getString(SPreferenceKey.USER_EMAIL);
        String mob_no = "9873529969";
        //PaytmPGService service = PaytmPGService.getStagingService();  //For Staging environment:
        PaytmPGService service = PaytmPGService.getProductionService(); //For Production environment:

        HashMap<String, String> paramMap = new HashMap<String, String>();
        paramMap.put("MID", merchant_id);  //Key in your staging and production MID available in your dashboard:
        paramMap.put("ORDER_ID", orderId);
        paramMap.put("CUST_ID", customerId);
        paramMap.put("MOBILE_NO", mob_no);
        paramMap.put("EMAIL", "sanjay@gmail.com");
        paramMap.put("CHANNEL_ID", "WAP");
        paramMap.put("TXN_AMOUNT", String.valueOf(decimalPointAmount));
        //paramMap.put( "WEBSITE" , "WEBSTAGING");
        paramMap.put("WEBSITE", "DEFAULT");
        paramMap.put("INDUSTRY_TYPE_ID", "Retail");
        //paramMap.put( "CALLBACK_URL", "https://securegw-stage.paytm.in/theia/paytmCallback?ORDER_ID="+orderId);
        paramMap.put("CALLBACK_URL", "https://securegw.paytm.in/theia/paytmCallback?ORDER_ID=" + orderId);
        paramMap.put("CHECKSUMHASH", checkSum);
        PaytmOrder order = new PaytmOrder(paramMap);

        service.initialize(order, null); // Initialize Service

        // Call start transaction method using service object:
        service.startPaymentTransaction(this, true, true
                , this);
    }

    @Override
    public void onTransactionResponse(Bundle bundle) {
        // After successful transaction this method gets called.
        // Response bundle contains the merchant response parameters.
        Log.w(TAG, "--Transaction Response--: " + bundle.toString());
        String responseCode = bundle.getString("RESPCODE");
        String respmsg = bundle.getString("RESPMSG");
        String status = bundle.getString("STATUS");
        String orderId = bundle.getString("ORDERID");
        String amount = bundle.getString("TXNAMOUNT");
        String transactionID = bundle.getString("TXNID");
        if (responseCode != null) {
            switch (responseCode) {
                //01 : Transaction Successful
                case "01":
                case "1":
                    //Toast.makeText(this, "Transaction successful", Toast.LENGTH_LONG).show();
                    serviceAddPayment(status, orderId, amount, transactionID);
                    break;

                //227 :Payment Failed due to a Bank Failure. Please try after some time.
                case "227":
                    //Toast.makeText(this, "Payment Failed due to a Bank Failure. Try again after some time.", Toast.LENGTH_LONG).show();
                    serviceAddPayment(status, orderId, amount, transactionID);
                    break;

                //repeated request
                case "269":
                    //Invalid checksum
                case "330":
                    //REQUEST CANCELLED BY YOU
                case "14112":
                    //Toast.makeText(this, "" + respmsg, Toast.LENGTH_LONG).show();
                    serviceAddPayment(status, orderId, amount, transactionID);
                    break;

                default:
                    //Toast.makeText(this, "Payment Failed. Something went wrong!", Toast.LENGTH_LONG).show();
                    serviceAddPayment(status, orderId, amount, transactionID);
                    break;

            }
        }
    }

    @Override
    public void networkNotAvailable() {
        // If network is not available, then this method gets called.
        Toast.makeText(this, "Network connection error: Check your internet connectivity", Toast.LENGTH_LONG).show();
    }

    @Override
    public void clientAuthenticationFailed(String inResponse) {
        /*Client authentication failure:
          This can happen due to multiple reason -
          * Paytm services are not available due to a downtime
          * Server unable to generate checksum or checksum response is not in proper format
          * Server failed to authenticate the client. That is value of payt_STATUS is 2. // payt_STATUS hasn't been defined anywhere*/
        Toast.makeText(this, "Authentication failed: Server error" + inResponse, Toast.LENGTH_LONG).show();
    }

    @Override
    public void someUIErrorOccurred(String inErrorMessage) {
        // Some UI Error Occurred in Payment Gateway Activity.
        // This may be due to initialization of views in Payment Gateway Activity or may be due to initialization of webview.
        // Error Message details the error occurred.
        Toast.makeText(this, "UI Error " + inErrorMessage, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onErrorLoadingWebPage(int i, String inResponse, String s1) {
        // This page gets called if some error occurred while loading some URL in Webview.
        // Error Code and Error Message describes the error.
        // Failing URL is the URL that failed to load.
        Toast.makeText(this, "Unable to load webpage " + inResponse, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onBackPressedCancelTransaction() {
        Toast.makeText(this, "Transaction cancelled", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onTransactionCancel(String s, Bundle bundle) {

    }

    //    METHOD TO REQUEST ADD_PAYMENT SERVICE ..
    private void serviceAddPayment(String status, String orderId, String amount, String transactionID) {
        if (bookingType.equalsIgnoreCase("Home Visit"))
            bookingType = "home";
        else
            bookingType = "call";
        getRandomIdFor("ORDER");
        getRandomIdFor("CUSTOM");
        if (CommonUtilities.isNetworkAvailable(this)) {
            try {
                ServicesInterface servicesInterface = ServicesConnection.getInstance().createService();
                Call<CommonModel> commonModelCall = servicesInterface.addPayment(SharedPreferenceWriter.getInstance(this).getString(SPreferenceKey.TOKEN),
                        "", "", amount, status, transactionID, orderId, bookingType,
                        "");

                ServicesConnection.getInstance().enqueueWithoutRetry(
                        commonModelCall,
                        this,
                        true,
                        new Callback<CommonModel>() {
                            @Override
                            public void onResponse(Call<CommonModel> call, Response<CommonModel> response) {
                                if (response.isSuccessful()) {
                                    CommonModel body = ((CommonModel) response.body());
                                    if (body.getStatus().equalsIgnoreCase(ParamEnum.SUCCESS.theValue())) {
                                        Toast.makeText(PaymentGatewayActivity.this, "Transaction successfully", Toast.LENGTH_LONG).show();
                                        Intent intent = new Intent(PaymentGatewayActivity.this, MainActivity.class);
                                        //intent.putExtra(ParamEnum.TYPE.theValue(),"");
                                        startActivity(intent);
                                        //serviceNotificationRead();
                                    } else {
                                        //CommonUtilities.snackBar(PaymentActivity.this,body.getMessage());
                                    }
                                }
                            }

                            @Override
                            public void onFailure(Call<CommonModel> call, Throwable t) {
                            }
                        });
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            //CommonUtilities.snackBar(this, getString(R.string.no_internet));
        }
    }


    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }
}

