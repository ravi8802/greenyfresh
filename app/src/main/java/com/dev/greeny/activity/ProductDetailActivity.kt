package com.dev.greeny.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.dev.greeny.R

class ProductDetailActivity : AppCompatActivity() {
  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_product_detail)
  }
}