package com.dev.greeny.activity

import android.app.ProgressDialog
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.dev.greeny.R
import com.dev.greeny.adapter.ClothListingAdapter
import com.dev.greeny.entity.ProductListEntity
import com.dev.greeny.service.request.Api
import com.dev.greeny.service.request.ApiClientConnection
import com.dev.greeny.service.response.RegistrationResponse
import com.dev.greeny.utility.Utils
import com.dev.greeny.utility.Validate
import com.dev.greeny.utility.setToolbar
import com.dev.greeny.utility.snackBar
import kotlinx.android.synthetic.main.cloths.*
import kotlinx.android.synthetic.main.layout_toolbar.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ClothsListActivity : AppCompatActivity(), ClothListingAdapter.CartItemsInterface {
  internal lateinit var progressDialog: ProgressDialog
  var apiInterface: Api? = null
  var validate: Validate? = null
  var tcount: Int? = null
  private var clothListingAdapter: ClothListingAdapter? = null

  override fun onCallBackCartItem(cartData: ProductListEntity, quantity: String) {
    //Toast.makeText(this, "done", Toast.LENGTH_SHORT).show()
    createOrderApi(cartData.id, quantity)
  }


  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.clothslist)
    setToolbar(this, toolbar, tvTitle, getString(R.string.prolist))
    img_cart.visibility = View.VISIBLE
    tvCartCount.visibility = View.VISIBLE
    validate = Validate(this)
    apiInterface = ApiClientConnection.Companion.instance.createApiInterface()

    img_cart.setOnClickListener {
      val intent = Intent(this, CartActivity::class.java)
      //intent.flags = (Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
      startActivity(intent)
    }

    shoplistitem()


  }

  override fun onResume() {
    super.onResume()

    tvCartCount.setText(validate!!.RetriveSharepreferenceInt(Utils.Totalcount).toString())
  }

  private fun FillData(list: List<ProductListEntity>?) {
    rvProductList.layoutManager = LinearLayoutManager(this)
    //rvProductList.adapter = ClothListingAdapter(this, list)
    clothListingAdapter = ClothListingAdapter(this, list)
    rvProductList.adapter = clothListingAdapter
    clothListingAdapter?.onCartItem(this)

  }

  fun shoplistitem() {
    progressDialog = ProgressDialog.show(
            this, resources.getString(R.string.app_name),
            getString(R.string.datadownloading)
    )
    //        progressDialog.setIcon(getDrawable(R.drawable.appicon))

    val call = apiInterface?.ProductList(
            validate!!.RetriveSharepreferenceInt(Utils.ShopCategoryid)!!,
            validate!!.RetriveSharepreferenceInt(Utils.Shopkeeper_id)!!)
    call?.enqueue(object : Callback<RegistrationResponse> {
      override fun onFailure(call: Call<RegistrationResponse>, t: Throwable) {
        t.printStackTrace()
        progressDialog.dismiss()
      }

      override fun onResponse(
              call: Call<RegistrationResponse>, response: Response<RegistrationResponse>
      ) {
        when (response.code()) {

          200 -> {
            if (!response!!.body()?.productList.isNullOrEmpty()) {
              progressDialog.dismiss()
              val list = response!!.body()?.productList
              FillData(list)
              snackBar(this@ClothsListActivity, response!!.body().message)

            } else {
              progressDialog.dismiss()
              snackBar(this@ClothsListActivity, response!!.body().message)
            }
          }
          500 -> {
            progressDialog.dismiss()
          }
        }

      }

    })


  }

  fun createOrderApi(id: Int, quantity: String) {
    progressDialog = ProgressDialog.show(
            this, resources.getString(R.string.app_name),
            getString(R.string.datadownloading)
    )
    //        progressDialog.setIcon(getDrawable(R.drawable.appicon))

    val call = apiInterface?.createOrder(
            validate!!.RetriveSharepreferenceString(Utils.Token)!!, id, quantity)
    call?.enqueue(object : Callback<RegistrationResponse> {
      override fun onFailure(call: Call<RegistrationResponse>, t: Throwable) {
        t.printStackTrace()
        progressDialog.dismiss()
      }

      override fun onResponse(
              call: Call<RegistrationResponse>, response: Response<RegistrationResponse>
      ) {
        when (response.code()) {

          200 -> {
            if (!response!!.body()?.productList.isNullOrEmpty()) {
              progressDialog.dismiss()
              val list = response!!.body()?.productList
              FillData(list)
              tvCartCount.setText(validate!!.RetriveSharepreferenceInt(Utils.Totalcount).toString())
              snackBar(this@ClothsListActivity, response!!.body().message)

            } else {
              progressDialog.dismiss()
              snackBar(this@ClothsListActivity, response!!.body().message)
            }
          }
          500 -> {
            progressDialog.dismiss()
          }
        }

      }

    })


  }

  override fun onBackPressed() {
    val intent = Intent(this, ClothsActivity::class.java)
    intent.flags = (Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
    startActivity(intent)
  }
  fun updatecount(){
    tvCartCount.setText(validate!!.RetriveSharepreferenceInt(Utils.Totalcount).toString())
  }

}
