package com.dev.greeny.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.dev.greeny.R
import com.dev.greeny.utility.setToolbar
import kotlinx.android.synthetic.main.layout_toolbar.*

class ContactUsActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_contact_us)
        setToolbar(this, toolbar, tvTitle, getString(R.string.contact_us))



    }

    override fun onBackPressed() {
      finish()
    }
}