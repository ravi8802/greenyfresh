package com.dev.greeny.activity

import android.app.ProgressDialog
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import com.dev.greeny.R
import com.dev.greeny.adapter.FoodListingAdapter
import com.dev.greeny.entity.ProductListEntity
import com.dev.greeny.service.request.Api
import com.dev.greeny.service.request.ApiClientConnection
import com.dev.greeny.service.response.RegistrationResponse
import com.dev.greeny.utility.*
import kotlinx.android.synthetic.main.cloths.*
import kotlinx.android.synthetic.main.layout_toolbar.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class FoodListActivity : AppCompatActivity() {
  internal lateinit var progressDialog: ProgressDialog
  var apiInterface: Api? = null
  var validate: Validate? = null


  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.clothslist)
    setToolbar(this, toolbar, tvTitle, getString(R.string.shoplist))
    validate = Validate(this)
    apiInterface = ApiClientConnection.Companion.instance.createApiInterface()
    shoplistitem()
  }

  private fun FillData(list: List<ProductListEntity>?) {
    rvProductList.layoutManager = LinearLayoutManager(this)
    rvProductList.adapter = FoodListingAdapter(this, list)
  }

  fun shoplistitem() {
    progressDialog = ProgressDialog.show(
            this, resources.getString(R.string.app_name),
            getString(R.string.datadownloading)
    )
    //        progressDialog.setIcon(getDrawable(R.drawable.appicon))

    val call = apiInterface?.ProductList(
            validate!!.RetriveSharepreferenceInt(Utils.ShopCategoryid)!!,
            validate!!.RetriveSharepreferenceInt(Utils.Shopkeeper_id)!!)
    call?.enqueue(object : Callback<RegistrationResponse> {
      override fun onFailure(call: Call<RegistrationResponse>, t: Throwable) {
        t.printStackTrace()
        progressDialog.dismiss()
      }

      override fun onResponse(
              call: Call<RegistrationResponse>, response: Response<RegistrationResponse>
      ) {
        when (response.code()) {

          200 -> {
            if (!response!!.body()?.productList.isNullOrEmpty()) {
              progressDialog.dismiss()
              val list = response!!.body()?.productList
              FillData(list)
              snackBar(this@FoodListActivity, response!!.body().message)

            } else {
              progressDialog.dismiss()
              snackBar(this@FoodListActivity, response!!.body().message)
            }
          }
          500 -> {
            progressDialog.dismiss()
          }
        }

      }

    })


  }

  override fun onBackPressed() {
    val intent = Intent(this, FoodActivity::class.java)
    intent.flags = (Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
    startActivity(intent)
  }


}
