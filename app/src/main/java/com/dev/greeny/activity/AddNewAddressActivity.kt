package com.dev.greeny.activity

import android.app.ProgressDialog
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.dev.greeny.R
import com.dev.greeny.baen.LoginInner
import com.dev.greeny.service.request.Api
import com.dev.greeny.service.request.ApiClientConnection
import com.dev.greeny.utility.Utils
import com.dev.greeny.utility.Validate
import kotlinx.android.synthetic.main.activity_add_address.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class AddNewAddressActivity : AppCompatActivity() {
    internal lateinit var progressDialog: ProgressDialog
    var apiInterface: Api? = null
    var validate: Validate? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_address)

        validate = Validate(this)
        apiInterface = ApiClientConnection.Companion.instance.createApiInterface()

        linear_add.setOnClickListener {
            if(validation()) {
                callADDAddressApi()
            }
        }

    }

    private fun validation(): Boolean {
        if (edit_title.getText().toString().isEmpty()) {
            Toast.makeText(this, "Please provide Name", Toast.LENGTH_SHORT).show()
            return false
        } else if (text_address.getText().toString().isEmpty()) {
            Toast.makeText(this, "Please enter address", Toast.LENGTH_SHORT).show()
            return false

          } else if (edit_cityno.getText().toString().isEmpty()) {
            Toast.makeText(this, "Please provide city", Toast.LENGTH_SHORT).show()
            return false

          } else if (edt_state.getText().toString().isEmpty()) {
            Toast.makeText(this, "Please enter state", Toast.LENGTH_SHORT).show()
            return false

          } else if (edt_phn.getText().toString().isEmpty()) {
            Toast.makeText(this, "Please provide phone number", Toast.LENGTH_SHORT).show()
            return false

          } else if (edt_passcode.getText().toString().isEmpty()) {
            Toast.makeText(this, "Please provide pincode", Toast.LENGTH_SHORT).show()
            return false
        } else if (edit_landmark.getText().toString().isEmpty()) {
            Toast.makeText(this, "Please provide landmark", Toast.LENGTH_SHORT).show()
            return false
        }
        return true
    }


    fun callADDAddressApi() {
        progressDialog = ProgressDialog.show(
                this, resources.getString(R.string.app_name),
                getString(R.string.datadownloading)
        )
        progressDialog.setIcon(getDrawable(R.drawable.greenyfreshlogo))

        val call = apiInterface?.addAddress("application/json", "Bearer " +
                validate!!.RetriveSharepreferenceString(Utils.Token)!!, edit_title.text.toString(), text_address.text.toString(),
                edit_cityno.text.toString(), edt_state.text.toString(),edt_phn.text.toString(),  edt_passcode.text.toString(),edit_landmark.text.toString())
        call?.enqueue(object : Callback<LoginInner> {
            override fun onFailure(call: Call<LoginInner>, t: Throwable) {
                t.printStackTrace()
                progressDialog.dismiss()
            }

            override fun onResponse(
                    call: Call<LoginInner>, response: Response<LoginInner>
            ) {
                when (response.code()) {

                    200 -> {
                        if (response!!.body()?.status!!.equals("true", ignoreCase = true)) {
                            progressDialog.dismiss()

                            finish()

                            //snackBar(this@CartActivity, response!!.body().message)

                        } else {
                            progressDialog.dismiss()
                            //snackBar(this@CartActivity, response!!.body().message)
                        }
                    }
                    500 -> {
                        progressDialog.dismiss()
                    }
                }

            }

        })


    }


}