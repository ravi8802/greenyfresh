package com.dev.greeny.activity

import android.app.ProgressDialog
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.dev.greeny.R
import com.dev.greeny.adapter.CartAdapter
import com.dev.greeny.adapter.OrderHistoryAdapter
import com.dev.greeny.baen.OrderListResponse
import com.dev.greeny.service.request.Api
import com.dev.greeny.service.request.ApiClientConnection
import com.dev.greeny.service.response.RegistrationResponse
import com.dev.greeny.utility.Utils
import com.dev.greeny.utility.Validate
import com.dev.greeny.utility.setToolbar
import com.dev.greeny.utility.snackBar
import kotlinx.android.synthetic.main.clothslist.*

import kotlinx.android.synthetic.main.layout_toolbar.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class OrderHIstoryActivity :AppCompatActivity() {
  internal lateinit var progressDialog: ProgressDialog
  var apiInterface: Api? = null
  var validate: Validate? = null
  private var cartAdapter: CartAdapter? = null

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.clothslist)
    setToolbar(this, toolbar, tvTitle, getString(R.string.myorder))
    validate = Validate(this)
    apiInterface = ApiClientConnection.Companion.instance.createApiInterface()

    //orderHistoryListApi()
  }

//    private fun FillData(list: List<OrderListResponse>?) {
//        if (list != null && list.size > 0) {
//            linear_no_data_cloths.visibility = View.GONE
//            rvProductList.layoutManager = LinearLayoutManager(this)
//            rvProductList.adapter = OrderHistoryAdapter(this, list)
//        }else{
//
//            linear_no_data_cloths.visibility = View.VISIBLE
//        }
//    }

//    fun orderHistoryListApi() {
//        progressDialog = ProgressDialog.show(
//                this, resources.getString(R.string.app_name),
//                getString(R.string.datadownloading)
//        )
//
//        val call = apiInterface?.ordersList("application/json",
//                validate!!.RetriveSharepreferenceString(Utils.Token)!!)
//        call?.enqueue(object : Callback<RegistrationResponse> {
//            override fun onFailure(call: Call<RegistrationResponse>, t: Throwable) {
//                t.printStackTrace()
//                progressDialog.dismiss()
//            }
//
//            override fun onResponse(
//                    call: Call<RegistrationResponse>, response: Response<RegistrationResponse>
//            ) {
//                when (response.code()) {
//
//                    200 -> {
//                        if (!response!!.body()?.cusOrderHistory.isNullOrEmpty()) {
//                            progressDialog.dismiss()
//                            val list = response!!.body()?.cusOrderHistory
//
//                            //FillData(list)
//                            snackBar(this@OrderHIstoryActivity, response!!.body().message)
//
//                        } else {
////                            val list = response!!.body()?.orderList
////                            FillData(list)
//                            progressDialog.dismiss()
//                            snackBar(this@OrderHIstoryActivity, response!!.body().message)
//                        }
//                    }
//                    500 -> {
//                        progressDialog.dismiss()
//                    }
//                }
//
//            }
//
//        })
//
//
//    }
}