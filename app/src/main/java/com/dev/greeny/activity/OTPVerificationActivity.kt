package com.dev.greeny.activity

import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.dev.greeny.R
import com.dev.greeny.constant.Constants.kDEVICETOKEN
import com.dev.greeny.service.request.Api
import com.dev.greeny.service.request.ApiClientConnection
import com.dev.greeny.service.response.RegistrationResponse
import com.dev.greeny.utility.*
import com.dev.greeny.utility.ParamEnumcollection.ParamEnum
import com.google.firebase.FirebaseException
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.PhoneAuthCredential
import com.google.firebase.auth.PhoneAuthProvider
import com.google.gson.JsonParser
import kotlinx.android.synthetic.main.activity_otpverification.*
import kotlinx.android.synthetic.main.layout_toolbar.*
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.concurrent.TimeUnit


class OTPVerificationActivity : AppCompatActivity(), View.OnClickListener {
    internal lateinit var progressDialog: ProgressDialog
    var apiInterface: Api? = null
    var validate: Validate? = null


    private var countryCode: String? = null
    private var stateid: String? = null
    private var districtid: String? = null
    private var Categoryid: String? = null
    private var gstno: String? = null
    private var shopkeeper: String? = null
    private var number: String? = null
    private var name: String? = null
    private var email: String? = null
    private var dob: String? = null
    private var address: String? = null
    private var gender: String? = null
    private var password: String? = null
    private var confirmPassword: String? = null
    private var mAuth: FirebaseAuth? = null
    private var verificationCode: String? = null
    private var mResendingToken: PhoneAuthProvider.ForceResendingToken? = null

    private val mCallback = object : PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
        override fun onVerificationCompleted(p0: PhoneAuthCredential) {
            val otp = p0?.smsCode
            Log.d("", "" + otp)
        }

        override fun onVerificationFailed(p0: FirebaseException) {
            toast(p0.message.toString())
        }

        override fun onCodeSent(p0: String, p1: PhoneAuthProvider.ForceResendingToken) {
            super.onCodeSent(p0, p1)
            verificationCode = p0
            mResendingToken = p1
            Log.e("OTP Code", p0)
            toast("OTP Sent")
        }

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_otpverification)
        setToolbar(this, toolbar, tvTitle, getString(R.string.verification))
        mAuth = FirebaseAuth.getInstance()
        validate = Validate(this)
        validate!!.SaveSharepreferenceInt(Utils.flag, 1)
        apiInterface = ApiClientConnection.Companion.instance.createApiInterface()
        initListener()
        getIntentData()



    }

    private fun initListener() {
        btnVerify.setOnClickListener(this)
        tvResend.setOnClickListener(this)
    }

    override fun onClick(p0: View?) {
        when (p0) {
            btnVerify -> verifyVerificationCode()
            tvResend -> fireBaseAction()
        }
    }

    private fun getIntentData() {
        if (intent.extras != null) {
            countryCode = intent.getStringExtra(ParamEnum.COUNTRY_CODE.theValue())
            number = intent.getStringExtra(ParamEnum.NUMBER.theValue())
            name = intent.getStringExtra(ParamEnum.NAME.theValue())
            email = intent.getStringExtra(ParamEnum.EMAIL.theValue())
            dob = intent.getStringExtra(ParamEnum.DOB.theValue())
            address = intent.getStringExtra(ParamEnum.ADDRESS.theValue())
            stateid = intent.getStringExtra(ParamEnum.Stateid.theValue())
            districtid = intent.getStringExtra(ParamEnum.Districtid.theValue())
            Categoryid = intent.getStringExtra(ParamEnum.Categoryid.theValue())
            gstno = intent.getStringExtra(ParamEnum.GST_number.theValue())
            shopkeeper = intent.getStringExtra(ParamEnum.CUSTOMER.theValue())
            gender = intent.getStringExtra(ParamEnum.GENDER.theValue())
            password = intent.getStringExtra(ParamEnum.PASSWORD.theValue())
            confirmPassword = intent.getStringExtra(ParamEnum.CONFIRM_PASSWORD.theValue())
            fireBaseAction()
        }
    }

    private fun fireBaseAction() {
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                "+$countryCode$number",// Phone number to verify
                60,                // Timeout duration
                TimeUnit.SECONDS,   // Unit of timeout
                this,      // Activity (for callback binding)
                mCallback)// OnVerificationStateChangedCallback
    }

    private fun verifyVerificationCode() {
        //  CommonUtilities.showLoadingDialog(this)
        //creating the credential
        val credential = PhoneAuthProvider.getCredential(verificationCode!!, pvOtp.value);
        //signing the user
        signInWithPhone(credential)
    }

    private fun signInWithPhone(credential: PhoneAuthCredential) {
        mAuth?.signInWithCredential(credential)
                ?.addOnCompleteListener {
                    if (it.isSuccessful) {
                        serviceSignUp()
                    } else {
                        toast("Incorrect OTP")
                        //  CommonUtilities.dismissLoadingDialog()
                    }
                }

    }


    fun serviceSignUp() {
        val sharedPreferences = getSharedPreferences("Greeny", Context.MODE_PRIVATE)
        val deviceToken = sharedPreferences.getString(kDEVICETOKEN, "")
        if (CommonUtilities.isNetworkAvailable(this)) {
            progressDialog = ProgressDialog.show(
                    this@OTPVerificationActivity, resources.getString(R.string.app_name),
                    getString(R.string.datauploading)
            )

            val call = apiInterface?.register( validate!!.RetriveSharepreferenceString(Utils.ReferalCode)!!,name, email, number, dob, address, gender, password, "91","android",deviceToken)
            call?.enqueue(object : Callback<RegistrationResponse> {
                override fun onFailure(call: Call<RegistrationResponse>, t: Throwable) {
                    t.printStackTrace()
                    progressDialog.dismiss()
                }

                override fun onResponse(
                        call: Call<RegistrationResponse>, response: Response<RegistrationResponse>
                ) {
                    when (response.code()) {
                        200 -> {
                            if (response!!.body()?.status!!.equals("true",ignoreCase = true)) {
                                progressDialog.dismiss()
                                snackBar(this@OTPVerificationActivity, response!!.body().message)
                            validate!!.SaveSharepreferenceString(Utils.Token, response!!.body()?.signupdata!!.token!!)
                            validate!!.SaveSharepreferenceString(Utils.Fullname, response!!.body()?.signupdata!!.userdata!!.name!!)
                            validate!!.SaveSharepreferenceString(Utils.UserId, response!!.body()?.signupdata!!.userdata!!.user_id!!)
                            validate!!.SaveSharepreferenceString(Utils.Mobileno, response!!.body()?.signupdata!!.userdata!!.phone!!)
                                //validate!!.SaveSharepreferenceInt(Utils.userid, response!!.body()?.signupdata!!.user_id!!)
//                                validate!!.SaveSharepreferenceInt(Utils.Categoryid, response!!.body()?.signupdata!!.category_id!!)
//                                validate!!.SaveSharepreferenceString(Utils.Token, response!!.body()?.signupdata!!.token!!)
                               // validate!!.SaveSharepreferenceString(Utils.Password, response!!.body()?.signupdata!!.password!!)
                               // validate!!.SaveSharepreferenceString(Utils.Mobileno, response!!.body()?.signupdata!!.phone!!)
                                val intent = Intent(this@OTPVerificationActivity, CustomerDashboard::class.java)
                                startActivity(intent)
                            finish()
                                //   intent.setFlags()
                                //snackBar(this@OTPVerificationActivity, response!!.body().message)
                            //} else {
                                //progressDialog.dismiss()


                            } else {
                                progressDialog.dismiss()
                                snackBar(this@OTPVerificationActivity, response!!.body().message)

                            }
                        }
                        411 -> {
                            progressDialog.dismiss()
                            try {
                                val errorBody = response.errorBody().string()
                                var jsonObject = JSONObject(errorBody.trim())
                                val errorr = jsonObject.getString("message")
                                snackBar(this@OTPVerificationActivity, errorr)

                            } catch (e: JSONException) {
                                e.printStackTrace();
                            }


                        }
                    }

                }

            })
        } else {
            snackBar(this, getString(R.string.no_internet))
        }

    }
}
