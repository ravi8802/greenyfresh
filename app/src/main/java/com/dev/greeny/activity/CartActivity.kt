package com.dev.greeny.activity

import android.app.Dialog
import android.app.ProgressDialog
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.Window
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.dev.greeny.R
import com.dev.greeny.adapter.CartAdapter
import com.dev.greeny.baen.OrderListResponse
import com.dev.greeny.service.request.Api
import com.dev.greeny.service.request.ApiClientConnection
import com.dev.greeny.service.response.RegistrationResponse
import com.dev.greeny.utility.Utils
import com.dev.greeny.utility.Validate
import com.dev.greeny.utility.setToolbar
import com.dev.greeny.utility.snackBar
import kotlinx.android.synthetic.main.cart_activity.*
import kotlinx.android.synthetic.main.layout_toolbar.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class CartActivity : AppCompatActivity(), CartAdapter.CartItemsInterface,CartAdapter.AddItemsInterface,CartAdapter.SubtractItemsInterface {
    internal lateinit var progressDialog: ProgressDialog
    var apiInterface: Api? = null
    var validate: Validate? = null
    private var cartAdapter: CartAdapter? = null
    var totalPrice: String? = null
    var pre_order_total:Double = 0.0

    var orderList: List<OrderListResponse> = ArrayList<OrderListResponse>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.cart_activity)
        setToolbar(this, toolbar, tvTitle, getString(R.string.mycart))
        img_cart.visibility = View.GONE
        tvCartCount.visibility = View.GONE

        validate = Validate(this)
        apiInterface = ApiClientConnection.Companion.instance.createApiInterface()



        text_next.setOnClickListener {
//            if (orderList != null && orderList.size > 0) {
            if(pre_order_total != 0.0) {
                val intent = Intent(this, OrderPlaceActivity::class.java)
                intent.putExtra("from", "Cart")
                intent.putExtra("totalPrice", totalPrice)
                startActivity(intent)
                linear_cart_info.visibility = View.VISIBLE
                linear_no_data.visibility = View.GONE
            }
//            } else {
//                linear_cart_info.visibility = View.GONE
//                linear_no_data.visibility = View.VISIBLE
//            }
        }

        //(validate!!.RetriveSharepreferenceInt(Utils.totalcount)!!)
        //orderListApi()
    }

    override fun onResume() {
        super.onResume()

        orderListApi()
    }

    override fun onCallBackCartItem(cartData: OrderListResponse) {
        showDeleteDialog(cartData.id)

    }
    override fun onCallBackAddCartItem(cartData: OrderListResponse) {

        callUpdateCartApi(cartData.id,cartData.quantity);
    }
    override fun onCallBackSubtractCartItem(cartData: OrderListResponse) {

        callUpdateCartSubApi(cartData.id,cartData.quantity);
    }


    private fun FillData(list: List<OrderListResponse>?) {
        if (list != null && list.size > 0) {
            linear_cart_info.visibility = View.VISIBLE
            linear_no_data.visibility = View.GONE
            rvCartList.layoutManager = LinearLayoutManager(this)
            cartAdapter = CartAdapter(this, list)
            rvCartList.adapter = cartAdapter
            cartAdapter?.onCartItem(this)
            cartAdapter?.onAddCartItem(this)
            cartAdapter?.onSubtractCartItem(this)
            //cartAdapter?.notifyDataSetChanged()

        } else {
            linear_cart_info.visibility = View.GONE
            linear_no_data.visibility = View.VISIBLE
        }
    }


    //product list showing Api


    fun orderListApi() {
        progressDialog = ProgressDialog.show(
                this, resources.getString(R.string.app_name),
                getString(R.string.datadownloading)
        )
                progressDialog.setIcon(getDrawable(R.drawable.greenyfreshlogo))

        val call = apiInterface?.cardList("Bearer "+
                validate!!.RetriveSharepreferenceString(Utils.Token)!!)
        call?.enqueue(object : Callback<RegistrationResponse> {
            override fun onFailure(call: Call<RegistrationResponse>, t: Throwable) {
                t.printStackTrace()
                progressDialog.dismiss()
            }

            override fun onResponse(
                    call: Call<RegistrationResponse>, response: Response<RegistrationResponse>
            ) {
                when (response.code()) {

                    200 -> {
                        if (response!!.body()?.status!!.equals("true", ignoreCase = true)) {
                            progressDialog.dismiss()
                            val list = response!!.body()?.cartList
                            totalPrice = response.body().final_amount.toString()

                            text_sub_total.setText(response.body().sub_totalAmt.toString())
                            pre_order_total =  response.body().final_amount!!.toDouble()

                            FillData(list)
                            //snackBar(this@CartActivity, response!!.body().message)

                        } else {
                            val list = response!!.body()?.orderList
                            FillData(list)
                            progressDialog.dismiss()
                            //snackBar(this@CartActivity, response!!.body().message)
                        }
                    }
                    500 -> {
                        progressDialog.dismiss()
                    }
                }

            }

        })


    }


    fun callRemoveOrderListApi(id: Int?) {
        progressDialog = ProgressDialog.show(
                this, resources.getString(R.string.app_name),
                getString(R.string.datadownloading)
        )
        //        progressDialog.setIcon(getDrawable(R.drawable.appicon))

        val call = apiInterface?.removeItemList(id.toString(),"Bearer "+
                validate!!.RetriveSharepreferenceString(Utils.Token)!!)
        call?.enqueue(object : Callback<RegistrationResponse> {
            override fun onFailure(call: Call<RegistrationResponse>, t: Throwable) {
                t.printStackTrace()
                progressDialog.dismiss()
            }

            override fun onResponse(
                    call: Call<RegistrationResponse>, response: Response<RegistrationResponse>
            ) {
                when (response.code()) {

                    200 -> {
                        if (response!!.body().status.equals("true")) {
                            progressDialog.dismiss()

                            //validate!!.SaveSharepreferenceInt(Utils.Totalcount,response.body().totalcount!!)

                            orderListApi()
                            snackBar(this@CartActivity, response!!.body().message)

                        } else {
                            progressDialog.dismiss()
                            snackBar(this@CartActivity, response!!.body().message)
                        }
                    }
                    500 -> {
                        progressDialog.dismiss()
                    }
                }

            }

        })


    }


    fun callUpdateCartApi(id: Int?,qty: Int?) {
        progressDialog = ProgressDialog.show(
                this, resources.getString(R.string.app_name),
                getString(R.string.datadownloading)
        )
        progressDialog.setIcon(getDrawable(R.drawable.greenyfreshlogo))

        val call = apiInterface?.updatecart("application/json",
                "Bearer "+
                validate!!.RetriveSharepreferenceString(Utils.Token)!!,id,qty?.plus(1))
        call?.enqueue(object : Callback<RegistrationResponse> {
            override fun onFailure(call: Call<RegistrationResponse>, t: Throwable) {
                t.printStackTrace()
                progressDialog.dismiss()
            }

            override fun onResponse(
                    call: Call<RegistrationResponse>, response: Response<RegistrationResponse>
            ) {
                when (response.code()) {

                    200 -> {
                        if (response!!.body()?.status!!.equals("true", ignoreCase = true)) {
                            progressDialog.dismiss()
                            orderListApi()
                            //snackBar(this@CartActivity, response!!.body().message)

                        } else {
                            progressDialog.dismiss()
                            //snackBar(this@CartActivity, response!!.body().message)
                        }
                    }
                    500 -> {
                        progressDialog.dismiss()
                    }
                }

            }

        })


    }


    fun callUpdateCartSubApi(id: Int?,qty: Int?) {
        progressDialog = ProgressDialog.show(
                this, resources.getString(R.string.app_name),
                getString(R.string.datadownloading)
        )
        progressDialog.setIcon(getDrawable(R.drawable.greenyfreshlogo))

        val call = apiInterface?.updatecart("application/json",
                "Bearer "+
                        validate!!.RetriveSharepreferenceString(Utils.Token)!!,id,qty?.minus(1))
        call?.enqueue(object : Callback<RegistrationResponse> {
            override fun onFailure(call: Call<RegistrationResponse>, t: Throwable) {
                t.printStackTrace()
                progressDialog.dismiss()
            }

            override fun onResponse(
                    call: Call<RegistrationResponse>, response: Response<RegistrationResponse>
            ) {
                when (response.code()) {

                    200 -> {
                        if (response!!.body()?.status!!.equals("true", ignoreCase = true)) {
                            progressDialog.dismiss()
                            orderListApi()
                            //snackBar(this@CartActivity, response!!.body().message)

                        } else {
                            progressDialog.dismiss()
                            //snackBar(this@CartActivity, response!!.body().message)
                        }
                    }
                    500 -> {
                        progressDialog.dismiss()
                    }
                }

            }

        })


    }

    private fun showDeleteDialog(id: Int?) {
        val dialog = Dialog(this)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(false)
        dialog.setContentView(R.layout.dialog_delete_address_layout)
        val body = dialog.findViewById(R.id.header) as TextView

        val yesBtn = dialog.findViewById(R.id.tv_ok) as TextView
        val noBtn = dialog.findViewById(R.id.tv_cancel) as TextView
        yesBtn.setOnClickListener {
            callRemoveOrderListApi(id);
            dialog.dismiss()
        }
        noBtn.setOnClickListener { dialog.dismiss() }
        dialog.show()

    }

}