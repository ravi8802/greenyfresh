package com.dev.greeny.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.dev.greeny.R
import com.dev.greeny.utility.setToolbar
import kotlinx.android.synthetic.main.layout_toolbar.*

class AddProductActivity : AppCompatActivity() {

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_add_product)
    setToolbar(this,toolbar,tvTitle,getString(R.string.add_product))
  }
}
