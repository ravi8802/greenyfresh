package com.dev.greeny.activity

import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.GravityCompat
import com.dev.greeny.R
import com.dev.greeny.constant.Constants
import com.dev.greeny.fragment.*
import com.dev.greeny.service.request.Api
import com.dev.greeny.service.request.ApiClientConnection
import com.dev.greeny.service.response.RegistrationResponse
import com.dev.greeny.utility.*
import com.google.android.material.navigation.NavigationView
import kotlinx.android.synthetic.main.activity_main2.*
import kotlinx.android.synthetic.main.app_bar_main2.*
import kotlinx.android.synthetic.main.nav_header_main2.view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class CustomerDashboard : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {
    internal lateinit var progressDialog: ProgressDialog
    var apiInterface: Api? = null
    var validate: Validate? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main2)
        setSupportActionBar(toolbar)
        validate = Validate(this)

        validate!!.SaveSharepreferenceInt(Utils.flag, 1)
        apiInterface = ApiClientConnection.Companion.instance.createApiInterface()

        val toggle = ActionBarDrawerToggle(
                this,
                drawer_layout,
                toolbar,
                R.string.navigation_drawer_open,
                R.string.navigation_drawer_close
        )
        drawer_layout.addDrawerListener(toggle)
        toggle.syncState()
        val header: View = nav_view.getHeaderView(0)
        nav_view.setNavigationItemSelectedListener(this)
        replaceFragmenty(
                fragment = HomeFragment(),
                allowStateLoss = true,
                containerViewId = R.id.mainContent
        )
        header.navname.text = validate!!.RetriveSharepreferenceString(Utils.Fullname)


        //callCategoriesApi()
        callCarHeaderApi()

        img_cart.setOnClickListener {
            val intent = Intent(this@CustomerDashboard, CartActivity::class.java)
            startActivity(intent)
        }
        tvCartCount.setOnClickListener {
            val intent = Intent(this@CustomerDashboard, CartActivity::class.java)
            startActivity(intent)
        }


    }

    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.
        when (item.itemId) {
            R.id.nav_home -> {
                val intent = Intent(this@CustomerDashboard, CartActivity::class.java)
                startActivity(intent)
                //finish()
//                replaceFragmenty(
//                        fragment = HomeFragment(),
//                        allowStateLoss = true,
//                        containerViewId = R.id.mainContent
//
//                )


            }
//            R.id.nav_invitefrnd -> {
//                val intent = Intent(this@CustomerDashboard, InviteFriendActivity::class.java)
//                startActivity(intent)
 //           }
            R.id.nav_fav -> {
            val intent = Intent(this@CustomerDashboard, WishListActivity::class.java)
            startActivity(intent)
        }
            R.id.nav_order -> {
                val intent = Intent(this@CustomerDashboard, OrderHistoryListActivity::class.java)
                startActivity(intent)
            }

            /* R.id.nav_profile -> {
                 replaceFragmenty(
                         fragment = ProfileUpdate(),
                         allowStateLoss = true,
                         containerViewId = R.id.mainContent

                 )

             }
             R.id.nav_order -> {
                 val startMain = Intent(this@CustomerDashboard, OrderHIstoryActivity::class.java)
                 startActivity(startMain)
 //                replaceFragmenty(
 //                        fragment = OrderHistoryFragment(),
 //                        allowStateLoss = true,
 //                        containerViewId = R.id.mainContent
 //
 //                )


             }
             // Handle the camera action
 */
            R.id.prv_poly -> {
                val intent = Intent(this@CustomerDashboard, WebBrowserActivity::class.java)
                intent.putExtra("url","Privacy Policy")
                startActivity(intent)

            }
            R.id.trm_conds -> {
                val intent = Intent(this@CustomerDashboard, WebBrowserActivity::class.java)
                intent.putExtra("url","Term Condition")
                startActivity(intent)

            }
            R.id.nav_contact -> {
                val intent = Intent(this@CustomerDashboard, ContactUsActivity::class.java)
                startActivity(intent)
            }
            R.id.nav_about -> {
                val intent = Intent(this@CustomerDashboard, WebBrowserActivity::class.java)
                intent.putExtra("url","About Us")
                startActivity(intent)
            }
            R.id.invite_frd -> {
                val intent = Intent(this@CustomerDashboard, InviteFriendActivity::class.java)
                startActivity(intent)
            }
            R.id.nav_logout -> {
                callLogoutApi()

            }
        }

        drawer_layout.closeDrawer(GravityCompat.START)

        return true

    }



    fun callCarHeaderApi() {
        progressDialog = ProgressDialog.show(
                this, resources.getString(R.string.app_name),
                getString(R.string.datadownloading)
        )
        //        progressDialog.setIcon(getDrawable(R.drawable.appicon))

        val call = apiInterface?.cartheader("application/json", "Bearer " + validate!!.RetriveSharepreferenceString(Utils.Token))
        call?.enqueue(object : Callback<RegistrationResponse> {
            override fun onFailure(call: Call<RegistrationResponse>, t: Throwable) {
                t.printStackTrace()
                progressDialog.dismiss()
            }

            override fun onResponse(
                    call: Call<RegistrationResponse>, response: Response<RegistrationResponse>
            ) {
                when (response.code()) {

                    200 -> {
                        if (response!!.body()?.status!!.equals("true", ignoreCase = true)) {
                            progressDialog.dismiss()
                            tvCartCount.text = response!!.body()?.cartdata!!.cartTotal.toString()
                            //tpvoint.text = response!!.body()?.cartdata!!.total.toString()


                            //snackBar(this@CustomerDashboard, response!!.body().message)

                        } else {
                            progressDialog.dismiss()
                            //snackBar(this@CustomerDashboard, response!!.body().message)
                        }
                    }
                    500 -> {
                        progressDialog.dismiss()
                    }
                }

            }

        })


    }



    fun callLogoutApi() {
        progressDialog = ProgressDialog.show(
                this, resources.getString(R.string.app_name),
                getString(R.string.datadownloading)
        )
        //        progressDialog.setIcon(getDrawable(R.drawable.appicon))

        val call = apiInterface?.logout("application/json", "Bearer " + validate!!.RetriveSharepreferenceString(Utils.Token))
        call?.enqueue(object : Callback<RegistrationResponse> {
            override fun onFailure(call: Call<RegistrationResponse>, t: Throwable) {
                t.printStackTrace()
                progressDialog.dismiss()
            }

            override fun onResponse(
                    call: Call<RegistrationResponse>, response: Response<RegistrationResponse>
            ) {
                when (response.code()) {

                    200 -> {
                        var str = validate!!.RetriveSharepreferenceString(Utils.TOKEN)
                        validate!!.ClearSharedPrefrence()
                        validate!!.SaveSharepreferenceString(Utils.TOKEN, str!!)
                        val startMain = Intent(this@CustomerDashboard, LoginActivity::class.java)
                        startActivity(startMain)
                        if (response!!.body()?.status!!.equals("true", ignoreCase = true)) {
                            progressDialog.dismiss()

                            snackBar(this@CustomerDashboard, response!!.body().message)

                        } else {
                            progressDialog.dismiss()
                            snackBar(this@CustomerDashboard, response!!.body().message)
                        }
                    }
                    500 -> {
                        progressDialog.dismiss()
                    }
                }

            }

        })


    }
}
