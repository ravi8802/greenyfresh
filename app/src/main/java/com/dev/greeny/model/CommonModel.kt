package com.dev.greeny.model

import com.google.gson.annotations.SerializedName

data class CommonModel(@SerializedName("status")
                       var status:String? = null,
                       @SerializedName("message")
                       var message:String? = null,
                       @SerializedName("requestKey")
                       var requestKey:String? = null,
                       @SerializedName("signup")
                       var signup:CommonResponse? = null,
                       @SerializedName("login")
                       var login:CommonResponse? = null)

data class CommonResponse(@SerializedName("name")
                          var name:String? = null,
                          @SerializedName("email")
                          var email:String? = null,
                          @SerializedName("date_of_birth")
                          var date_of_birth:String? = null,
                          @SerializedName("address")
                          var address:String? = null,
                          @SerializedName("gender")
                          var gender:String? = null,
                          @SerializedName("user_type")
                          var user_type:String? = null,
                          @SerializedName("GST_number")
                          var GST_number:String? = null,
                          @SerializedName("token")
                          var token:String? = null,
                          @SerializedName("user_id")
                          var user_id:String? = null)