package com.dev.greeny.sharedpreferences;


public class SPreferenceKey {

	public static final String IS_LOGIN = "login";
    public static final String DEVICE_TOKEN = "device_token";
	public static final String TOKEN = "token";
	public static final String USER_ID = "user_id";
	public static final String USER_NAME = "user_name";
	public static final String USER_EMAIL = "user_mail";
	public static final String USER_TYPE = "user_type";


}
