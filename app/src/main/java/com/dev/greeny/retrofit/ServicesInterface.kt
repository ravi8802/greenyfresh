package com.dev.greeny.retrofit

import com.dev.greeny.model.CommonModel
import okhttp3.RequestBody

import retrofit2.Call
import retrofit2.http.*

/**
 * Created by mahipal on 26/Sep/18.
 */

interface ServicesInterface {

    @FormUrlEncoded
    @POST("signup")
    fun signUp(
        @Field("name") name: String,
        @Field("email") email: String,
        @Field("phone") phone: String,
        @Field("date_of_birth") dob: String,
        @Field("address") address: String,
        @Field("gender") gender: String,
        @Field("password") password: String,
        @Field("confirm_password") confirm_password: String,
        @Field("user_type") user_type: String,
        @Field("GST_number") GST_number: String
    ): Call<CommonModel>

    @FormUrlEncoded
    @POST("login")
    fun login(
        @Field("phone") phone: String,
        @Field("password") password: String,
        @Field("deviceType") deviceType: String,
        @Field("deviceToken") deviceToken: String
    ): Call<CommonModel>

    @Multipart
    @POST("generateChecksum")
    @JvmSuppressWildcards
    fun generateChecksum(@PartMap part: Map<String?, RequestBody?>?): Call<com.dev.greeny.baen.CommonModel?>?

    @FormUrlEncoded
    @POST("addPayment")
    fun addPayment(@Field("token") token: String?,
                   @Field("booking_id") booking_id: String?,
                   @Field("doctor_id") doctor_id: String?,
                   @Field("amount") amount: String?,
                   @Field("status") status: String?,
                   @Field("transaction_id") transaction_id: String?,
                   @Field("order_id") order_id: String?,
                   @Field("type") type: String?,
                   @Field("language_type") language_type: String?): Call<com.dev.greeny.baen.CommonModel?>?

}






