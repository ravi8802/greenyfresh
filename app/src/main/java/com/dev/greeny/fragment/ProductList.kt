package com.dev.greeny.fragment


import android.app.ProgressDialog
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager

import com.dev.greeny.R
import com.dev.greeny.adapter.ProductListingAdapter
import com.dev.greeny.entity.ProductListEntity
import com.dev.greeny.service.request.Api
import com.dev.greeny.service.request.ApiClientConnection
import com.dev.greeny.service.response.RegistrationResponse
import com.dev.greeny.utility.Utils
import com.dev.greeny.utility.Validate
import com.dev.greeny.utility.snackBar
import kotlinx.android.synthetic.main.fragment_product_list.view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * A simple [Fragment] subclass.
 */
class ProductList : Fragment() {
    internal lateinit var progressDialog: ProgressDialog
    var apiInterface: Api? = null
    var validate: Validate? = null
    private lateinit var layoutView: View

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        layoutView =  inflater.inflate(R.layout.fragment_product_list, container, false)
        apiInterface = ApiClientConnection.Companion.instance.createApiInterface()
        validate = Validate(activity!!)
        shoplistitem()
        return layoutView
    }

    private fun FillData(list: List<ProductListEntity>?) {
        layoutView.rvProductList.layoutManager = LinearLayoutManager(context)
        layoutView.rvProductList.adapter = ProductListingAdapter(context!!,list)
    }



    fun shoplistitem() {
        progressDialog = ProgressDialog.show(
                activity!!, resources.getString(R.string.app_name),
                getString(R.string.datadownloading)
        )
        //        progressDialog.setIcon(getDrawable(R.drawable.appicon))

        val call = apiInterface?.ProductList(
                validate!!.RetriveSharepreferenceInt(Utils.Categoryid)!!,
                validate!!.RetriveSharepreferenceInt(Utils.Shopkeeper_id)!! )
        call?.enqueue(object : Callback<RegistrationResponse> {
            override fun onFailure(call: Call<RegistrationResponse>, t: Throwable) {
                t.printStackTrace()
                progressDialog.dismiss()
            }

            override fun onResponse(
                    call: Call<RegistrationResponse>, response: Response<RegistrationResponse>
            ) {
                when (response.code()) {

                    200 -> {
                        if (!response!!.body()?.productList.isNullOrEmpty()) {
                            progressDialog.dismiss()
                            val list = response!!.body()?.productList
                            FillData(list)
                            snackBar(activity!!, response!!.body().message)

                        } else {
                            progressDialog.dismiss()
                            snackBar(activity!!, response!!.body().message)
                        }
                    }
                    500 ->{
                        progressDialog.dismiss()
                    }
                }

            }

        })


    }

}
