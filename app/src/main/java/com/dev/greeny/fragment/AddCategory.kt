package com.dev.greeny.fragment


import android.app.ProgressDialog

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.dev.greeny.R
import com.dev.greeny.service.request.Api
import com.dev.greeny.service.request.ApiClientConnection
import com.dev.greeny.service.response.RegistrationResponse
import com.dev.greeny.utility.*
import com.tsongkha.spinnerdatepicker.DatePicker
import kotlinx.android.synthetic.main.fragmentaddcategory.*
import kotlinx.android.synthetic.main.fragmentaddcategory.view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class AddCategory : Fragment(), View.OnClickListener, com.tsongkha.spinnerdatepicker.DatePickerDialog.OnDateSetListener {
    internal lateinit var progressDialog: ProgressDialog
    var apiInterface: Api? = null
    var validate: Validate? = null

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {

        val root = inflater.inflate(R.layout.fragmentaddcategory, container, false)
        apiInterface = ApiClientConnection.Companion.instance.createApiInterface()
        validate = Validate(activity!!)

        initClickListener(root)
        return root
    }


    fun addcategorylist() {
        progressDialog = ProgressDialog.show(
                activity!!, resources.getString(R.string.app_name),
                getString(R.string.datauploading)
        )
        //        progressDialog.setIcon(getDrawable(R.drawable.appicon))

        val call = apiInterface?.addCategory(validate!!.RetriveSharepreferenceString(Utils.Token)!!, etProductname.text.toString())
        call?.enqueue(object : Callback<RegistrationResponse> {
            override fun onFailure(call: Call<RegistrationResponse>, t: Throwable) {
                t.printStackTrace()
                progressDialog.dismiss()
            }

            override fun onResponse(
                    call: Call<RegistrationResponse>, response: Response<RegistrationResponse>
            ) {
                progressDialog.dismiss()
                if (response.isSuccessful) {

                    if (!response!!.body()?.status.equals("SUCCESS")) {
                        snackBar(activity!!, response!!.body().message)
                        replaceFragments(
                                fragment = ProductList(),
                                allowStateLoss = true,
                                containerViewId = R.id.mainContent
                        )

                    } else {

                        snackBar(activity!!, response!!.body().message)
                    }


                }

            }

        })


    }


    private fun initClickListener(root: View) {
        root.btnAdd.setOnClickListener(this)

    }

    override fun onClick(p0: View?) {
        when (p0) {
            btnAdd -> {
                if (validation())
                    addcategorylist()

            }


        }
    }

    private fun validation(): Boolean {
        when {
            etProductname.text.toString().isEmpty() -> {
                snackBar(activity!!, "Please enter Category name")
                return false
            }


            else -> return true
        }
    }


    override fun onDateSet(view: DatePicker?, year: Int, monthOfYear: Int, dayOfMonth: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }


}