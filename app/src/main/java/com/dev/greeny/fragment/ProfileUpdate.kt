package com.dev.greeny.fragment


import android.app.Activity
import android.app.ProgressDialog
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.os.StrictMode
import android.provider.MediaStore
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.dev.greeny.R
import com.dev.greeny.service.request.Api
import com.dev.greeny.service.request.ApiClientConnection
import com.dev.greeny.service.response.RegistrationResponse
import com.dev.greeny.utility.Utils
import com.dev.greeny.utility.Validate
import com.dev.greeny.utility.replaceFragments
import com.dev.greeny.utility.snackBar
import kotlinx.android.synthetic.main.fragmentprofileupdate.*
import kotlinx.android.synthetic.main.fragmentprofileupdate.view.*
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.*
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.HashMap

class ProfileUpdate : Fragment(), View.OnClickListener {
    internal lateinit var progressDialog: ProgressDialog
    var apiInterface: Api? = null
    var validate: Validate? = null

    var otherimage1 = ""
    var bitmap: Bitmap? = null
    var IMAGE_DIRECTORY_NAME = "Kohinoor"
    private var fileUri: Uri? = null
    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {

        val root = inflater.inflate(R.layout.fragmentprofileupdate, container, false)
        apiInterface = ApiClientConnection.Companion.instance.createApiInterface()
        validate = Validate(activity!!)

        initClickListener(root)
        return root
    }




    fun preData(): HashMap<String, RequestBody> {
        var userData=HashMap<String, RequestBody>()
        userData.put("token", RequestBody.create(MediaType.parse("text/plain"), validate!!.RetriveSharepreferenceString(Utils.Token)));
        userData.put("name", RequestBody.create(MediaType.parse("text/plain"), etname.text.toString()));
        userData.put("address", RequestBody.create(MediaType.parse("text/plain"), etaddress.text.toString()));


        return userData;


    }

    fun updateprofile() {
        progressDialog = ProgressDialog.show(
                activity!!, resources.getString(R.string.app_name),
                getString(R.string.datauploading)
        )
        //        progressDialog.setIcon(getDrawable(R.drawable.appicon))
        val mediaFile: File
        val mediaStorageDir = File(
                Environment
                        .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                IMAGE_DIRECTORY_NAME
        )
        mediaFile = File(
                mediaStorageDir.path + File.separator
                        + otherimage1
        )
        val datamap=preData()
        val file1 = RequestBody.create(MediaType.parse("multipart/form-data"), mediaFile)
        val imagePart = MultipartBody.Part.createFormData("image", mediaFile.name, file1)
        val call = apiInterface?.updateprofile(datamap,imagePart)
        call?.enqueue(object : Callback<RegistrationResponse> {
            override fun onFailure(call: Call<RegistrationResponse>, t: Throwable) {
                t.printStackTrace()
                progressDialog.dismiss()
            }

            override fun onResponse(
                    call: Call<RegistrationResponse>, response: Response<RegistrationResponse>
            ) {
                when (response.code()) {
                    200 -> {
                        if (response!!.body()?.status!!.equals("SUCCESS", ignoreCase = true)) {
                            progressDialog.dismiss()
                            replaceFragments(
                                    fragment = HomeFragment(),
                                    allowStateLoss = true,
                                    containerViewId = R.id.mainContent
                            )
                            snackBar(activity!!, response!!.body().message)

                        } else {
                            progressDialog.dismiss()
                            snackBar(activity!!, response!!.body().message)
                        }
                    }
                    404 -> {
                        snackBar(activity!!, response!!.errorBody().toString())
                    }
                    500 -> {
                        progressDialog.dismiss()
                        snackBar(activity!!, getString(R.string.somethingerror))
                    }
                }

            }

        })


    }

    private fun initClickListener(root: View) {
        root.btnupdate.setOnClickListener(this)
        root.imgcamera.setOnClickListener(this)


    }

    override fun onClick(p0: View?) {
        when (p0) {
            btnupdate -> {
                if (validation())
                    updateprofile()

            }

            imgcamera -> captureimage(101)

        }
    }

    private fun validation(): Boolean {
        when {
            etname.text.toString().isEmpty() -> {
                snackBar(activity!!, "Please enter  name")
                return false
            }
            etaddress.text.toString().isEmpty() -> {
                snackBar(activity!!, "Please enter  Address")
                return false
            }


           otherimage1.isEmpty() -> {
                snackBar(activity!!, "Please capture Image")
                return false
            }

            else -> return true
        }
    }




    fun captureimage(code: Int) {
        if (Build.VERSION.SDK_INT >= 24) {
            try {
                val m = StrictMode::class.java.getMethod("disableDeathOnFileUriExposure")
                m.invoke(null)
            } catch (e: Exception) {
                e.printStackTrace()
            }

        }
        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        fileUri = getOutputMediaFileUri(MediaStore.Files.FileColumns.MEDIA_TYPE_IMAGE, code)
        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri)
        startActivityForResult(intent, code)

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        try {
            compreesedimage()
            bitmap = BitmapFactory.decodeFile(fileUri!!.path)
            if (requestCode == 101 && resultCode == Activity.RESULT_OK) {
                image1.setImageBitmap(bitmap)
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun getOutputMediaFileUri(type: Int, flag: Int): Uri {
        return Uri.fromFile(getOutputMediaFile(type, flag))
    }

    private fun getOutputMediaFile(type: Int, flag: Int): File? {

        // External sdcard location
        val mediaStorageDir = File(
                Environment
                        .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                IMAGE_DIRECTORY_NAME
        )

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {


                return null
            }
        }

        // Create a media file name
        val timeStamp = SimpleDateFormat(
                "yyyyMMdd_HHmmss",
                Locale.ENGLISH
        ).format(Date())
        val mediaFile: File
        if (type == MediaStore.Files.FileColumns.MEDIA_TYPE_IMAGE) {
            mediaFile = File(
                    mediaStorageDir.path + File.separator
                            + "IMG_" + timeStamp + ".jpg"

            )
            var imgPathUpload = mediaStorageDir.path + File.separator + "IMG_" + timeStamp + ".jpg"
            // imgNameUpload = timeStamp + ".jpg"
            if (flag == 101) {
                otherimage1 = "IMG_$timeStamp.jpg"
            }

        } else {
            return null
        }

        return mediaFile
    }

    fun compreesedimage() {
       val  options:BitmapFactory.Options  =  BitmapFactory.Options()
        var bitmap = BitmapFactory.decodeFile(fileUri!!.path,options)

        if (bitmap != null) {
            val actualWidth = options.outWidth
            val actualHeight = options.outHeight
            var mutableBitmap = Bitmap.createScaledBitmap(bitmap, actualWidth/2, actualHeight/2, false);


            var fOut: OutputStream? = null;
            var file: File = File(fileUri!!.path)
            try {
                fOut = FileOutputStream(file);
            } catch (e: FileNotFoundException) {
                e.printStackTrace();
            }
            mutableBitmap.compress(Bitmap.CompressFormat.JPEG, 50, fOut);
            try {
                fOut!!.flush();
                fOut!!.close();
            } catch (e: IOException) {
                e.printStackTrace();
            }

        }
    }
}