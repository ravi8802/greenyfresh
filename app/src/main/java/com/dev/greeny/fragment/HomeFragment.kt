package com.dev.greeny.fragment


import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.viewpager.widget.ViewPager
import com.bumptech.glide.Glide
import com.dev.greeny.R
import com.dev.greeny.activity.ClothsActivity
import com.dev.greeny.adapter.ProductImageView
import com.dev.greeny.baen.CommonModel
import com.dev.greeny.baen.CommonResponse
import com.dev.greeny.service.request.Api
import com.dev.greeny.service.request.ApiClientConnection
import com.dev.greeny.utility.Utils
import com.dev.greeny.utility.Validate
import kotlinx.android.synthetic.main.fragmenthome.*
import kotlinx.android.synthetic.main.fragmenthome.view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*


class HomeFragment : Fragment(), View.OnClickListener {
    private var prdct_imgadapter: ProductImageView? = null
    var currentPage = 0
    var timer: Timer? = null
    val DELAY_MS: Long = 500 //delay in milliseconds before task is to be executed
    val PERIOD_MS: Long = 3000 // time in milliseconds between successive task executions.


    internal lateinit var progressDialog: ProgressDialog
    var apiInterface: Api? = null
    var validate: Validate? = null
    var viewPager:ViewPager?=null
    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {

        val root = inflater.inflate(R.layout.fragmenthome, container, false)

        apiInterface = ApiClientConnection.Companion.instance.createApiInterface()
        validate = Validate(activity!!)
        initClickListener(root)
        viewPager = root.findViewById(R.id.adv_pager)


        callCategoryApi()
        callAdvertiseApi()
        return root
    }

    private fun fillCategoryData(list: List<CommonResponse>?) {

        var imagelist = list!!.get(0).image
        if (!imagelist.isNullOrEmpty()) {
            context?.let {
                Glide.with(it)
                        .load(imagelist) // image url
                        .placeholder(R.drawable.greenyfreashappicon) // any placeholder to load at start
                        .error(R.drawable.greenyfreashappicon)  // any image in case of error
                        .override(200, 200) // resizing
                        .centerCrop()
                        .into(iv_vegitable)
            }
        }



         var masaleimage = list!!.get(1).image
        if (!masaleimage.isNullOrEmpty()) {
            context?.let {
                Glide.with(it)
                        .load(masaleimage) // image url
                        .placeholder(R.drawable.greenyfreashappicon) // any placeholder to load at start
                        .error(R.drawable.greenyfreashappicon)  // any image in case of error
                        .override(200, 200) // resizing
                        .centerCrop()
                        .into(iv_masale)
            }
        }

         var fruitimage = list!!.get(2).image
        if (!fruitimage.isNullOrEmpty()) {
            context?.let {
                Glide.with(it)
                        .load(fruitimage) // image url
                        .placeholder(R.drawable.greenyfreashappicon) // any placeholder to load at start
                        .error(R.drawable.greenyfreashappicon)  // any image in case of error
                        .override(400, 400) // resizing
                        .centerCrop()
                        .into(iv_fruit)
            }
        }

        tv_vegitable.setText(list!!.get(0).descriptionsData.get(0).title)
        tv_masale.setText(list!!.get(1).descriptionsData.get(0).title)
        tvfruit.setText(list!!.get(2).descriptionsData.get(0).title)



    }


    private fun fillData(list: List<CommonResponse>?) {


        prdct_imgadapter = ProductImageView(context, list)
        viewPager?.setAdapter(prdct_imgadapter)


        /*After setting the adapter use the timer */
        val handler = Handler()
        val Update = Runnable {
            if (currentPage == list?.size) {
                currentPage = 0
            }
            viewPager?.setCurrentItem(currentPage++, true)
        }

        timer = Timer() // This will create a new Thread

        timer!!.schedule(object : TimerTask() {
            // task to be scheduled
            override fun run() {
                handler.post(Update)
            }
        }, DELAY_MS, PERIOD_MS)


//        rvAdv?.layoutManager=LinearLayoutManager(activity,LinearLayoutManager.HORIZONTAL,false)
//
//        rvAdv?.adapter=AdapterAdv(activity!!,list)
//
//        val linearSnapHelper: LinearSnapHelper = SnapHelperOneByOne()
//        linearSnapHelper.attachToRecyclerView(rvAdv)
//        rvAdv?.addItemDecoration(CirclePagerIndicatorDecoration())


    }




    private fun initClickListener(root: View) {
        root.linear_cloth.setOnClickListener(this)
        root.linear_food.setOnClickListener(this)
        root.linear_medicine.setOnClickListener(this)
        root.linear_grocery.setOnClickListener(this)
    }

    override fun onClick(p0: View?) {
        when (p0) {
            linear_cloth -> {
                validate!!.SaveSharepreferenceInt(Utils.Categoryid,1)
                val intent = Intent(activity!!, ClothsActivity::class.java)
                intent.flags=(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
                startActivity(intent)
            }

            linear_grocery ->{
                validate!!.SaveSharepreferenceInt(Utils.Categoryid,3)
                val intent = Intent(activity!!, ClothsActivity::class.java)
                intent.flags=(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
                startActivity(intent)
            }
            linear_food -> {
                validate!!.SaveSharepreferenceInt(Utils.Categoryid,2)
                val intent = Intent(activity!!, ClothsActivity::class.java)
                intent.flags=(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
                startActivity(intent)
            }

            linear_medicine ->{
//                validate!!.SaveSharepreferenceInt(Utils.Categoryid,2)
//                val intent = Intent(activity!!, ClothsActivity::class.java)
//                intent.flags=(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
//                startActivity(intent)
            }

        }
    }

    fun callCategoryApi() {
//        progressDialog = ProgressDialog.show(
//                activity, resources.getString(R.string.app_name),
//                getString(R.string.datadownloading)
 //       )
        //progressDialog.setIcon(getDrawable(Context,R.drawable.greenyfreshlogo))

        val call = apiInterface?.categoriesfind()
        call?.enqueue(object : Callback<CommonModel> {
            override fun onFailure(call: Call<CommonModel>, t: Throwable) {
                t.printStackTrace()
                //progressDialog.dismiss()
            }

            override fun onResponse(
                    call: Call<CommonModel>, response: Response<CommonModel>
            ) {
                when (response.code()) {

                    200 -> {
                        if (response!!.body()?.status!!.equals("true", ignoreCase = true)) {
                            //progressDialog.dismiss()
                            val list = response!!.body()?.categoryData

                            fillCategoryData(list)
                            //snackBar(context, response!!.body().message)

                        } else {
                            //progressDialog.dismiss()
                            //snackBar(context, response!!.body().message)
                        }
                    }
                    500 -> {
                        //progressDialog.dismiss()
                    }
                }

            }

        })


    }


    //Api call for Advertise
    fun callAdvertiseApi() {
        progressDialog = ProgressDialog.show(
                activity, resources.getString(R.string.app_name),
                getString(R.string.datadownloading)
        )
        //progressDialog.setIcon(getDrawable(Context,R.drawable.greenyfreshlogo))

        val call = apiInterface?.advertise()
        call?.enqueue(object : Callback<CommonModel> {
            override fun onFailure(call: Call<CommonModel>, t: Throwable) {
                t.printStackTrace()
                progressDialog.dismiss()
            }

            override fun onResponse(
                    call: Call<CommonModel>, response: Response<CommonModel>
            ) {
                when (response.code()) {

                    200 -> {
                        if (response!!.body()?.status!!.equals("true", ignoreCase = true)) {
                            progressDialog.dismiss()
                            val list = response!!.body()?.advertiseData

                            fillData(list)
                            //snackBar(context, response!!.body().message)

                        } else {
                            progressDialog.dismiss()
                            //snackBar(context, response!!.body().message)
                        }
                    }
                    500 -> {
                        progressDialog.dismiss()
                    }
                }

            }

        })


    }
}