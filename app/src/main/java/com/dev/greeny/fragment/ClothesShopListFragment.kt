package com.dev.greeny.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.dev.greeny.R

/**
 * A simple [Fragment] subclass.
 */
class ClothesShopListFragment : Fragment() {
    private lateinit var layoutView: View

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        layoutView=  inflater.inflate(R.layout.fragment_clothes_shop_list, container, false)

        return layoutView
    }

}
