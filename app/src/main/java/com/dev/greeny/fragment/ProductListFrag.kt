package com.dev.greeny.fragment


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.dev.greeny.R
import com.dev.greeny.activity.MainActivity

/**
 * A simple [Fragment] subclass.
 */
class ProductListFrag : Fragment() {

    private lateinit var layoutView: View

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        layoutView =  inflater.inflate(R.layout.fragment_product_list, container, false)
        setUpRecyclerView()
        return layoutView
    }

    private fun setUpRecyclerView() {
//        layoutView.rvProductList.layoutManager = LinearLayoutManager(context)
//        layoutView.rvProductList.adapter = ProductListingAdapter(context)
    }

    override fun onResume() {
        super.onResume()
        (activity as MainActivity).setMainToolbar(getString(R.string.products),View.VISIBLE)
    }

}
