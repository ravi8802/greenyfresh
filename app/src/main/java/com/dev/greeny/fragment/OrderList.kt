package com.dev.greeny.fragment


import android.app.ProgressDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.dev.greeny.R
import com.dev.greeny.service.request.Api
import com.dev.greeny.service.request.ApiClientConnection
import com.dev.greeny.utility.Validate

class OrderList : Fragment() {
    internal lateinit var progressDialog: ProgressDialog
    var apiInterface: Api? = null
    var validate: Validate? = null
    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {

        val root = inflater.inflate(R.layout.fragmentorderlist, container, false)
        apiInterface = ApiClientConnection.Companion.instance.createApiInterface()
        validate = Validate(activity!!)
        // shoplistitem()
        return root
    }

    /* fun FillData(list: List<ProductEntity>?) {
         recycler_orderlist.layoutManager = LinearLayoutManager(activity!!)

         recycler_orderlist.adapter = shopProductdetailAdapter(activity!!, list)

     }

     fun shoplistitem() {
         progressDialog = ProgressDialog.show(
             activity!!, resources.getString(R.string.app_name),
             getString(R.string.datadownloading)
         )
 //        progressDialog.setIcon(getDrawable(R.drawable.appicon))

         val call = apiInterface?.shopdetail(
             validate!!.RetriveSharepreferenceString(Utils.Mobileno)!!,
             validate!!.RetriveSharepreferenceString(Utils.Password)!!            ,
             validate!!.RetriveSharepreferenceInt(Utils.userid).toString()
         )
         call?.enqueue(object : Callback<RegistrationResponse> {
             override fun onFailure(call: Call<RegistrationResponse>, t: Throwable) {
                 t.printStackTrace()
                 progressDialog.dismiss()
             }

             override fun onResponse(
                 call: Call<RegistrationResponse>, response: Response<RegistrationResponse>
             ) {
                 when (response.code()) {
                     200 -> {
                         if (!response!!.body()?.productdetail.isNullOrEmpty()) {
                             progressDialog.dismiss()
                             val list = response!!.body()?.productdetail
                             FillData(list)

                         } else {
                             progressDialog.dismiss()
                             Toast.makeText(
                                 activity!!,
                                 getString(R.string.invaliduser),
                                 Toast.LENGTH_LONG
                             ).show()

                         }
                     }
                 }

             }

         })


     }*/

}