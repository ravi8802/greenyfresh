package com.dev.greeny.fragment


import android.app.Activity
import android.app.ProgressDialog
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.os.StrictMode
import android.provider.MediaStore
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import androidx.fragment.app.Fragment
import com.dev.greeny.R
import com.dev.greeny.entity.CategoryListEntity
import com.dev.greeny.service.request.Api
import com.dev.greeny.service.request.ApiClientConnection
import com.dev.greeny.service.response.RegistrationResponse
import com.dev.greeny.utility.*
import com.tsongkha.spinnerdatepicker.DatePicker
import kotlinx.android.synthetic.main.fragmentaddproduct.*
import kotlinx.android.synthetic.main.fragmentaddproduct.view.*
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.*
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.HashMap

class AddProduct : Fragment(), View.OnClickListener, com.tsongkha.spinnerdatepicker.DatePickerDialog.OnDateSetListener {
    internal lateinit var progressDialog: ProgressDialog
    var apiInterface: Api? = null
    var validate: Validate? = null
    var categoryid = 0
    var categoryList: List<CategoryListEntity>? = null

    var otherimage1 = ""
    var bitmap: Bitmap? = null
    var IMAGE_DIRECTORY_NAME = "Kohinoor"
    private var fileUri: Uri? = null
    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {

        val root = inflater.inflate(R.layout.fragmentaddproduct, container, false)
        apiInterface = ApiClientConnection.Companion.instance.createApiInterface()
        validate = Validate(activity!!)
        if (categoryList.isNullOrEmpty()) {
            categorylist(root)
        }
        initClickListener(root)
        return root
    }


    fun categorylist(root: View) {
        progressDialog = ProgressDialog.show(
                activity!!, resources.getString(R.string.app_name),
                getString(R.string.datadownloading)
        )
        //        progressDialog.setIcon(getDrawable(R.drawable.appicon))

        val call = apiInterface?.CategoryList(validate!!.RetriveSharepreferenceString(Utils.Token)!!)
        call?.enqueue(object : Callback<RegistrationResponse> {
            override fun onFailure(call: Call<RegistrationResponse>, t: Throwable) {
                t.printStackTrace()
                progressDialog.dismiss()
            }

            override fun onResponse(
                    call: Call<RegistrationResponse>, response: Response<RegistrationResponse>
            ) {
                when (response.code()) {
                    200 -> {
                        if (!response!!.body()?.categoryList.isNullOrEmpty()) {
                            progressDialog.dismiss()
                            categoryList = response!!.body()?.categoryList
                            setcategorySpinner(activity!!, root.spincategory, categoryList)

                            snackBar(activity!!, response!!.body().message)

                        } else {
                            progressDialog.dismiss()
                            snackBar(activity!!, response!!.body().message)
                        }
                    }
                    500 -> {
                        progressDialog.dismiss()
                        snackBar(activity!!, getString(R.string.somethingerror))
                    }
                }

            }

        })


    }

    fun preData(): HashMap<String, RequestBody> {
        var userData=HashMap<String, RequestBody>()
        userData.put("token", RequestBody.create(MediaType.parse("text/plain"), validate!!.RetriveSharepreferenceString(Utils.Token)));
        userData.put("product_name", RequestBody.create(MediaType.parse("text/plain"), etProductname.text.toString()));
        userData.put("product_price", RequestBody.create(MediaType.parse("text/plain"), etprice.text.toString()));
        userData.put("product_description", RequestBody.create(MediaType.parse("text/plain"), etdescription.text.toString()));
        userData.put("product_size", RequestBody.create(MediaType.parse("text/plain"), etsize.text.toString()));
        userData.put("product_color", RequestBody.create(MediaType.parse("text/plain"), etcolor.text.toString()));
        userData.put("product_quantity", RequestBody.create(MediaType.parse("text/plain"), etqty.text.toString()));
        userData.put("category_id", RequestBody.create(MediaType.parse("text/plain"), validate!!.RetriveSharepreferenceInt(Utils.Categoryid).toString()));


        return userData;


    }

    fun addproduct() {
        progressDialog = ProgressDialog.show(
                activity!!, resources.getString(R.string.app_name),
                getString(R.string.datauploading)
        )
        //        progressDialog.setIcon(getDrawable(R.drawable.appicon))
        val mediaFile: File
        val mediaStorageDir = File(
                Environment
                        .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                IMAGE_DIRECTORY_NAME
        )
        mediaFile = File(
                mediaStorageDir.path + File.separator
                        + otherimage1
        )
        val datamap=preData()
        val file1 = RequestBody.create(MediaType.parse("multipart/form-data"), mediaFile)
        val imagePart = MultipartBody.Part.createFormData("image[]", mediaFile.name, file1)
        val call = apiInterface?.addProducts(datamap,imagePart)
        call?.enqueue(object : Callback<RegistrationResponse> {
            override fun onFailure(call: Call<RegistrationResponse>, t: Throwable) {
                t.printStackTrace()
                progressDialog.dismiss()
            }

            override fun onResponse(
                    call: Call<RegistrationResponse>, response: Response<RegistrationResponse>
            ) {
                when (response.code()) {
                    200 -> {
                        if (response!!.body()?.status!!.equals("SUCCESS", ignoreCase = true)) {
                            progressDialog.dismiss()
                            replaceFragments(
                                    fragment = ProductList(),
                                    allowStateLoss = true,
                                    containerViewId = R.id.mainContent
                            )
                            snackBar(activity!!, response!!.body().message)

                        } else {
                            progressDialog.dismiss()
                            snackBar(activity!!, response!!.body().message)
                        }
                    }
                    404 -> {
                        snackBar(activity!!, response!!.errorBody().toString())
                    }
                    500 -> {
                        progressDialog.dismiss()
                        snackBar(activity!!, getString(R.string.somethingerror))
                    }
                }

            }

        })


    }

    private fun initClickListener(root: View) {
        root.btnAdd.setOnClickListener(this)
        root.imgcamera.setOnClickListener(this)

        root.tvcategory.setOnClickListener(this)
        root.spincategory.onItemSelectedListener = categorySelectListener
    }

    override fun onClick(p0: View?) {
        when (p0) {
            btnAdd -> {
                if (validation())
                    addproduct()

            }

            imgcamera -> captureimage(101)
            tvcategory -> spincategory.performClick()

        }
    }

    private fun validation(): Boolean {
        when {
            etProductname.text.toString().isEmpty() -> {
                snackBar(activity!!, "Please enter Product name")
                return false
            }

            tvcategory.text.toString().isEmpty() -> {
                snackBar(activity!!, "Please enter Category")
                return false
            }
            etdescription.text.toString().isEmpty() -> {
                snackBar(activity!!, "Please enter Description")
                return false
            }
            etprice.text.toString().isEmpty() -> {
                snackBar(activity!!, "Please enter Price")
                return false
            }
            etcolor.text.toString().isEmpty() -> {
                snackBar(activity!!, "Please enter Color")
                return false
            }
            etsize.text.toString().isEmpty() -> {
                snackBar(activity!!, "Please enter Size")
                return false
            }
            etqty.text.toString().isEmpty() -> {
                snackBar(activity!!, "Please enter Quantity")
                return false
            }

            else -> return true
        }
    }

    private val categorySelectListener = object : AdapterView.OnItemSelectedListener {
        override fun onNothingSelected(p0: AdapterView<*>?) {
        }

        override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
            if (p2 == 0)
                return
            categoryid = categoryList!![p2 - 1].id
            tvcategory.text = p0?.getItemAtPosition(p2).toString()

        }
    }

    override fun onDateSet(view: DatePicker?, year: Int, monthOfYear: Int, dayOfMonth: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    fun captureimage(code: Int) {
        if (Build.VERSION.SDK_INT >= 24) {
            try {
                val m = StrictMode::class.java.getMethod("disableDeathOnFileUriExposure")
                m.invoke(null)
            } catch (e: Exception) {
                e.printStackTrace()
            }

        }
        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        fileUri = getOutputMediaFileUri(MediaStore.Files.FileColumns.MEDIA_TYPE_IMAGE, code)
        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri)
        startActivityForResult(intent, code)

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        try {
            compreesedimage()
            bitmap = BitmapFactory.decodeFile(fileUri!!.path)
            if (requestCode == 101 && resultCode == Activity.RESULT_OK) {
                image1.setImageBitmap(bitmap)
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun getOutputMediaFileUri(type: Int, flag: Int): Uri {
        return Uri.fromFile(getOutputMediaFile(type, flag))
    }

    private fun getOutputMediaFile(type: Int, flag: Int): File? {

        // External sdcard location
        val mediaStorageDir = File(
                Environment
                        .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                IMAGE_DIRECTORY_NAME
        )

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {


                return null
            }
        }

        // Create a media file name
        val timeStamp = SimpleDateFormat(
                "yyyyMMdd_HHmmss",
                Locale.ENGLISH
        ).format(Date())
        val mediaFile: File
        if (type == MediaStore.Files.FileColumns.MEDIA_TYPE_IMAGE) {
            mediaFile = File(
                    mediaStorageDir.path + File.separator
                            + "IMG_" + timeStamp + ".jpg"

            )
            var imgPathUpload = mediaStorageDir.path + File.separator + "IMG_" + timeStamp + ".jpg"
            // imgNameUpload = timeStamp + ".jpg"
            if (flag == 101) {
                otherimage1 = "IMG_$timeStamp.jpg"
            }

        } else {
            return null
        }

        return mediaFile
    }

    fun compreesedimage() {
        var bitmap = BitmapFactory.decodeFile(fileUri!!.path);

        if (bitmap != null) {
            var mutableBitmap = Bitmap.createScaledBitmap(bitmap, 500, 500, false);


            var fOut: OutputStream? = null;
            var file: File = File(fileUri!!.path)
            try {
                fOut = FileOutputStream(file);
            } catch (e: FileNotFoundException) {
                e.printStackTrace();
            }
            mutableBitmap.compress(Bitmap.CompressFormat.JPEG, 100, fOut);
            try {
                fOut!!.flush();
                fOut!!.close();
            } catch (e: IOException) {
                e.printStackTrace();
            }

        }
    }
}