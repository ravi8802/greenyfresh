package com.dev.greeny.fragment

import android.app.ProgressDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.dev.greeny.R
import com.dev.greeny.service.request.Api
import com.dev.greeny.service.request.ApiClientConnection
import com.dev.greeny.utility.Validate

class OrderHistoryFragment : Fragment() {
    internal lateinit var progressDialog: ProgressDialog
    var apiInterface: Api? = null
    var validate: Validate? = null


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val root = inflater.inflate(R.layout.fragment_product_list, container, false)
        apiInterface = ApiClientConnection.Companion.instance.createApiInterface()
        validate = Validate(activity!!)
        // shoplistitem()
        return root
    }



}