package  com.dev.greeny.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey


@Entity(tableName = "district")
data class mstdistrict(
        @PrimaryKey @ColumnInfo(name = "district_id") val district_id: Int,
        @ColumnInfo(name = "name") val name: String
       // @ColumnInfo(name = "state_id") val state_id: Int

)