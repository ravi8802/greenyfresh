package  com.dev.greeny.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey


@Entity(tableName = "shopList")
data class ShopListEntity(
        @PrimaryKey @ColumnInfo(name = "shopkeeper_id") val shopkeeper_id: Int,
        @ColumnInfo(name = "token") val token: String,
        @ColumnInfo(name = "shop_name") val shop_name: String?,
        @ColumnInfo(name = "category_id") val category_id: Int?,
        @ColumnInfo(name = "shop_category_name") val shop_category_name: String?,
        @ColumnInfo(name = "shop_email") val shop_email: String?,
        @ColumnInfo(name = "shop_phone") val shop_phone: String?,
        @ColumnInfo(name = "shop_address") val shop_address: String?,
        @ColumnInfo(name = "shop_image") val shop_image: String?,
        @ColumnInfo(name = "shop_gstNumber") val shop_gstNumber: String?,
        @ColumnInfo(name = "shop_state_name") val shop_state_name: String?,
        @ColumnInfo(name = "shop_district_name") val shop_district_name: String?
)