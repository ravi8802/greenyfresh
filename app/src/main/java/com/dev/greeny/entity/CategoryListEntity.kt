package  com.dev.greeny.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey


@Entity(tableName = "categoryList")
data class CategoryListEntity(
        @PrimaryKey @ColumnInfo(name = "id") val id: Int,
        @ColumnInfo(name = "shopkeeper_id") val shopkeeper_id: Int?,
        @ColumnInfo(name = "category_name") val category_name: String?

)