package  com.dev.greeny.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey


@Entity(tableName = "productList")
data class ProductListEntity(
        @PrimaryKey @ColumnInfo(name = "id") val id: Int,
        @ColumnInfo(name = "category_name") val category_name: String?,
        @ColumnInfo(name = "shopkeeper_name") val shopkeeper_name: String?,
        @ColumnInfo(name = "product_name") val product_name: String?,
        @ColumnInfo(name = "product_description") val product_description: String?,
        @ColumnInfo(name = "product_color") val product_color: String?,
        @ColumnInfo(name = "product_size") val product_size: String?,
        @ColumnInfo(name = "product_quantity") val product_quantity: String?,
        @ColumnInfo(name = "image") val image: Array<String>?,
        @ColumnInfo(name = "product_image") val product_image: String?,
        @ColumnInfo(name = "product_price") val product_price: Int?

)