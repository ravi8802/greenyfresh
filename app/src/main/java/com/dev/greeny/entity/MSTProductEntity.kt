package com.dev.greeny.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey


@Entity(tableName = "category")
data class MSTProductEntity(
        @PrimaryKey @ColumnInfo(name = "id_category") val id_category: Int,
        @ColumnInfo(name = "name_category") val name_category: String

)