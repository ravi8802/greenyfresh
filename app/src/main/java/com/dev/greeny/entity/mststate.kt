package  com.dev.greeny.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey


@Entity(tableName = "state")
data class mststate(
        @PrimaryKey @ColumnInfo(name = "state_id") val state_id: Int,
        @ColumnInfo(name = "state_name") val state_name: String

)