package  com.dev.greeny.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey


@Entity(tableName = "user")
data class UserEntity(
        @PrimaryKey @ColumnInfo(name = "user_id") val user_id: Int,
        @ColumnInfo(name = "name") val name: String,
        @ColumnInfo(name = "phone") val phone: String,
        @ColumnInfo(name = "password") val password: String,
        @ColumnInfo(name = "address") val address: String?,
        @ColumnInfo(name = "category_id") val category_id: Int?,
        @ColumnInfo(name = "token") val token: String?


)