package com.dev.greeny.Firebase.service;//package com.mobu.etapp.Firebase.service;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import static com.dev.greeny.constant.Constants.kDEVICETOKEN;


public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {

    private static final String TAG = MyFirebaseInstanceIDService.class.getSimpleName();

    @Override
    public void onTokenRefresh() {
        super.onTokenRefresh();
        String refreshedToken= FirebaseInstanceId.getInstance().getToken();

        SharedPreferences sharedPreferences = getSharedPreferences("Greeny", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor =  sharedPreferences.edit();
        editor.putString(kDEVICETOKEN,refreshedToken);
        editor.commit();
        Log.v(TAG, "sendRegistrationToServer: " + refreshedToken);

        //saving token to shared preference
       // BaseManager.saveDataIntoPreferences(refreshedToken,kDeviceToken);

    }
//
}
