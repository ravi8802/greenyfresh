package com.dev.greeny.Firebase.service;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.dev.greeny.Firebase.app.Config;
import com.dev.greeny.Firebase.utils.NotificationUtils;
import com.dev.greeny.activity.DashboardActivity;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;


/**
 * Created by ravi on 18/7/19.
 * Copyright to Mobulous Technology Pvt. Ltd.
 */

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "PUSH";

    private NotificationUtils notificationUtils;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        if (remoteMessage == null)
            return;

        try {
            handleMyDataMessage(remoteMessage);
             handleNotification(remoteMessage.getNotification().getBody());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

//    @Override
//    public void onNewToken(@NonNull String s) {
//        super.onNewToken(s);
//
//        String refreshedToken = s;
//        Log.v(TAG, "sendRegistrationToServer: " + refreshedToken);
//
//        SharedPreferenceWriter.getInstance(this).writeStringValue(DEVICE_TOKEN, refreshedToken);
//
//    }

    private void handleMyDataMessage(RemoteMessage remoteMessage) throws Exception {

        Log.d("daas", remoteMessage + "");

        //Map<String, String> data = remoteMessage.getData();

        //JSONObject jsonObject = new JSONObject(data);
        String title = remoteMessage.getData().get("title");
        String body = remoteMessage.getData().get("body");

        //String type = jsonObject.getString("type");
        // notiType(type);


        if (NotificationUtils.isAppIsInBackground(getApplicationContext())) {
            // app is in foreground, broadcast the push message
            Intent pushNotification = new Intent(Config.PUSH_NOTIFICATION);
           /* pushNotification.putExtra(kBody, body);
            pushNotification.putExtra(kTitle, "");*/

            LocalBroadcastManager.getInstance(this).sendBroadcast(pushNotification);

            Intent intent = new Intent(getApplicationContext(), DashboardActivity.class);
//            String userName = jsonObject.getString("userName");
//            String sender_id = jsonObject.getString("sender_id");
//            String userImage = jsonObject.getString("userImage");
//            String position = jsonObject.getString("position");
//            String room_id = jsonObject.getString("room_id");
//
//
//            intent.putExtra("userName", userName);
//            intent.putExtra("sender_id", sender_id);
//            intent.putExtra("userImage", userImage);
//            intent.putExtra("position", position);
//            intent.putExtra("room_id", room_id);
//            intent.putExtra("kType", type);

            showNotificationMessage(getApplicationContext(), title, body, "", intent);
            // play notification sound
            NotificationUtils notificationUtils = new NotificationUtils(getApplicationContext());
            notificationUtils.playNotificationSound();


        } else {
//            if (title.equalsIgnoreCase("New Message Received")) {
                Intent pushNotification = new Intent(Config.PUSH_NOTIFICATION);
                LocalBroadcastManager.getInstance(this).sendBroadcast(pushNotification);

                Intent intent = new Intent(getApplicationContext(), DashboardActivity.class);

            showNotificationMessage(getApplicationContext(), title, body, "", intent);
            // play notification sound
            NotificationUtils notificationUtils = new NotificationUtils(getApplicationContext());
            notificationUtils.playNotificationSound();




                // play notification sound

           // }
//                Intent resultIntent = HomeMapActivity.getIntent(getApplicationContext(),"");
//                showNotificationMessage(getApplicationContext(), title, body, NotificationUtils.generateTimeStamp(), resultIntent);
        }
            /*else if (body.equals(kCancelBookingFoodie)) {
                Intent resultIntent = MyReservationScreen.getIntent(getApplicationContext());
                showNotificationMessage(getApplicationContext(), title, body, Utils.generateTimeStamp(), resultIntent);
            } else if (body.equals(kCancelBookingHost)) {
                Intent resultIntent = MyHistoryActivity.getIntent(getApplicationContext());
                showNotificationMessage(getApplicationContext(), title, body, Utils.generateTimeStamp(), resultIntent);
            } else if (body.equals(kCompletebooking)) {
                Intent resultIntent = MyHistoryActivity.getIntent(getApplicationContext());
                showNotificationMessage(getApplicationContext(), title, body, Utils.generateTimeStamp(), resultIntent);
            }else  if(body.equals(kPaymentRecieve)){
                Intent resultIntent = HomeHostActivity.getIntent(getApplicationContext());
                showNotificationMessage(getApplicationContext(), title, body, Utils.generateTimeStamp(), resultIntent);
            }else if(body.equals("by_admin")){
                Intent resultIntent= HostNotificationActivity.getIntent(getApplicationContext());
                showNotificationMessage(getApplicationContext(), title, body, Utils.generateTimeStamp(), resultIntent);

            }

            else {
                Intent resultIntent = MyHistoryActivity.getIntent(getApplicationContext());
                showNotificationMessage(getApplicationContext(), title, body, Utils.generateTimeStamp(), resultIntent);
            }
*/
    }


    private void notiType(String type) {

        if (type.equalsIgnoreCase("offerAvailableTest")) {
            Bundle bundle = new Bundle();
            bundle.putString("offerAvailable", "offerAvailable");
            Intent intent = new Intent("Tap Successful");
            intent.putExtras(bundle);
            sendBroadcast(intent);

        } else if (type.equalsIgnoreCase("workDoneByDP")) {
            Bundle bundle = new Bundle();
            bundle.putString("workDoneByDP", "workDoneByDP");
            Intent intent = new Intent("workDoneByDP");
            intent.putExtras(bundle);
            sendBroadcast(intent);

        } else if (type.equalsIgnoreCase("workDoneByPW")) {
            Bundle bundle = new Bundle();
            bundle.putString("workDoneByPW", "workDoneByPW");
            Intent intent = new Intent("Tap Successful");
            intent.putExtras(bundle);
            LocalBroadcastManager.getInstance(MyFirebaseMessagingService.this).sendBroadcast(intent);
        } else if (type.equalsIgnoreCase("offerAvailableProfessional")) {
            Bundle bundle = new Bundle();
            bundle.putString("offerAvailableProfessional", "offerAvailableProfessional");
            Intent intent = new Intent("offerAvailableProfessional");
            intent.putExtras(bundle);
            sendBroadcast(intent);
        } else if (type.equalsIgnoreCase("invoiceAvailable")) {
            Bundle bundle = new Bundle();
            bundle.putString("invoiceAvailable", "invoiceAvailable");
            Intent intent = new Intent("invoiceAvailable");
            intent.putExtras(bundle);
            sendBroadcast(intent);
        } else if (type.equalsIgnoreCase("goStatus")) {
            Bundle bundle = new Bundle();
            bundle.putString("goStatus", "goStatus");
            Intent intent = new Intent("Tap Successful");
            intent.putExtras(bundle);
            sendBroadcast(intent);
        } else if (type.equalsIgnoreCase("arrivedStatus")) {
            Bundle bundle = new Bundle();
            bundle.putString("arrivedStatus", "arrivedStatus");
            Intent intent = new Intent("Tap Successful");
            intent.putExtras(bundle);
            sendBroadcast(intent);
        } else if (type.equalsIgnoreCase("requestApproved")) {
            Bundle bundle = new Bundle();
            bundle.putString("requestApproved", "requestApproved");
            Intent intent = new Intent("Tap Successful");
            intent.putExtras(bundle);
            LocalBroadcastManager.getInstance(MyFirebaseMessagingService.this).sendBroadcast(intent);
        } else if (type.equalsIgnoreCase("offerAcceptOfDelivery")) {
            Bundle bundle = new Bundle();
            bundle.putString("offerAcceptOfDelivery", "offerAcceptOfDelivery");
            Intent intent = new Intent("Tap Successful");
            intent.putExtras(bundle);
            sendBroadcast(intent);
        } else if (type.equalsIgnoreCase("offerAcceptOfProfessional")) {
            Bundle bundle = new Bundle();
            bundle.putString("offerAcceptOfProfessional", "offerAcceptOfProfessional");
            Intent intent = new Intent("Tap Successful");
            intent.putExtras(bundle);
            LocalBroadcastManager.getInstance(MyFirebaseMessagingService.this).sendBroadcast(intent);
        } else if (type.equalsIgnoreCase("offerAvailable")) {

            Bundle bundle = new Bundle();
            bundle.putString("offerAvailable", "offerAvailable");
            Intent intent = new Intent("Tap Successful");
            intent.putExtras(bundle);
            sendBroadcast(intent);

        } else if (type.equalsIgnoreCase("offerDelete")) {

            Toast.makeText(this, "offerDelete", Toast.LENGTH_SHORT).show();

        } else if (type.equalsIgnoreCase("offerReject")) {
            Bundle bundle = new Bundle();
            bundle.putString("offerAcceptOfDelivery", "offerReject");
            Intent intent = new Intent("Tap Successful");
            intent.putExtras(bundle);
            sendBroadcast(intent);
        }


    }

    private Integer convertIntoInteger(String value) {
        Integer intvalue = 0;
        try {
            intvalue = Integer.parseInt(value);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return intvalue;
    }


    /**
     * Showing notification with text only
     */
    private void showNotificationMessage(Context context, String title, String message, String timeStamp, Intent intent) {
        notificationUtils = new NotificationUtils(context);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        notificationUtils.showNotificationMessage(title, message, timeStamp, intent);
    }


    /**
     * Showing notification with text and image
     */
    private void showNotificationMessageWithBigImage(Context context, String title, String message, String timeStamp, Intent intent, String imageUrl) {
        notificationUtils = new NotificationUtils(context);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        notificationUtils.showNotificationMessage(title, message, timeStamp, intent, imageUrl);
    }


    private void handleNotification(String message) {
        if (!NotificationUtils.isAppIsInBackground(getApplicationContext())) {
            // app is in foreground, broadcast the push message
            Intent pushNotification = new Intent(Config.PUSH_NOTIFICATION);
            pushNotification.putExtra("message", message);
            LocalBroadcastManager.getInstance(this).sendBroadcast(pushNotification);

            // play notification sound
            NotificationUtils notificationUtils = new NotificationUtils(getApplicationContext());
            notificationUtils.playNotificationSound();
        } else {
            //If the app is in background, firebase itself handles the notification
            // showNotificationMessageWithBigImage(getApplicationContext(), "", message, "", new Intent(getApplicationContext(), SuccessStory.class), "");
            Intent pushNotification = new Intent(Config.PUSH_NOTIFICATION);
            pushNotification.putExtra("message", message);
            LocalBroadcastManager.getInstance(this).sendBroadcast(pushNotification);

            // play notification sound
            NotificationUtils notificationUtils = new NotificationUtils(getApplicationContext());
            notificationUtils.playNotificationSound();
        }
    }

}
