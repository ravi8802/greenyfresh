package com.dev.greeny.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.dev.greeny.R
import com.dev.greeny.entity.ProductListEntity
import kotlinx.android.synthetic.main.layout_product_list_item.view.*

class ProductListingAdapter(private var context: Context,val productlist: List<ProductListEntity>?): RecyclerView.Adapter<ProductListingAdapter.ProductListingViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductListingViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.layout_product_list_item,parent,false)
        return ProductListingViewHolder(view)
    }

    override fun getItemCount(): Int {
        return productlist!!.size
    }

    override fun onBindViewHolder(holder: ProductListingViewHolder, position: Int) {
        holder.tvname.text=productlist!!.get(position).product_name
        holder.tvdescription.text=productlist!!.get(position).product_description
        holder.tvprice.text=productlist!!.get(position).product_price.toString()
        var imagelist=productlist!!.get(position).image
        if(!imagelist.isNullOrEmpty()) {
            Glide.with(context)
                    .load(imagelist[imagelist.size-1]) // image url
                    .placeholder(R.drawable.kohinoor512) // any placeholder to load at start
                    .error(R.drawable.kohinoor512)  // any image in case of error
                    .override(200, 200) // resizing
                    .centerCrop()
                    .into(holder.imageView)
        }

    }

    inner class ProductListingViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val imageView = view.imageView
        val tvname = view.tvname
        val tvdescription = view.tvdescription
        val tvprice = view.tvprice

    }
}