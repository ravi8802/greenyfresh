package com.dev.greeny.adapter

import android.app.ProgressDialog
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.dev.greeny.R
import com.dev.greeny.activity.ClothsListActivity
import com.dev.greeny.entity.ProductListEntity
import com.dev.greeny.service.request.Api
import com.dev.greeny.service.request.ApiClientConnection
import com.dev.greeny.service.response.RegistrationResponse
import com.dev.greeny.utility.Utils
import com.dev.greeny.utility.Validate
import kotlinx.android.synthetic.main.cloth_list_item.view.*
import kotlinx.android.synthetic.main.layout_product_list_item.view.imageView
import kotlinx.android.synthetic.main.layout_product_list_item.view.tvdescription
import kotlinx.android.synthetic.main.layout_product_list_item.view.tvname
import kotlinx.android.synthetic.main.layout_product_list_item.view.tvprice
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ClothListingAdapter(private var context: Context, val productList: List<ProductListEntity>?): RecyclerView.Adapter<ClothListingAdapter.ProductListingViewHolder>() {
    private var onCallBack: CartItemsInterface?=null
    var cart_qty = 0
    internal lateinit var progressDialog: ProgressDialog
    var apiInterface: Api? = null
    var validate: Validate? = null

    override fun getItemCount(): Int {
        return productList!!.size
    }

    fun onCartItem(onCallBack: CartItemsInterface){
        this.onCallBack=onCallBack
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductListingViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.cloth_list_item,parent,false)
        return ProductListingViewHolder(view)
    }

    override fun onBindViewHolder(holder: ProductListingViewHolder, position: Int) {
        holder.tvname.text=productList!!.get(position).product_name
        holder.tvdescription.text=productList!!.get(position).product_description
        holder.tvprice.text=productList!!.get(position).product_price.toString()
        var imagelist=productList!!.get(position).image
        if(!imagelist.isNullOrEmpty()) {
            Glide.with(context)
                    .load(imagelist[imagelist.size-1]) // image url
                    .placeholder(R.drawable.kohinoor512) // any placeholder to load at start
                    .error(R.drawable.kohinoor512)  // any image in case of error
                    .override(200, 200) // resizing
                    .centerCrop()
                    .into(holder.imageView)
        }
        holder.linear_add_to_cart.setOnClickListener{
            //TODO : LOGGEDIN CHECK
            //holder.linear_attribues.requestFocus()
            holder.linear_qty.setVisibility(View.VISIBLE)
            holder.text_add_cart.setVisibility(View.GONE)
            holder.linear_add_to_cart.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.bg_bottom_round_coner_orange))
        }

        holder.text_minus.setOnClickListener {
            if (cart_qty > 0) {
                cart_qty--
                holder.text_qty.post(Runnable { holder.text_qty.setText(cart_qty.toString()) })
            }
        }

        holder.text_plus.setOnClickListener {
            cart_qty++
            holder.text_qty.post(Runnable { holder.text_qty.setText(cart_qty.toString()) })
        }

        holder.linear_text_done.setOnClickListener {
            //update qty
            if (holder.text_qty.getText().toString().equals("0", ignoreCase = true)) {
                Toast.makeText(context, "Select quantity", Toast.LENGTH_SHORT).show()
            } else {
                //onCallBack?.onCallBackCartItem(productList.get(position),cart_qty.toString())
                createOrderApi(productList.get(position).id,cart_qty.toString())
                    }
            }

    }

    interface CartItemsInterface{
        fun onCallBackCartItem(cartData: ProductListEntity,quantity:String)
    }

    inner class ProductListingViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val imageView = view.imageView
        val tvname = view.tvname
        val tvdescription = view.tvdescription
        val tvprice = view.tvprice
        val text_plus = view.text_plus
        val text_qty = view.text_qty
        val text_minus = view.text_minus
        val text_add_cart = view.text_add_cart
        val linear_text_done = view.linear_text_done
        val linear_qty = view.linear_qty
        val linear_add_to_cart = view.linear_add_to_cart

    }


    fun createOrderApi(id: Int, quantity: String) {
        validate = Validate(context)
        progressDialog = ProgressDialog.show(
                context, context.resources.getString(R.string.app_name),
                context.getString(R.string.datadownloading)
        )
        //        progressDialog.setIcon(getDrawable(R.drawable.appicon))
        apiInterface =  ApiClientConnection.Companion.instance.createApiInterface()
        val call = apiInterface?.createOrder(
                validate!!.RetriveSharepreferenceString(Utils.Token)!!,id,quantity)
        call?.enqueue(object : Callback<RegistrationResponse> {
            override fun onFailure(call: Call<RegistrationResponse>, t: Throwable) {
                t.printStackTrace()
                progressDialog.dismiss()
            }

            override fun onResponse(
                    call: Call<RegistrationResponse>, response: Response<RegistrationResponse>
            ) {
                when (response.code()) {

                    200 -> {
                        if (response!!.body().status.equals("SUCCESS")) {
                            progressDialog.dismiss()
                            cart_qty = 0


                            validate!!.SaveSharepreferenceInt(Utils.Totalcount,response.body().totalcount!!)
                            (context as ClothsListActivity).updatecount()
                            
                            // FillData(list)
                           // snackBar(context, response!!.body().message)

                        } else {
                            progressDialog.dismiss()
                            //snackBar(context, response!!.body().message)
                        }
                    }
                    500 -> {
                        progressDialog.dismiss()
                    }
                }

            }

        })


    }
}