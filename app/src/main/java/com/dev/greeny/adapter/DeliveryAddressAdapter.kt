package com.dev.greeny.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView

import com.dev.greeny.R
import com.dev.greeny.baen.OrderListResponse

import kotlinx.android.synthetic.main.single_row_deliveryaddress.view.*

class DeliveryAddressAdapter(private var context: Context, val addressList: List<OrderListResponse>?) : RecyclerView.Adapter<DeliveryAddressAdapter.AddressViewHolder>() {
    private var onCallBack: CartItemsInterface? = null
    private var onSelectCallBack: CartItemsInterface? = null

    var cart_qty = 0


    override fun getItemCount(): Int {
        return addressList!!.size
    }


    fun onCartItem(onCallBack: CartItemsInterface) {
        this.onCallBack = onCallBack
    }

    fun onSelectCartItem(onSelectCallBack: CartItemsInterface) {
        this.onSelectCallBack = onSelectCallBack
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AddressViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.single_row_deliveryaddress, parent, false)
        return AddressViewHolder(view)
    }

    override fun onBindViewHolder(holder: AddressViewHolder, position: Int) {
        holder.text_title.text = addressList!!.get(position).address

        holder.text_address.text = addressList!!.get(position).state


        holder.image_edit.setOnClickListener {
            //update qty
            onCallBack?.onCallBackCartItem(addressList.get(position))
        }

        holder.ll_main.setOnClickListener {
            //update qty
            onSelectCallBack?.onSelectCallBackCartItem(addressList.get(position))
        }


    }

    interface CartItemsInterface {
        fun onCallBackCartItem(cartData: OrderListResponse)
        fun onSelectCallBackCartItem(cartSelectData: OrderListResponse)
    }


    inner class AddressViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val image_edit = view.image_edit
        val text_title = view.text_title
        val text_address = view.text_address
        val text_default = view.text_default
        val ll_main = view.ll_main


    }

}