package com.dev.greeny.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.dev.greeny.R
import com.dev.greeny.activity.FoodListActivity
import com.dev.greeny.baen.ProductDataResponce
import com.dev.greeny.entity.ShopListEntity
import com.dev.greeny.utility.Utils
import com.dev.greeny.utility.Validate
import kotlinx.android.synthetic.main.layout_product_list_item.view.*
import kotlinx.android.synthetic.main.layout_product_list_item.view.grid_cardview
import kotlinx.android.synthetic.main.layout_product_list_item.view.imageView
import kotlinx.android.synthetic.main.layout_product_list_item.view.tvdescription
import kotlinx.android.synthetic.main.layout_product_list_item.view.tvname
import kotlinx.android.synthetic.main.layout_product_list_item.view.tvprice
import kotlinx.android.synthetic.main.row_wishlist.view.*
import kotlinx.android.synthetic.main.row_wishlist.view.btnaddcart
import kotlinx.android.synthetic.main.row_wishlist.view.ivLike
import kotlinx.android.synthetic.main.shop_list_item.view.*

class WishListAdapter (private var context: Context, val shopList: List<ProductDataResponce>?) : RecyclerView.Adapter<WishListAdapter.ProductListingViewHolder>() {
    var validate: Validate? = null

    private var onCallBack: AddCardInterface? = null
    private var onLikeCallBack: LikeInterface? = null

    fun onAddCard(onCallBack: AddCardInterface) {
        this.onCallBack = onCallBack
    }
    fun onLikeItem(onLikeCallBack: LikeInterface) {
        this.onLikeCallBack = onLikeCallBack
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductListingViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.row_wishlist, parent, false)
        validate = Validate(context)
        return ProductListingViewHolder(view)
    }

    override fun getItemCount(): Int {
        return shopList!!.size
    }


    override fun onBindViewHolder(holder: ProductListingViewHolder, position: Int) {
        holder.tvname.text = shopList!!.get(position).product!!.descriptionsList!!.get(0).name

        holder.tvprice.text = "Rs." + shopList!!.get(position).product!!.price.toString()
        var imagelist = shopList!!.get(position).product!!.imageList!!.get(0).image
        if (!imagelist.isNullOrEmpty()) {
            Glide.with(context)
                    .load(imagelist) // image url
                    .placeholder(R.drawable.greenyfreashappicon) // any placeholder to load at start
                    .error(R.drawable.greenyfreashappicon)  // any image in case of error
                    .override(200, 200) // resizing
                    .centerCrop()
                    .into(holder.imageView)
        }
//        if (shopList!!.get(position).wish.equals("no")) {
//            Glide.with(context)
//                    .load(R.drawable.heart_green) // image url
//                    .into(holder.ivLike)
//        }else{
//            Glide.with(context)
//                    .load(R.drawable.like) // image url
//                    .into(holder.ivLike)
//        }

        holder.btnaddcart.setOnClickListener {
            onCallBack?.onCallBackAddCardItem(shopList.get(position))
        }
        holder.ivLike.setOnClickListener {
            onLikeCallBack?.onCallLikeItem(shopList.get(position))
        }


    }

    interface AddCardInterface {
        fun onCallBackAddCardItem(propertyData: ProductDataResponce)
    }
    interface LikeInterface {
        fun onCallLikeItem(propertyData: ProductDataResponce)
    }


    inner class ProductListingViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val imageView = view.imageView
        val grid_cardview = view.grid_cardview
        val tvname = view.tvname
        val tvdescription = view.tvdescription
        val tvprice = view.tvprice
        val btnaddcart = view.btnaddcart
        val ivLike = view.ivLike

    }
}