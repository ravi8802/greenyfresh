package com.dev.greeny.adapter

import android.app.ProgressDialog
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.dev.greeny.R
import com.dev.greeny.baen.OrderListResponse
import com.dev.greeny.service.response.RegistrationResponse
import com.dev.greeny.utility.Utils

import kotlinx.android.synthetic.main.row_addcart_layout.view.*

import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class CartAdapter(private var context: Context, val productList: List<OrderListResponse>?) : RecyclerView.Adapter<CartAdapter.CartViewHolder>() {
    private var onCallBack: CartItemsInterface? = null
    private var onCallBackAdd: AddItemsInterface? = null
    private var onCallBackSubtract: SubtractItemsInterface? = null
    var cart_qty = 0


    override fun getItemCount(): Int {
        return productList!!.size
    }

    fun onAddCartItem(onCallBackAdd: AddItemsInterface) {
        this.onCallBackAdd = onCallBackAdd
    }

    fun onSubtractCartItem(onCallBackSubtract: SubtractItemsInterface) {
        this.onCallBackSubtract = onCallBackSubtract
    }

    fun onCartItem(onCallBack: CartItemsInterface) {
        this.onCallBack = onCallBack
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CartViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.row_addcart_layout, parent, false)
        return CartViewHolder(view)
    }

    override fun onBindViewHolder(holder: CartViewHolder, position: Int) {
        holder.text_dishname.text = productList!!.get(position).name
        holder.text_suppliername.text = "Qwt : " + productList!!.get(position).quantity
        holder.text_qty.text = productList!!.get(position).quantity.toString()
        holder.text_price.text =context.getString(R.string.rupee)+" "+ productList!!.get(position).sale_price.toString()
        holder.text_total_price.text =context.getString(R.string.rupee)+" "+ productList!!.get(position).total_price.toString()

//        val a:Int = productList!!.get(position).quantity!!
//        val b:Int = productList!!.get(position).price!!
//        val result:Int = a * b
//        holder.text_total_price.text = result.toString()

        var imagelist = productList!!.get(position).image
        if (!imagelist.isNullOrEmpty()) {
            Glide.with(context)
                    .load(imagelist) // image url
                    .placeholder(R.drawable.greenyfreashappicon) // any placeholder to load at start
                    .error(R.drawable.greenyfreashappicon)  // any image in case of error
                    .override(200, 200) // resizing
                    .centerCrop()
                    .into(holder.imageDish)
        }

//
//
        holder.image_delete.setOnClickListener {
            //update qty
            onCallBack?.onCallBackCartItem(productList.get(position))
        }
        holder.tv_plus.setOnClickListener {
            //update qty
            onCallBackAdd?.onCallBackAddCartItem(productList.get(position))
        }
        holder.tv_minus.setOnClickListener {
            //update qty
            onCallBackSubtract?.onCallBackSubtractCartItem(productList.get(position))
        }


    }

    interface CartItemsInterface {
        fun onCallBackCartItem(cartData: OrderListResponse)
    }

    interface AddItemsInterface {
        fun onCallBackAddCartItem(cartData: OrderListResponse)
    }

    interface SubtractItemsInterface {
        fun onCallBackSubtractCartItem(cartData: OrderListResponse)
    }

    inner class CartViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val imageDish = view.imageDish
        val text_dishname = view.text_dishname
        val image_delete = view.image_delete
        val text_suppliername = view.text_suppliername
        val text_price = view.text_price
        val text_total_price = view.text_total_price
        val text_qty = view.text_qty
        val tv_plus = view.tv_plus
        val tv_minus = view.tv_minus


  //      val tvqty = view.tvqty

//        val text_add_cart = view.text_add_cart
//        val linear_text_done = view.linear_text_done
//        val linear_qty = view.linear_qty
//        val linear_add_to_cart = view.linear_add_to_cart

    }

}



