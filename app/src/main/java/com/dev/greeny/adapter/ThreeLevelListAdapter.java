package com.dev.greeny.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.dev.greeny.R;
import com.dev.greeny.baen.CategoryDataResponse;
import com.dev.greeny.baen.CommonResponse;
import com.dev.greeny.utility.MyInterface;


import java.util.List;

public class ThreeLevelListAdapter extends BaseExpandableListAdapter {
    String[] parentHeaders;
    // List<String[]> secondLevel;
    private Context context;
    MyInterface listener;
    // List<LinkedHashMap<String, String[]>> data;
    private List<CategoryDataResponse> categoryDataResponses;
    private List<CategoryDataResponse> subCategoryDataResponses;
    private List<CategoryDataResponse> subToSubCategoryDataResponses;
    private CommonResponse[] categoryData;


    public ThreeLevelListAdapter(Context context, List<CategoryDataResponse> categoryDataResponses, List<CategoryDataResponse> subCategoryDataResponses, List<CategoryDataResponse> subToSubCategoryDataResponses, MyInterface listener) {
        this.context = context;

        this.listener = listener;
        //this.secondLevel = secondLevel;
        this.categoryDataResponses = categoryDataResponses;
        this.subCategoryDataResponses = subCategoryDataResponses;
        this.subToSubCategoryDataResponses = subToSubCategoryDataResponses;
    }

    public ThreeLevelListAdapter(Context context, CommonResponse[] categoryData, MyInterface listener) {
        this.context = context;
        this.listener = listener;
        this.categoryData = categoryData;

    }

    @Override
    public int getGroupCount() {
        return categoryData != null?categoryData.length:0;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        // no idea why this code is working
        return categoryData[groupPosition].getSubCategoryData().length;

    }

    @Override
    public Object getGroup(int groupPosition) {
        return categoryData[groupPosition];
    }

    @Override
    public Object getChild(int group, int child) {
        return categoryData[group].getSubCategoryData()[child];
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = inflater.inflate(R.layout.row_order_history_frist_layout, null);
        TextView text = (TextView) convertView.findViewById(R.id.text_order_no);
        TextView textloc = (TextView) convertView.findViewById(R.id.text_loc);
        TextView text_price = (TextView) convertView.findViewById(R.id.text_price);
        TextView text_date = (TextView) convertView.findViewById(R.id.text_date);
        TextView text_items = (TextView) convertView.findViewById(R.id.text_items);

        text.setText(categoryData[groupPosition].getOrder_number());
        textloc.setText(categoryData[groupPosition].getAddress());
        text_price.setText("Rs."+categoryData[groupPosition].getSubtotal());
        text_date.setText("Payment method- "+categoryData[groupPosition].getPayment_method());
        if(categoryData[groupPosition].getStatus().equalsIgnoreCase("0")){
            text_items.setText("pending");
        }else {
            text_items.setText("completed");
        }




        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = inflater.inflate(R.layout.row_order_history, null);

        TextView textView = (TextView) convertView.findViewById(R.id.text_dishname);
        TextView text_suppliername = (TextView) convertView.findViewById(R.id.text_suppliername);
        TextView text_price = (TextView) convertView.findViewById(R.id.text_price);

        // String[] childArray=data.get(groupPosition);

        String text = categoryData[groupPosition].getSubCategoryData()[childPosition].getName();
        String text_price1 = "Rs. "+categoryData[groupPosition].getSubCategoryData()[childPosition].getPrice();
        String text_suppliername1 = "qty: "+categoryData[groupPosition].getSubCategoryData()[childPosition].getQty();
        textView.setText(text);
        text_suppliername.setText(text_suppliername1);
        text_price.setText(text_price1);

        textView.setOnClickListener(view -> {

            if (listener != null) {
                listener.onCategory(text);
            }

        });

        return convertView;

    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}
