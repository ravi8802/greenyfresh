package com.dev.greeny.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.dev.greeny.R
import com.dev.greeny.activity.FoodListActivity
import com.dev.greeny.entity.ShopListEntity
import com.dev.greeny.utility.Utils
import com.dev.greeny.utility.Validate
import kotlinx.android.synthetic.main.layout_product_list_item.view.*

class ShopListingAdapter(private var context: Context, val shopList: List<ShopListEntity>?): RecyclerView.Adapter<ShopListingAdapter.ProductListingViewHolder>() {
var validate:Validate?=null
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductListingViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.shop_list_item,parent,false)
        validate= Validate(context)
        return ProductListingViewHolder(view)
    }

    override fun getItemCount(): Int {
        return shopList!!.size
    }

    override fun onBindViewHolder(holder: ProductListingViewHolder, position: Int) {
        holder.tvname.text=shopList!!.get(position).shop_name
        holder.tvdescription.text=shopList!!.get(position).shop_address
        holder.tvprice.text=shopList!!.get(position).shop_phone
        var imagelist=shopList!!.get(position).shop_image
        if(!imagelist.isNullOrEmpty()) {
            Glide.with(context)
                    .load(imagelist) // image url
                    .placeholder(R.drawable.kohinoor512) // any placeholder to load at start
                    .error(R.drawable.kohinoor512)  // any image in case of error
                    .override(200, 200) // resizing
                    .centerCrop()
                    .into(holder.imageView)
        }
        holder.grid_cardview.setOnClickListener {
            validate!!.SaveSharepreferenceInt(Utils.ShopCategoryid,shopList!!.get(position).category_id!!)
            validate!!.SaveSharepreferenceInt(Utils.Shopkeeper_id,shopList!!.get(position).shopkeeper_id!!)
            val intent = Intent(context, FoodListActivity::class.java)
            intent.flags = (Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
            context.startActivity(intent)
        }

    }

    inner class ProductListingViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val grid_cardview = view.grid_cardview
        val imageView = view.imageView
        val tvname = view.tvname
        val tvdescription = view.tvdescription
        val tvprice = view.tvprice

    }
}