package com.dev.greeny.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.viewpager.widget.PagerAdapter;

import com.bumptech.glide.Glide;
import com.dev.greeny.R;
import com.dev.greeny.baen.CommonResponse;

import java.util.ArrayList;
import java.util.List;

public class ProductImageView extends PagerAdapter {
    List<CommonResponse> image;
    Context context;
//        private List<String> image;

    public ProductImageView(Context context, List<CommonResponse> images) {
        this.context = context;
        image = images;

    }

    @Override
    public Object instantiateItem(final ViewGroup container, int position) {

        View itemView = LayoutInflater.from(context).inflate(R.layout.product_imageview, container, false);
        ImageView product_imgs = (ImageView) itemView.findViewById(R.id.product_imgs);


        Glide.with(context).load(image.get(position).getImage()).override(500,500).into(product_imgs);



        container.addView(itemView);
        return itemView;
    }


    @Override
    public int getCount() {
        return image.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((LinearLayout) object);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);

    }
}
