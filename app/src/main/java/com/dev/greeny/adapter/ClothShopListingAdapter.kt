package com.dev.greeny.adapter

import android.content.Context
import android.graphics.Paint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.dev.greeny.R
import com.dev.greeny.baen.ProductDataResponce
import com.dev.greeny.utility.Validate
import kotlinx.android.synthetic.main.shop_list_item.view.*

class ClothShopListingAdapter(private var context: Context, var shopList: List<ProductDataResponce>?) : RecyclerView.Adapter<ClothShopListingAdapter.ProductListingViewHolder>() {
    var validate: Validate? = null

    private var onCallBack: AddCardInterface? = null
    private var onLikeCallBack: LikeInterface? = null

    fun onAddCard(onCallBack: AddCardInterface) {
        this.onCallBack = onCallBack
    }
fun onLikeItem(onLikeCallBack: LikeInterface) {
        this.onLikeCallBack = onLikeCallBack
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductListingViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.shop_list_item, parent, false)
        validate = Validate(context)
        return ProductListingViewHolder(view)
    }

    override fun getItemCount(): Int {
        return shopList!!.size
    }


    override fun onBindViewHolder(holder: ProductListingViewHolder, position: Int) {
        holder.tvdisprice.setPaintFlags(holder.tvdisprice.getPaintFlags() or Paint.STRIKE_THRU_TEXT_FLAG)

        holder.tvname.text = shopList!!.get(position).descriptionsList!!.get(0).name

        holder.tvdisprice.text = context.getString(R.string.rupee)+" " + shopList!!.get(position).price.toString()
        holder.tvprice.text = context.getString(R.string.rupee)+" " + shopList!!.get(position).price.toString()
        var imagelist = shopList!!.get(position).imageList!!.get(0).image
        if (!imagelist.isNullOrEmpty()) {
            Glide.with(context)
                    .load(imagelist) // image url
                    .placeholder(R.drawable.greenyfreashappicon) // any placeholder to load at start
                    .error(R.drawable.greenyfreashappicon)  // any image in case of error
                    .override(200, 200) // resizing
                    .centerCrop()
                    .into(holder.imageView)
        }
        if (shopList!!.get(position).wish.equals("no")) {
            Glide.with(context)
                    .load(R.drawable.heart_green) // image url
                    .into(holder.ivLike)
        }else{
            Glide.with(context)
                    .load(R.drawable.like) // image url
                    .into(holder.ivLike)
        }
        if(shopList!!.get(position).cart.equals("no")){
            holder.btnaddcart.text="ADD TO CART"
        }else{
            holder.btnaddcart.text="ADDED!"
        }


        holder.btnaddcart.setOnClickListener {
            onCallBack?.onCallBackAddCardItem(shopList!!,position)
        }
        holder.ivLike.setOnClickListener {
            onLikeCallBack?.onCallLikeItem(shopList!!.get(position))
        }


//        holder.grid_cardview.setOnClickListener {
//            validate!!.SaveSharepreferenceInt(Utils.ShopCategoryid,shopList!!.get(position).category_id!!)
//            validate!!.SaveSharepreferenceInt(Utils.Shopkeeper_id,shopList!!.get(position).shopkeeper_id!!)
//            val intent = Intent(context, ClothsListActivity::class.java)
//            intent.flags = (Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
//            context.startActivity(intent)
//        }
    }

    interface AddCardInterface {
        fun onCallBackAddCardItem(propertyData: List<ProductDataResponce>,position:Int)
    }
    interface LikeInterface {
        fun onCallLikeItem(propertyData: ProductDataResponce)
    }


    inner class ProductListingViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val imageView = view.imageView
        val grid_cardview = view.grid_cardview
        val tvname = view.tvname
        val tvdisprice = view.tvdisprice
        val tvprice = view.tvprice
        val btnaddcart = view.btnaddcart
        val ivLike = view.ivLike

    }

    fun updatedata(propertyData: List<ProductDataResponce>){
        shopList = propertyData
        notifyDataSetChanged()
    }
}