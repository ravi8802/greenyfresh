package com.dev.greeny.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseExpandableListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.dev.greeny.R
import com.dev.greeny.baen.OrderListResponse
import kotlinx.android.synthetic.main.single_row_cart.view.*

class OrderHistoryAdapter (private var context: Context, val productList: List<OrderListResponse>?) : RecyclerView.Adapter<OrderHistoryAdapter.CartViewHolder>() {
    private var onCallBack: CartItemsInterface? = null
    var cart_qty = 0


    override fun getItemCount(): Int {
        return productList!!.size
    }

    fun onCartItem(onCallBack: CartItemsInterface) {
        this.onCallBack = onCallBack
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CartViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.row_order_history, parent, false)
        return CartViewHolder(view)
    }

    override fun onBindViewHolder(holder: CartViewHolder, position: Int) {
        holder.text_dishname.text = productList!!.get(position).name
        holder.text_suppliername.text = "Qwt : "+productList!!.get(position).quantity
        holder.text_price.text = "Amt : "+productList!!.get(position).total_price.toString()
        var imagelist = productList!!.get(position).image
        if (!imagelist.isNullOrEmpty()) {
            Glide.with(context)
                    .load(imagelist) // image url
                    .placeholder(R.drawable.kohinoor512) // any placeholder to load at start
                    .error(R.drawable.kohinoor512)  // any image in case of error
                    .override(200, 200) // resizing
                    .centerCrop()
                    .into(holder.imageDish)
        }


    }

    interface CartItemsInterface {
        fun onCallBackCartItem(cartData: OrderListResponse)
    }

    inner class CartViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val imageDish = view.imageDish
        val text_dishname = view.text_dishname
        val image_delete = view.image_delete
        val text_suppliername = view.text_suppliername
        val text_price = view.text_price
        val tvqty = view.tvqty


    }
}