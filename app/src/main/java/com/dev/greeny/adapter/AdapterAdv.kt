package com.dev.greeny.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.dev.greeny.R
import com.dev.greeny.baen.CommonResponse
import kotlinx.android.synthetic.main.row_adv.view.*
import kotlinx.android.synthetic.main.single_row_cart.view.*

class AdapterAdv (private var context: Context, val productlist: List<CommonResponse>?): RecyclerView.Adapter<AdapterAdv.adapterAdvViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): adapterAdvViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.row_adv, parent, false)


        return adapterAdvViewHolder(view)
    }

    override fun getItemCount(): Int {
        return productlist!!.size
    }

    override fun onBindViewHolder(holder: adapterAdvViewHolder, position: Int) {

        var imagelist = productlist!!.get(position).image
        if (!imagelist.isNullOrEmpty()) {
            Glide.with(context)
                    .load(imagelist) // image url
                    .placeholder(R.drawable.greenyfreashappicon) // any placeholder to load at start
                    .error(R.drawable.greenyfreashappicon)  // any image in case of error
                    .override(200, 200) // resizing
                    .centerCrop()
                    .into(holder.imageView3)
        }

    }

    inner class adapterAdvViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val imageView3 = view.imageView3
    }



}