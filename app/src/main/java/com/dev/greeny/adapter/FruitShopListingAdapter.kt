package com.dev.greeny.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.dev.greeny.R
import com.dev.greeny.entity.ShopListEntity
import com.dev.greeny.utility.Validate
import kotlinx.android.synthetic.main.fruitshop_list_item.view.*

class FruitShopListingAdapter(private var context: Context, val shopList: List<ShopListEntity>?): RecyclerView.Adapter<FruitShopListingAdapter.ProductListingViewHolder>() {
var validate: Validate?=null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductListingViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.fruitshop_list_item,parent,false)
        validate= Validate(context)
        return ProductListingViewHolder(view)
    }

    override fun getItemCount(): Int {
        return 4
    }



    override fun onBindViewHolder(holder: ProductListingViewHolder, position: Int) {
       /* holder.tvname.text=shopList!!.get(position).shop_name
        holder.tvdescription.text=shopList!!.get(position).shop_address
        holder.tvprice.text=shopList!!.get(position).shop_phone
        var imagelist=shopList!!.get(position).shop_image
        if(!imagelist.isNullOrEmpty()) {
            Glide.with(context)
                    .load(imagelist) // image url
                    .placeholder(R.drawable.kohinoor512) // any placeholder to load at start
                    .error(R.drawable.kohinoor512)  // any image in case of error
                    .override(200, 200) // resizing
                    .centerCrop()
                    .into(holder.imageView)
        }
        holder.grid_cardview.setOnClickListener {
            validate!!.SaveSharepreferenceInt(Utils.ShopCategoryid,shopList!!.get(position).category_id!!)
            validate!!.SaveSharepreferenceInt(Utils.Shopkeeper_id,shopList!!.get(position).shopkeeper_id!!)
            val intent = Intent(context, ClothsListActivity::class.java)
            intent.flags = (Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
            context.startActivity(intent)
        }
*/
    }




    inner class ProductListingViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val imageView = view.imageView
        val grid_cardview = view.grid_cardview
        val tvname = view.tvname
        val tvdescription = view.tvdescription
        val tvprice = view.tvprice

    }
}