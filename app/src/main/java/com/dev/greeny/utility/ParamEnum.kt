package com.dev.greeny.utility

/**
 * Created by Mahipal on 2/11/18.
 */
enum class ParamEnum {

    NUMBER("number"),
    COUNTRY_CODE("country_code"),
    NAME("name"),
    EMAIL("email"),
    DOB("dob"),
    GENDER("gender"),
    ADDRESS("address"),
    PASSWORD("password"),
    CONFIRM_PASSWORD("confirm_password"),
    SUCCESS("SUCCESS"),
    TRUE("true"),
    CUSTOMER("Customer"),
    SHOPKEEPER("Shopkeeper"),
    ANDROID("android"),
    FROM("from");

    private val value: String?

    private constructor(value: String) {
        this.value = value
    }

    private constructor() {
        this.value = null
    }

    fun theValue(): String? {
        return this.value
    }

}