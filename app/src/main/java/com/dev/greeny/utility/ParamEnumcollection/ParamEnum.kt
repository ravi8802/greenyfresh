package com.dev.greeny.utility.ParamEnumcollection

/**
 * Created by Mahipal on 2/11/18.
 */
enum class ParamEnum {

    NUMBER("number"),
    COUNTRY_CODE("country_code"),
    NAME("name"),
    EMAIL("email"),
    DOB("date_of_birth"),
    GENDER("gender"),
    ADDRESS("address"),
    PASSWORD("password"),
    CONFIRM_PASSWORD("confirm_password"),
    CUSTOMER("CUSTOMER"),
    GST_number("GST_number"),
    Stateid("state_id"),
    Districtid("district_id"),
    Categoryid("Categoryid");

    private val value: String?

    private constructor(value: String) {
        this.value = value
    }

    private constructor() {
        this.value = null
    }

    fun theValue(): String? {
        return this.value
    }

}