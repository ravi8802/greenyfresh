package com.dev.greeny.utility;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Created by mobulous2 on 31/5/16.
 */
public class NetworkConnectionCheck
{
    private Context context;

    public NetworkConnectionCheck(Context context){
        this.context=context;
    }

    public boolean isConnect()
    {
        ConnectivityManager connectivity = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null)
        {
            NetworkInfo info = connectivity.getActiveNetworkInfo();
            if (info != null && info.isConnectedOrConnecting())
            {
                return true;
            }
        }
        return false;
    }
}