package com.dev.greeny.utility

import android.app.Dialog
import android.content.Context
import android.graphics.drawable.ColorDrawable
import android.view.Gravity
import android.view.View
import android.view.Window

import com.airbnb.lottie.LottieAnimationView
import com.dev.greeny.R


/**
 * Created by Mahipal Singh  mahisingh1@Outlook.com on 6/12/18.
 */
class CustomProgressBar(context: Context, theme: Int) : Dialog(context) {
    companion object {
        fun show(context: Context, indeterminate: Boolean, cancelable: Boolean): CustomProgressBar {
            val dialog = CustomProgressBar(context, android.R.style.Theme_Black_NoTitleBar_Fullscreen)
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog.window!!.setBackgroundDrawable(ColorDrawable(android.graphics.Color.TRANSPARENT))
            dialog.setContentView(R.layout.layout_progress)
            val lottieAnimationView =
                dialog.findViewById<View>(R.id.lottieAnimationView) as LottieAnimationView
            lottieAnimationView.setAnimation("splashy_loader.json")
            lottieAnimationView.playAnimation()
            lottieAnimationView.loop(true)
            dialog.setCancelable(cancelable)
            dialog.window!!.attributes.gravity = Gravity.CENTER
            val lp = dialog.window!!.attributes
            lp.dimAmount = 0.2f
            dialog.window!!.attributes = lp
            //dialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_BLUR_BEHIND);
            dialog.show()
            return dialog
        }
    }
}
