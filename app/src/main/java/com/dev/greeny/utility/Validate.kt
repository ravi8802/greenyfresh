package  com.dev.greeny.utility

import android.annotation.SuppressLint
import android.app.Activity
import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.content.SharedPreferences
import android.content.pm.PackageInfo
import android.content.pm.PackageManager
import android.net.ConnectivityManager
import android.view.LayoutInflater

import android.view.ViewGroup
import android.widget.*
import  com.dev.greeny.R
import  com.dev.greeny.entity.MSTProductEntity
import com.google.gson.Gson
import kotlinx.android.synthetic.main.dialoge.view.*
import org.json.JSONArray
import org.json.JSONObject

import java.text.SimpleDateFormat
import java.util.*
import java.util.regex.Pattern


class Validate(context: Context) {

    val MyPREFERENCES = "APPISP"
    lateinit var sharedpreferences: SharedPreferences
    lateinit var editor: SharedPreferences.Editor
    lateinit var context: Context
    lateinit var myLocale: Locale
    internal var datepic: Dialog? = null

    init {
        this.context = context
        sharedpreferences = context.getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE)
        editor = sharedpreferences.edit()
    }

    fun SaveSharepreferenceString(key: String, Value: String) {
        editor.putString(key, Value)
        editor.commit()
    }

    fun RetriveSharepreferenceString(key: String): String? {
        return sharedpreferences.getString(key, "")
    }

    fun SaveSharepreferenceInt(key: String, iValue: Int) {
        editor.putInt(key, iValue)
        editor.commit()
    }

    fun RetriveSharepreferenceInt(key: String): Int {
        return sharedpreferences.getInt(key, 0)
    }

    fun ClearSharedPrefrence() {
        editor.clear()
        editor.commit()
    }

    fun ReturnVersionCode(activity: Activity): Int {
        var pinfo: PackageInfo? = null
        try {
            pinfo = activity.packageManager.getPackageInfo(activity.packageName, 0)
        } catch (e: PackageManager.NameNotFoundException) {
            e.printStackTrace()
        }

        val iVersionCode = pinfo!!.versionCode
        val sVersionName = pinfo.versionName
        return iVersionCode
    }

    fun random(): String {
        val chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz".toCharArray()
        val sb = StringBuilder()
        val random = Random()
        for (i in 0..29) {
            val c = chars[random.nextInt(chars.size)]
            sb.append(c)
        }
        val dateFormat = SimpleDateFormat("yyyyMMddHHmmssSS")
        val date = Date()
        val SDateString = dateFormat.format(date)
        return sb.toString() + SDateString
    }

    fun orderid(): String {
        val dateFormat = SimpleDateFormat("yyyyMMddHHmmssSS")
        val date = Date()
        val SDateString = dateFormat.format(date)
        return SDateString
    }

    @SuppressLint("ResourceType")
    fun dynamicRadio(context: Context, radioGroup: RadioGroup, values: Array<String?>) {

        for (i in values) {
            val radioButton1 = RadioButton(context)
            radioButton1.layoutParams =
                LinearLayout.LayoutParams(
                    ViewGroup.LayoutParams.WRAP_CONTENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT
                )
            radioButton1.setText(i)
            radioButton1.id = values.indexOf(i)
            val params = RadioGroup.LayoutParams(context, null)
            params.setMargins(0, 0, 100, 0)
            radioButton1.setLayoutParams(params)
            if (radioGroup != null) {
                radioGroup.addView(radioButton1)
                radioGroup.setOnCheckedChangeListener { group, checkedId ->

                }
            }
        }

    }


    fun returnIntegerValue(myString: String?): Int {
        var iValue = 0
        if (myString != null && !myString.equals(
                "null",
                ignoreCase = true
            ) && myString.length > 0
        ) {
            iValue = Integer.valueOf(myString)
        }
        return iValue

    }

    fun returnStringValue(myString: String?): String {
        var iValue = ""
        if (myString != null && !myString.equals(
                "null",
                ignoreCase = true
            ) && myString.length > 0
        ) {
            iValue = myString
        }
        return iValue

    }

    /* fun dynamicMultiCheck(context: Context, liear: LinearLayout, flag: Int) {

         mstCommon = AppiANMAplication.database?.commonDao()?.getCommonData(flag)

         if (mstCommon != null) {
             val iGen = mstCommon!!.size
             val name = arrayOfNulls<String>(iGen + 1)
             for (i in 0 until mstCommon!!.size) {
                 val multicheck1 = CheckBox(context)
                 multicheck1.layoutParams =
                     LinearLayout.LayoutParams(
                         ViewGroup.LayoutParams.WRAP_CONTENT,
                         ViewGroup.LayoutParams.WRAP_CONTENT
                     )
                 multicheck1.setText(mstCommon!!.get(i).Value)
                 multicheck1.id = mstCommon!!.get(i).ID
                 if (liear != null) {
                     liear.addView(multicheck1);
                 }

             }
         }

     }*/


    fun returndoubleValue(myString: String?): Double {
        var iValue = 0.0
        if (myString != null && !myString.equals(
                "null",
                ignoreCase = true
            ) && myString.length > 0
        ) {
            iValue = myString.toDouble()
        }
        return iValue

    }


    /*fun CustomAlertSpinner(activity: Activity, spin: Spinner, msg: String) {
        val mDialogView = LayoutInflater.from(activity!!).inflate(R.layout.dialoge, null)
        val mBuilder = AlertDialog.Builder(activity!!)
            .setView(mDialogView)
        val mAlertDialog = mBuilder.show()
        mDialogView.txt_msg.text = msg
        mDialogView.btn_ok.setOnClickListener {

            mAlertDialog.dismiss()
            spin.performClick()
            spin.requestFocus()

        }
    }*/
    fun checkmobileno(mobileno: String?): Int {

        if (mobileno != null && !mobileno.equals(
                "null",
                ignoreCase = true
            ) && mobileno.length > 0
        ) {
            val pattern = Pattern.compile("[6-9]{1}[0-9]{9}")

            val matcher = pattern.matcher(mobileno)
            // Check if pattern matches
            return if (matcher.matches()) {
                1
            } else {
                0
            }
        }
        return 0

    }

    fun ReturnJson(
        abc: Array<ArrayList<*>>,
        TableName: Array<String>,
        jsonObjectcombined: JSONObject
    ): String {

        for (i in TableName.indices) {
            try {
                if (abc[i].size > 0) {
                    val json1 = Gson().toJson(abc[i])
                    val JSONCrosscheck = JSONArray(json1)
                    jsonObjectcombined.put(
                        TableName[i],
                        JSONCrosscheck
                    )
                } else {
                    val otherJsonArray = JSONArray()
                    jsonObjectcombined.put(TableName[i], otherJsonArray)

                }
            } catch (e: Exception) {
                e.printStackTrace()
            }

        }

        return jsonObjectcombined.toString()
    }

    fun isNetworkConnected(activity: Activity): Boolean {
        val connectivityManager =
            activity.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager //1
        val networkInfo = connectivityManager.activeNetworkInfo //2
        return networkInfo != null && networkInfo.isConnected //3
    }

    /*fun fillSpinner(
        activity: Activity,
        spin: Spinner,
        Header: String?,
        liststatus: List<MSTProductEntity>
    ) {

        val adapter: ArrayAdapter<String?>
        val sValue = arrayOfNulls<String>(liststatus!!.size + 1)
        if (Header != null && Header.length > 0) {
            sValue[0] = Header
        } else {
            sValue[0] = activity.resources.getString(R.string.selectproduct)
        }
        for (i in liststatus.indices) {
            sValue[i + 1] = liststatus.get(i).name_category
        }
        adapter = ArrayAdapter(
            activity,
            R.layout.my_spinner_space, sValue
        )
        adapter.setDropDownViewResource(R.layout.my_spinner)
        spin.adapter = adapter
    }
*/
    fun returnstrProduct(id: Int?, data: List<MSTProductEntity>?): String {

        var str = ""
        if (!data.isNullOrEmpty()) {
            if (id!! > 0) {
                for (i in data.indices) {
                    if (id == i)
                        str = data.get(i).name_category!!
                }
            }
        }
        return str
    }

    fun returnidProduct(id: Int?, data: List<MSTProductEntity>?): Int {

        var pos = 0
        if (!data.isNullOrEmpty()) {
            if (id!! <=data.size) {
                pos = data.get(id).id_category

            }
        }
        return pos
    }


    fun CustomAlert(str: String, activity: Activity) {
        val mDialogView = LayoutInflater.from(activity!!).inflate(R.layout.dialoge, null)
        val mBuilder = AlertDialog.Builder(activity!!)
            .setView(mDialogView)
        val mAlertDialog = mBuilder.show()
        mDialogView.txt_msg.text = str
        mDialogView.btn_ok.setOnClickListener {

            mAlertDialog.dismiss()


        }
    }

}