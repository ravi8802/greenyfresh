package com.dev.greeny.utility

import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.net.Uri
import android.text.Spannable
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.ArrayAdapter
import android.widget.Spinner
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import com.dev.greeny.R
import com.dev.greeny.entity.CategoryListEntity
import com.dev.greeny.entity.mstdistrict
import com.dev.greeny.entity.mststate
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.iid.InstanceIdResult
import java.io.IOException
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

///////email check string///////////

var emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+"
var MobilePattern = "[0-9]{10}"
var passwordPattern = "^(?=.*[0-9])(?=.*[A-Z])(?=.*[@#$%^&+=!])(?=\\S+$).{4,}$"

fun setToolbar(activity: AppCompatActivity, toolbar: Toolbar, title: TextView, TitleContent: String) {
    activity.setSupportActionBar(toolbar)
    toolbar.setNavigationIcon(R.drawable.back_btn_wht)
    title.text = TitleContent
    activity.supportActionBar!!.setDisplayShowTitleEnabled(false)
    toolbar.setNavigationOnClickListener { activity.onBackPressed() }
}

fun Context.toast(msg: String){
    Toast.makeText(this, msg, Toast.LENGTH_SHORT).show()
}

fun showDialog(activity: Activity, message: String) {
    val builder = AlertDialog.Builder(activity)
    builder.setTitle("Alert!")
    builder.setCancelable(false)
    builder.setMessage(message)
    builder.setPositiveButton("Ok", null)
    builder.show()
}

fun getAge(year: Int, month: Int, day: Int): String {
    val dob = Calendar.getInstance()
    val today = Calendar.getInstance()

    dob.set(day, month, year)

    var age = today.get(Calendar.YEAR) - dob.get(Calendar.YEAR)

    if (today.get(Calendar.DAY_OF_YEAR) < dob.get(Calendar.DAY_OF_YEAR)) {
        age--
    }

    val ageInt = age

    return ageInt.toString()
}

fun snackBar(activity: Activity, message: String?) {
    val snackbar =
            Snackbar.make(activity.findViewById(android.R.id.content), message?:"", Snackbar.LENGTH_LONG)
    val snackBarView = snackbar.view
    snackBarView.minimumHeight = 20
    snackBarView.setBackgroundColor(Color.parseColor("#DB181D"))
    val tv = snackBarView.findViewById(R.id.snackbar_text) as TextView
    tv.textSize = 13f
    //Fonts.OpenSans_Regular_Txt(tv, activity.getAssets());
    tv.setTextColor(Color.parseColor("#ffffff"))
    snackbar.show()
}

//    METHOD: TO DRAW UNDERLINE BELOW TEXT_VIEW AND MAKE TEXT AND LINE IN BLUE COLOR
fun setUpBlackText(textView: TextView, start: Int, str: String) {

    val content = SpannableString(str)
    content.setSpan(
            ForegroundColorSpan(Color.BLACK),
            start,
            str.length,
            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
    )
    //        content.setSpan(new StyleSpan(Typeface.ITALIC), start, end, 0);
    textView.text = content
}

fun hideSoftInput(activity: Activity) {
    activity.window.setSoftInputMode(
            WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN
    )
}

@Throws(IOException::class)
fun openFile(context: Context, url: String) {
    try {
        // Create URI
        val uri = Uri.parse(url)

        val intent = Intent()
        // Check what kind of file you are trying to open, by comparing the url with extensions.
        // When the if condition is matched, plugin sets the correct intent (mime) type,
        // so Android knew what application to use to open the file
        if (url.contains(".doc") || url.contains(".docx")) {
            // Word document
            intent.setDataAndType(uri, "application/msword")
        } else if (url.contains(".pdf")) {
            // PDF file
            intent.setDataAndType(uri, "application/pdf")
        } else if (url.contains(".ppt") || url.contains(".pptx")) {
            // Powerpoint file
            intent.setDataAndType(uri, "application/vnd.ms-powerpoint")
        } else if (url.contains(".xls") || url.contains(".xlsx")) {
            // Excel file
            intent.setDataAndType(uri, "application/vnd.ms-excel")
        } else if (url.contains(".zip") || url.contains(".rar")) {
            // WAV audio file
            intent.setDataAndType(uri, "application/x-wav")
        } else if (url.contains(".rtf")) {
            // RTF file
            intent.setDataAndType(uri, "application/rtf")
        } else if (url.contains(".wav") || url.contains(".mp3")) {
            // WAV audio file
            intent.setDataAndType(uri, "audio/x-wav")
        } else if (url.contains(".gif")) {
            // GIF file
            intent.setDataAndType(uri, "image/gif")
        } else if (url.contains(".jpg") || url.contains(".jpeg") || url.contains(".png")) {
            // JPG file
            intent.setDataAndType(uri, "image/jpeg")
        } else if (url.contains(".txt")) {
            // Text file
            intent.setDataAndType(uri, "text/plain")
        } else if (url.contains(".3gp") || url.contains(".mpg") || url.contains(".mpeg") || url.contains(
                        ".mpe"
                ) || url.contains(".mp4") || url.contains(".avi")
        ) {
            // Video files
            intent.setDataAndType(uri, "video/*")
        } else {
            //if you want you can also define the intent type for any other file

            //additionally use else clause below, to manage other unknown extensions
            //in this case, Android will show all applications installed on the device
            //so you can choose which application to use
            intent.setDataAndType(uri, "*/*")
        }

        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        if (intent.resolveActivity(context.packageManager) != null) {
            context.startActivity(intent)
        }
    } catch (e: Exception) {
        e.printStackTrace()
    }

}

fun getOutputFormat(responseDate: String): String {
    //        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
    //            DateTimeFormatter inputFormatter = DateTimeFormatter.ofPattern("dd-MM-yyyy hh:mm a", Locale.ENGLISH);
    //            DateTimeFormatter outputFormatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm", Locale.ENGLISH);
    //            LocalDate date = LocalDate.parse(responseDate, inputFormatter);
    //            String formattedDate = outputFormatter.format(date);
    //            return formattedDate;
    //        }else {
    val inputFormate = SimpleDateFormat("dd-MM-yyyy hh:mm a", Locale.ENGLISH)
    val outputFormate = SimpleDateFormat("dd-MM-yyyy HH:mm", Locale.ENGLISH)
    var date: Date? = null
    try {
        date = inputFormate.parse(responseDate)
    } catch (e: ParseException) {
        e.printStackTrace()
    }

    return outputFormate.format(date)
    //}
}

fun setSpinner(context: Context, spinner: Spinner, array: Array<String>) {
    val mContext = context.applicationContext
    val adapter = object :
            ArrayAdapter<String>(mContext, android.R.layout.simple_spinner_dropdown_item, array) {
        override fun isEnabled(position: Int): Boolean {
            return position != 0
        }
        override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup): View {
            val mInflater = LayoutInflater.from(context)
            val convertView = mInflater.inflate(R.layout.layout_custom_spinner, parent, false)
            val label = convertView.findViewById<TextView>(R.id.spinner_text)
            label.text = array[position]
            if (position == 0) {
                label.setTextColor(context.resources.getColor(android.R.color.darker_gray));
            } else {
                label.setTextColor(context.resources.getColor(android.R.color.black));
            }
            return convertView
        }
    }
    spinner.adapter = adapter
}
fun setstateSpinner(context: Context, spinner: Spinner, list: List<mststate>?) {
    val mContext = context.applicationContext
    val iGen = list!!.size
    val name = arrayOfNulls<String>(iGen + 1)
    name[0] = mContext.resources.getString(R.string.district)

    for (i in 0 until list!!.size) {
        name[i + 1] = list[i].state_name
    }
    val adapter = object :
            ArrayAdapter<String>(mContext, android.R.layout.simple_spinner_dropdown_item, name) {
        override fun isEnabled(position: Int): Boolean {
            return position != 0
        }
        override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup): View {
            val mInflater = LayoutInflater.from(context)
            val convertView = mInflater.inflate(R.layout.layout_custom_spinner, parent, false)
            val label = convertView.findViewById<TextView>(R.id.spinner_text)
            label.text = name[position]
            if (position == 0) {
                label.setTextColor(context.resources.getColor(android.R.color.darker_gray));
            } else {
                label.setTextColor(context.resources.getColor(android.R.color.black));
            }
            return convertView
        }
    }
    spinner.adapter = adapter
}

fun setdistrictSpinner(context: Context, spinner: Spinner, list: List<mstdistrict>?) {
    val mContext = context.applicationContext
    val iGen = list!!.size
    val name = arrayOfNulls<String>(iGen + 1)
    name[0] = mContext.resources.getString(R.string.district)

    for (i in 0 until list!!.size) {
        name[i + 1] = list[i].name
    }
    val adapter = object :
            ArrayAdapter<String>(mContext, android.R.layout.simple_spinner_dropdown_item, name) {
        override fun isEnabled(position: Int): Boolean {
            return position != 0
        }
        override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup): View {
            val mInflater = LayoutInflater.from(context)
            val convertView = mInflater.inflate(R.layout.layout_custom_spinner, parent, false)
            val label = convertView.findViewById<TextView>(R.id.spinner_text)
            label.text = name[position]
            if (position == 0) {
                label.setTextColor(context.resources.getColor(android.R.color.darker_gray));
            } else {
                label.setTextColor(context.resources.getColor(android.R.color.black));
            }
            return convertView
        }
    }
    spinner.adapter = adapter
}
fun setcategorySpinner(context: Context, spinner: Spinner, list: List<CategoryListEntity>?) {
    val mContext = context.applicationContext
    val iGen = list!!.size
    val name = arrayOfNulls<String>(iGen + 1)
    name[0] = mContext.resources.getString(R.string.district)

    for (i in 0 until list!!.size) {
        name[i + 1] = list[i].category_name
    }
    val adapter = object :
            ArrayAdapter<String>(mContext, android.R.layout.simple_spinner_dropdown_item, name) {
        override fun isEnabled(position: Int): Boolean {
            return position != 0
        }
        override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup): View {
            val mInflater = LayoutInflater.from(context)
            val convertView = mInflater.inflate(R.layout.layout_custom_spinner, parent, false)
            val label = convertView.findViewById<TextView>(R.id.spinner_text)
            label.text = name[position]
            if (position == 0) {
                label.setTextColor(context.resources.getColor(android.R.color.darker_gray));
            } else {
                label.setTextColor(context.resources.getColor(android.R.color.black));
            }
            return convertView
        }
    }
    spinner.adapter = adapter
}

fun getDeviceToken(context: Context) {
    val mContext = context.applicationContext
    var validate:Validate?=null
    val thread = object : Thread() {
        override fun run() {
            Log.e(">>>>>>>>>>>>>>", "thred IS  running")
            try {
                if (validate!!.RetriveSharepreferenceString(Utils.TOKEN).isNullOrEmpty()) {
                    FirebaseInstanceId.getInstance().instanceId.addOnCompleteListener { task ->
                        if (!task.isSuccessful) {
                            return@addOnCompleteListener
                        }
                        val token = Objects.requireNonNull<InstanceIdResult>(task.result).token
                        Log.e("Generated Device Token", "-->$token")
                        if (token == null) {
                            getDeviceToken(mContext)
                        } else {
                            validate!!.SaveSharepreferenceString(Utils.TOKEN,token)

                        }
                    }
                }
            } catch (e1: Exception) {
                e1.printStackTrace()
            }
            super.run()
        }
    }
    thread.start()
}

